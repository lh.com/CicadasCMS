package com.cicadascms.plugin.wxpush;

import com.cicadascms.plugin.annotation.Plugin;
import com.cicadascms.plugin.hook.AbstractContentPublishPlugin;

/**
 * 微信群发插件
 *
 * @author Jin
 */
@Plugin
public class WxMpMassPlugin extends AbstractContentPublishPlugin<String> {

    @Override
    public Object invoke(String obj) {
        return null;
    }
}
