create table cms_channel
(
    channel_id int auto_increment
        primary key,
    site_id int default 1 null comment '站点编号',
    domain varchar(64) null comment '域名',
    channel_name varchar(255) default '' not null comment '分类明细',
    channel_model_id int null comment '栏目模型编号',
    content_model_ids varchar(50) null comment '内容模型编号',
    channel_url_path varchar(255) default '' not null comment '栏目路径',
    parent_id bigint default 0 null comment '父类编号',
    is_alone tinyint(1) default 0 null comment '单页栏目（0：不是，1：是）',
    alone_content mediumtext null comment '单页内容',
    channel_view varchar(255) default '' not null comment '首页视图模板',
    is_nav tinyint(1) default 0 not null comment '导航',
    url varchar(255) null comment '外链地址',
    has_children tinyint(1) default 0 null comment '是否有子类',
    page_size int null comment '栏目分页数量',
    allow_search tinyint(1) unsigned default 0 null comment '当前栏目下的是否支持全文搜索',
    channel_type int null comment '栏目分类',
    channel_icon varchar(255) null comment '栏目图标',
    sort_id int unsigned default 0 null,
    create_user int null comment '创建人',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '删除状态'
)
comment '内容分类表';


create table cms_content
(
    content_id int auto_increment comment '内容编号'
        primary key,
    site_id int not null comment '站点编号',
    channel_id int null comment '栏目编号',
    model_id int null comment '模型编号',
    title varchar(255) null comment '标题',
    sub_title varchar(255) null comment '副标题',
    author varchar(100) null comment '作者',
    keywords int null comment '页面关键字',
    description int null comment '页面描述',
    state int(1) null comment '内容状态',
    source varchar(64) null comment '来源',
    source_url varchar(255) null comment '原文地址',
    thumb varchar(255) null comment '封面图片',
    view_num int null comment '浏览数量',
    paid_reading tinyint(1) null comment '付费阅读',
    price int null comment '价格',
    page_total int null comment '内容分页',
    create_time datetime null comment '录入时间',
    create_user int null,
    update_user int null,
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null
)
    comment '内容';

create table cms_model
(
    model_id int auto_increment
        primary key,
    site_id bigint null comment '站点id',
    model_name varchar(255) null comment '模型名称',
    table_name varchar(255) null comment '模型表名称',
    model_type int null comment '内容模型，栏目模型',
    des varchar(255) default '' null comment '字段描述',
    status tinyint(1) default 1 not null comment '状态',
    model_view varchar(255) null comment '模型视图模板',
    create_user int null comment '创建人',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '删除状态'
)
    comment '内容模型表';

create table cms_model_field
(
    field_id int auto_increment comment '主键'
        primary key,
    model_id int not null comment '模型字段',
    field_name varchar(64) null comment '字段名称',
    field_type int null comment '字段类型',
    column_type int null comment '数据库字段类型',
    field_config json null comment '字段属性',
    is_search_field tinyint null comment '是否检索字段',
    des varchar(200) null comment '字段描述',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新时间',
    create_user int null comment '创建用户',
    create_time datetime null comment '创建时间',
    deleted_flag tinyint(1) default 0 null comment '删除标记'
)
    comment '模型字段';

create table cms_nav
(
    nav_id int auto_increment
        primary key,
    nav_name varchar(200) null comment '导航名称',
    target varchar(10) null comment 'target',
    href varchar(200) null comment '链接地址',
    parent_id int null comment '上级编号',
    has_child tinyint null comment '是否拥有子类',
    icon varchar(200) null comment '导航图标',
    status tinyint(1) null comment '导航状态',
    create_user int null comment '创建用户',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新用户',
    deleted_flag tinyint(1) null comment '删除标记'
)
    comment '导航菜单';

create table cms_site
(
    site_id int auto_increment
        primary key,
    site_name varchar(255) null comment '站点名称',
    http_protocol int(1) default 1 null comment 'http协议',
    domain varchar(500) null comment '站点域名',
    site_dir varchar(200) null comment '站点路径',
    status tinyint(1) default 1 null comment '站点状态',
    site_suffix int(1) null comment '站点请求后缀',
    is_default tinyint(1) default 0 not null comment '是否默认站点',
    pc_template_dir varchar(100) null comment 'pc端模板目录',
    mobile_template_dir varchar(100) null comment '移动端手机模板',
    create_user int null comment '创建用户',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新用户',
    deleted_flag tinyint(1) default 0 null comment '删除标记'
)
    comment '站点表';

create table qrtz_calendars
(
    SCHED_NAME varchar(120) not null,
    CALENDAR_NAME varchar(190) not null,
    CALENDAR blob not null,
    primary key (SCHED_NAME, CALENDAR_NAME)
);

create table qrtz_fired_triggers
(
    SCHED_NAME varchar(120) not null,
    ENTRY_ID varchar(95) not null,
    TRIGGER_NAME varchar(190) not null,
    TRIGGER_GROUP varchar(190) not null,
    INSTANCE_NAME varchar(190) not null,
    FIRED_TIME bigint(13) not null,
    SCHED_TIME bigint(13) not null,
    PRIORITY int not null,
    STATE varchar(16) not null,
    JOB_NAME varchar(190) null,
    JOB_GROUP varchar(190) null,
    IS_NONCONCURRENT varchar(1) null,
    REQUESTS_RECOVERY varchar(1) null,
    primary key (SCHED_NAME, ENTRY_ID)
);

create index IDX_QRTZ_FT_INST_JOB_REQ_RCVRY
	on qrtz_fired_triggers (SCHED_NAME, INSTANCE_NAME, REQUESTS_RECOVERY);

create index IDX_QRTZ_FT_JG
	on qrtz_fired_triggers (SCHED_NAME, JOB_GROUP);

create index IDX_QRTZ_FT_J_G
	on qrtz_fired_triggers (SCHED_NAME, JOB_NAME, JOB_GROUP);

create index IDX_QRTZ_FT_TG
	on qrtz_fired_triggers (SCHED_NAME, TRIGGER_GROUP);

create index IDX_QRTZ_FT_TRIG_INST_NAME
	on qrtz_fired_triggers (SCHED_NAME, INSTANCE_NAME);

create index IDX_QRTZ_FT_T_G
	on qrtz_fired_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

create table qrtz_job_details
(
    SCHED_NAME varchar(120) not null,
    JOB_NAME varchar(190) not null,
    JOB_GROUP varchar(190) not null,
    DESCRIPTION varchar(250) null,
    JOB_CLASS_NAME varchar(250) not null,
    IS_DURABLE varchar(1) not null,
    IS_NONCONCURRENT varchar(1) not null,
    IS_UPDATE_DATA varchar(1) not null,
    REQUESTS_RECOVERY varchar(1) not null,
    JOB_DATA blob null,
    primary key (SCHED_NAME, JOB_NAME, JOB_GROUP)
);

create index IDX_QRTZ_J_GRP
	on qrtz_job_details (SCHED_NAME, JOB_GROUP);

create index IDX_QRTZ_J_REQ_RECOVERY
	on qrtz_job_details (SCHED_NAME, REQUESTS_RECOVERY);

create table qrtz_locks
(
    SCHED_NAME varchar(120) not null,
    LOCK_NAME varchar(40) not null,
    primary key (SCHED_NAME, LOCK_NAME)
);

create table qrtz_paused_trigger_grps
(
    SCHED_NAME varchar(120) not null,
    TRIGGER_GROUP varchar(190) not null,
    primary key (SCHED_NAME, TRIGGER_GROUP)
);

create table qrtz_scheduler_state
(
    SCHED_NAME varchar(120) not null,
    INSTANCE_NAME varchar(190) not null,
    LAST_CHECKIN_TIME bigint(13) not null,
    CHECKIN_INTERVAL bigint(13) not null,
    primary key (SCHED_NAME, INSTANCE_NAME)
);

create table qrtz_triggers
(
    SCHED_NAME varchar(120) not null,
    TRIGGER_NAME varchar(190) not null,
    TRIGGER_GROUP varchar(190) not null,
    JOB_NAME varchar(190) not null,
    JOB_GROUP varchar(190) not null,
    DESCRIPTION varchar(250) null,
    NEXT_FIRE_TIME bigint(13) null,
    PREV_FIRE_TIME bigint(13) null,
    PRIORITY int null,
    TRIGGER_STATE varchar(16) not null,
    TRIGGER_TYPE varchar(8) not null,
    START_TIME bigint(13) not null,
    END_TIME bigint(13) null,
    CALENDAR_NAME varchar(190) null,
    MISFIRE_INSTR smallint(2) null,
    JOB_DATA blob null,
    primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint qrtz_triggers_ibfk_1
        foreign key (SCHED_NAME, JOB_NAME, JOB_GROUP) references qrtz_job_details (SCHED_NAME, JOB_NAME, JOB_GROUP)
);

create table qrtz_blob_triggers
(
    SCHED_NAME varchar(120) not null,
    TRIGGER_NAME varchar(190) not null,
    TRIGGER_GROUP varchar(190) not null,
    BLOB_DATA blob null,
    primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint qrtz_blob_triggers_ibfk_1
        foreign key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) references qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

create index SCHED_NAME
	on qrtz_blob_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

create table qrtz_cron_triggers
(
    SCHED_NAME varchar(120) not null,
    TRIGGER_NAME varchar(190) not null,
    TRIGGER_GROUP varchar(190) not null,
    CRON_EXPRESSION varchar(120) not null,
    TIME_ZONE_ID varchar(80) null,
    primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint qrtz_cron_triggers_ibfk_1
        foreign key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) references qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

create table qrtz_simple_triggers
(
    SCHED_NAME varchar(120) not null,
    TRIGGER_NAME varchar(190) not null,
    TRIGGER_GROUP varchar(190) not null,
    REPEAT_COUNT bigint(7) not null,
    REPEAT_INTERVAL bigint(12) not null,
    TIMES_TRIGGERED bigint(10) not null,
    primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint qrtz_simple_triggers_ibfk_1
        foreign key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) references qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

create table qrtz_simprop_triggers
(
    SCHED_NAME varchar(120) not null,
    TRIGGER_NAME varchar(190) not null,
    TRIGGER_GROUP varchar(190) not null,
    STR_PROP_1 varchar(512) null,
    STR_PROP_2 varchar(512) null,
    STR_PROP_3 varchar(512) null,
    INT_PROP_1 int null,
    INT_PROP_2 int null,
    LONG_PROP_1 bigint null,
    LONG_PROP_2 bigint null,
    DEC_PROP_1 decimal(13,4) null,
    DEC_PROP_2 decimal(13,4) null,
    BOOL_PROP_1 varchar(1) null,
    BOOL_PROP_2 varchar(1) null,
    primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
    constraint qrtz_simprop_triggers_ibfk_1
        foreign key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) references qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
);

create index IDX_QRTZ_T_C
	on qrtz_triggers (SCHED_NAME, CALENDAR_NAME);

create index IDX_QRTZ_T_G
	on qrtz_triggers (SCHED_NAME, TRIGGER_GROUP);

create index IDX_QRTZ_T_J
	on qrtz_triggers (SCHED_NAME, JOB_NAME, JOB_GROUP);

create index IDX_QRTZ_T_JG
	on qrtz_triggers (SCHED_NAME, JOB_GROUP);

create index IDX_QRTZ_T_NEXT_FIRE_TIME
	on qrtz_triggers (SCHED_NAME, NEXT_FIRE_TIME);

create index IDX_QRTZ_T_NFT_MISFIRE
	on qrtz_triggers (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME);

create index IDX_QRTZ_T_NFT_ST
	on qrtz_triggers (SCHED_NAME, TRIGGER_STATE, NEXT_FIRE_TIME);

create index IDX_QRTZ_T_NFT_ST_MISFIRE
	on qrtz_triggers (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME, TRIGGER_STATE);

create index IDX_QRTZ_T_NFT_ST_MISFIRE_GRP
	on qrtz_triggers (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME, TRIGGER_GROUP, TRIGGER_STATE);

create index IDX_QRTZ_T_N_G_STATE
	on qrtz_triggers (SCHED_NAME, TRIGGER_GROUP, TRIGGER_STATE);

create index IDX_QRTZ_T_N_STATE
	on qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, TRIGGER_STATE);

create index IDX_QRTZ_T_STATE
	on qrtz_triggers (SCHED_NAME, TRIGGER_STATE);

create table sys_area
(
    id char(6) not null
        primary key,
    name varchar(60) null comment '名称',
    parent_id char(6) null comment '父编号',
    short_name varchar(60) null comment '简称',
    level_type int null comment '级别',
    cty_code varchar(10) null comment '城市代码',
    zip_code char(6) null comment '邮编',
    merger_name varchar(90) null comment '详细名称',
    lng varchar(60) null comment '经度',
    lat varchar(60) null comment '维度',
    pinyin varchar(90) null comment '拼音',
    create_user int null comment '创建人',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新人',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '删除状态'
)
    comment '区域';

create table sys_attr
(
    id int auto_increment
        primary key,
    attr_class_id int null comment '附件分类',
    attr_name varchar(100) null comment '附件名称',
    attr_store_type int null comment '附件存储类型',
    attr_location varchar(200) null comment '附件地址',
    create_by int null comment '创建人',
    create_time datetime null comment '创建时间'
)
    comment '附件';

create table sys_attr_class
(
    class_id int auto_increment
        primary key
)
    comment '附件分类';

create table sys_dept
(
    dept_id int auto_increment comment '部门id'
        primary key,
    dept_name varchar(200) null comment '部门名称',
    dept_code varchar(200) null comment '部门编码',
    parent_id int null comment '父id',
    sort_id int null comment '排序字段',
    remark varchar(1000) null comment '备注',
    tenant_id int default 0 null comment '租户id',
    has_children tinyint(1) null comment '是否拥有下级',
    create_user int null comment '创建用户',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '删除标记'
)
    comment '机构部门' charset=utf8;

create table sys_dept_role
(
    dept_id int not null comment '部门id
',
    role_id int not null comment '角色Id',
    primary key (dept_id, role_id)
)
    comment '部门角色关联表' charset=utf8;

create table sys_dict
(
    id int auto_increment comment '字典表主键id'
        primary key,
    dict_code varchar(200) null comment '字典标识',
    dict_name varchar(200) null comment '字典名称',
    dict_value int null comment '字典值',
    parent_id int default 0 null comment '上级ID',
    state tinyint(1) null comment '状态[0：不可用，1：可用]',
    sort_id int null comment '排序字段',
    create_user int null comment '创建用户',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '是否删除-0：否，1：是',
    remarks varchar(500) null comment '备注'
)
    comment '字典表' charset=utf8;

create table sys_log
(
    id bigint auto_increment comment '编号'
        primary key,
    type varchar(24) null comment '日志类型',
    title varchar(1000) default '' null comment '日志标题',
    client_id varchar(64) null comment '服务ID',
    create_by varchar(64) null comment '创建者',
    create_time datetime default CURRENT_TIMESTAMP null comment '创建时间',
    remote_addr varchar(255) not null comment '操作IP地址',
    user_agent varchar(1000) null comment '用户代理',
    request_uri varchar(255) null comment '请求URI',
    method varchar(10) null comment '操作方式',
    params text null comment '操作提交的数据',
    time mediumtext null comment '执行时间',
    exception text null comment '异常信息',
    tenant_id int default 0 null comment '所属租户',
    process_time varchar(200) null comment '请求时长'
)
    comment '日志表' charset=utf8;

create index sys_log_create_by
	on sys_log (create_by);

create index sys_log_create_date
	on sys_log (create_time);

create index sys_log_request_uri
	on sys_log (request_uri);

create index sys_log_type
	on sys_log (type);

create table sys_menu
(
    menu_id int auto_increment
        primary key,
    tenant_id varchar(64) default '0' null comment '租户id',
    parent_id int null comment '父菜单编号',
    menu_name varchar(100) not null comment '菜单名称
',
    menu_path varchar(200) null comment '前端路由地址',
    menu_icon varchar(500) null comment '图标',
    menu_type int null comment '菜单类型（1,一级菜单，2，二级菜单，3,按钮）',
    component varchar(256) null comment '前端组件',
    redirect_url varchar(500) null comment '跳转链接',
    permission_key varchar(128) not null comment '权限标识',
    status tinyint(1) null comment '菜单状态',
    sort_id int null comment '排序字段',
    remake varchar(500) null,
    has_children tinyint(1) null comment '是否拥有下级',
    create_user int null comment '创建用户',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '删除标记'
)
    comment '系统菜单表' charset=utf8;

create table sys_oauth_client_details
(
    id int auto_increment comment '主键'
        primary key,
    client_id varchar(48) not null comment '应用标识',
    resource_ids varchar(256) null comment '资源限定串(逗号分割)',
    client_secret varchar(256) null comment '应用密钥(bcyt) 加密',
    client_secret_str varchar(256) null comment '应用密钥(明文)',
    scope varchar(256) null comment '范围',
    authorized_grant_types varchar(256) null comment '5种oauth授权方式(authorization_code,password,refresh_token,client_credentials)',
    web_server_redirect_uri varchar(256) null comment '回调地址',
    authorities varchar(256) null comment '权限',
    access_token_validity int null comment 'access_token有效期',
    refresh_token_validity int null comment 'refresh_token有效期',
    additional_information varchar(4096) default '{}' null comment '{}',
    autoapprove varchar(256) null comment '是否自动授权 是-true',
    create_user varchar(64) null comment '创建用户',
    create_time datetime null comment '创建时间',
    update_user varchar(64) null comment '更新用户',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '删除标记'
)
    comment 'oauth客户端' charset=utf8;

create table sys_position
(
    id int auto_increment comment '职位id'
        primary key,
    post_name varchar(64) not null comment '职位名称',
    post_code varchar(64) null comment '职位编号',
    post_type int null comment '职位类型字典表(post_type)',
    sort_id int null comment '排序字段',
    create_user int null comment '创建人',
    create_time datetime null comment '创建时间',
    update_user int null comment '跟新人',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '删除状态'
)
    comment '职位表';

create table sys_quartz_job
(
    id int unsigned auto_increment comment '自增主键'
        primary key,
    job_name varchar(50) not null comment '任务名称',
    job_group varchar(50) not null comment '任务分组',
    job_class_name varchar(100) not null comment '执行类',
    cron_expression varchar(100) not null comment 'cron表达式',
    trigger_state varchar(15) not null comment '任务状态',
    old_job_name varchar(50) default '' not null comment '修改之前的任务名称',
    old_job_group varchar(50) default '' not null comment '修改之前的任务分组',
    description varchar(100) not null comment '描述',
    create_user varchar(64) null,
    create_time datetime null,
    update_user varchar(64) null,
    update_time datetime null,
    deleted_flag tinyint(1) null comment '删除标记',
    constraint un_group_name
        unique (job_group, job_name)
)
    comment '定时任务' charset=utf8;

create table sys_role
(
    role_id int auto_increment comment '角色id'
        primary key,
    tenant_id int default 0 not null comment '租户id',
    parent_id int null comment '父编号',
    role_name varchar(100) null comment '角色名称',
    role_key varchar(100) null comment '角色标识',
    role_type int null comment '菜单类型(1，系统角色，2，应用角色)',
    remark varchar(500) null comment '说明',
    has_children tinyint(1) null comment '是否拥有下级',
    data_scope json null comment '数据权限',
    create_user int null comment '创建用户',
    create_time datetime null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null comment '更新时间',
    deleted_flag tinyint(1) default 0 null comment '删除标记',
    sort_id int default 0 null comment '排序字段'
)
    comment '角色表' charset=utf8;

create table sys_role_menu
(
    role_id int not null,
    menu_id int not null,
    primary key (menu_id, role_id)
)
    comment '角色菜单关联表' charset=utf8;

create table sys_user
(
    uid int auto_increment comment '账号id'
        primary key,
    email varchar(30) default '' null comment '邮箱',
    phone varchar(20) default '' null comment '手机号',
    username varchar(30) default '' not null comment '登陆名',
    password varchar(64) default '' not null comment '密码',
    user_type int default 1 null comment '用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）',
    real_name varchar(64) null comment '真实姓名',
    tenant_id bigint null comment '租户id',
    status int default 0 not null comment '状态 1:enable, 0:disable, -1:deleted',
    last_login_time datetime null comment '最后一次登陆时间',
    last_login_ip varchar(64) default '' not null comment '最后一次登陆ip',
    login_num int default 0 not null comment '登录次数',
    avatar varchar(200) null,
    create_ip varchar(64) default '' not null comment '创建ip',
    create_user int null comment '创建用户',
    create_time datetime not null comment '创建时间',
    update_user int null comment '更新用户',
    update_time datetime null,
    deleted_flag tinyint(1) default 0 null comment '删除标记',
    constraint sys_user_email_uindex
        unique (email),
    constraint sys_user_phone_uindex
        unique (phone),
    constraint sys_user_username_uindex
        unique (username)
)
    comment '基础账户表' charset=utf8;

create table sys_user_connection
(
    userId varchar(255) not null,
    providerId varchar(255) not null,
    providerUserId varchar(255) not null,
    providerUnionId varchar(255) null,
    providerSessionKey varchar(255) null,
    rank_num int default 1 not null,
    displayName varchar(255) null,
    profileUrl varchar(512) null,
    imageUrl varchar(512) null,
    accessToken varchar(512) null,
    secret varchar(512) null,
    refreshToken varchar(512) null,
    expireTime bigint null,
    constraint UserConnectionRank
        unique (userId, providerId, rank_num),
    constraint userId
        unique (userId, providerId)
)
    comment '用户社交账号绑定' charset=utf8;

create table sys_user_dept
(
    uid int null,
    dept_id int null comment '部门'
)
    comment '用户部门关联表' charset=utf8;

create table sys_user_position
(
    uid int not null,
    post_id int not null,
    primary key (uid, post_id)
)
    comment '用户职位';

create table sys_user_role
(
    uid int not null comment '用户id',
    role_id int not null comment '角色id',
    primary key (uid, role_id)
)
    comment '用户角色关联表' charset=utf8;

create table testtable
(
    aaa_bbb int auto_increment
        primary key,
    AA date default '2022-01-01' not null
);

