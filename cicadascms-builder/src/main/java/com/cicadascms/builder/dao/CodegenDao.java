package com.cicadascms.builder.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.builder.vo.TableVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CodegenDao extends BaseMapper {

    List<TableVo> findTableList(Page page, @Param("tableName") String tableName);

    List<TableVo> findTableList(@Param("tableName") String tableName);

}
