package com.cicadascms.builder.vo;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.ImageView;

/**
 * TableInfoVo
 *
 * @author Jin
 */
public class TableInfoVo {

    private StringProperty tableName;

    private StringProperty tableComment;

    private StringProperty engine;

    private StringProperty createTime;

    private ImageView imageView;

    public String getTableName() {
        return tableName.get();
    }

    public StringProperty tableNameProperty() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName.set(tableName);
    }

    public String getTableComment() {
        return tableComment.get();
    }

    public StringProperty tableCommentProperty() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment.set(tableComment);
    }

    public String getEngine() {
        return engine.get();
    }

    public StringProperty engineProperty() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine.set(engine);
    }

    public String getCreateTime() {
        return createTime.get();
    }

    public StringProperty createTimeProperty() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime.set(createTime);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public TableInfoVo() {
        this.tableName = new SimpleStringProperty();
        this.tableComment = new SimpleStringProperty();
        this.engine = new SimpleStringProperty();
        this.createTime = new SimpleStringProperty();
    }
}