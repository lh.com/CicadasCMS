package com.cicadascms.builder.vo;

import cn.hutool.core.date.DateUtil;
import javafx.scene.image.ImageView;
import lombok.Data;

import java.util.Date;

/**
 * TableVo
 *
 * @author Jin
 */
@Data
public class TableVo {

    private String tableName;
    private String tableComment;
    private String engine;
    private Date createTime;

    public TableInfoVo toTableInfoModel() {
        TableInfoVo tableInfoVo = new TableInfoVo();
        tableInfoVo.tableNameProperty().set(this.getTableName());
        tableInfoVo.tableCommentProperty().set(this.getTableComment() + "(" + this.getTableName() + ")");
        tableInfoVo.engineProperty().set(this.getEngine());
        tableInfoVo.createTimeProperty().set(DateUtil.format(this.getCreateTime(), "yy/MM/dd HH:mm"));
        tableInfoVo.setImageView(getOtherImageView(20, 20));
        return tableInfoVo;
    }


    private ImageView getOtherImageView(int w, int h) {
        ImageView imageView = new ImageView("/icons/public/IconFile.png");
        imageView.setFitHeight(w);
        imageView.setFitWidth(h);
        return imageView;
    }
}
