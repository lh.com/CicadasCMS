package com.cicadascms.builder.core.config;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.generator.InjectionConfig;

import java.util.Map;


/**
 * 自定义注入配置
 *
 * @author Jin
 */
public class MyFileGenerateConfig extends InjectionConfig {

    @Override
    public void initMap() {

    }

    @Override
    public Map<String, Object> prepareObjectMap(Map<String, Object> objectMap) {
        objectMap.putAll(getMap());
        String entityName = (String) objectMap.get("entity");
        if (entityName.endsWith("DO")) entityName = entityName.replace("DO", "");
        objectMap.put("nonPrefixEntityNameLower", StrUtil.lowerFirst(entityName));
        objectMap.put("nonPrefixEntityNameUpper", StrUtil.upperFirst(entityName));
        return objectMap;
    }
}
