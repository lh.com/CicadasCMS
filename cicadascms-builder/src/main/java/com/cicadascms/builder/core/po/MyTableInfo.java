package com.cicadascms.builder.core.po;

import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 自定义 TableInfo
 *
 * @author Jin
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper=true)
public class MyTableInfo extends TableInfo {

    private String updateDTOName;
    private String inputDTOName;
    private String queryDTOName;
    private String entityVOName;
    private String wrapperName;

}
