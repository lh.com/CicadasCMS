package com.cicadascms.builder.core.config;

import com.baomidou.mybatisplus.generator.config.PackageConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 自定义包设置
 *
 * @author Jin
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper=true)
public class MyPackageConfig extends PackageConfig {

    /**
     * vo 类包名
     */
    private String vo = "vo";

    /**
     * dto 类包名
     */
    private String dto = "dto";

    /**
     * wrapper 包装类包名
     */
    private String wrapper = "wrapper";

}
