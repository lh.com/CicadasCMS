package com.cicadascms.builder.core.fileout;

import com.cicadascms.builder.core.BaseFileOutConfig;
import com.cicadascms.builder.core.constant.MyConstVal;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;


/**
 * 自定义 UI Route 生成配置
 *
 * @author Jin
 */
public class UIRouteFileOutConfig extends BaseFileOutConfig {

    @Override
    public String getTemplatePath() {
        return MyConstVal.TEMPLATE_UI_ROUTER + ".vm";
    }

    @Override
    public String outputFile(TableInfo tableInfo) {
        String path = System.getProperty("user.dir") + "/vue/";
        String fileName = getLowerFirstNonPrefixName(tableInfo.getName(), tableInfo.getEntityName()) + MyConstVal.JS_SUFFIX;
        return joinPath(path,  "ui.route." ) + fileName;
    }

}
