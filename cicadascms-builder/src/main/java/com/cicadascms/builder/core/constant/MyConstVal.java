package com.cicadascms.builder.core.constant;

import com.baomidou.mybatisplus.generator.config.ConstVal;

/**
 * 常量扩展
 *
 * @author Jin
 */
public interface MyConstVal extends ConstVal {

    String DO = "DO";
    String INPUT_DTO = "%sInputDTO";
    String UPDATE_DTO = "%sUpdateDTO";
    String QUERY_DTO = "%sQueryDTO";
    String VO = "VO";
    String WRAPPER = "Wrapper";

    String DTO_PATH = "DTO_Path";
    String VO_PATH = "VO_Path";
    String WRAPPER_PATH = "Wrapper_Path";

    String JS_SUFFIX = ".js";
    String VUE_SUFFIX = ".vue";

    String TEMPLATE_INPUT_DTO = "/templates/input_dto.java";
    String TEMPLATE_UPDATE_DTO = "/templates/update_dto.java";
    String TEMPLATE_QUERY_DTO = "/templates/query_dto.java";
    String TEMPLATE_VO = "/templates/vo.java";
    String TEMPLATE_WRAPPER = "/templates/wrapper.java";

    String TEMPLATE_UI_API = "/templates/api.js";
    String TEMPLATE_UI_ROUTER = "/templates/router.js";
    String TEMPLATE_UI_VIEW = "/templates/view.vue";

}
