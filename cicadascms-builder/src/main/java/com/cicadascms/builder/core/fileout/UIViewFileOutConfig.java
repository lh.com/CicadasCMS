package com.cicadascms.builder.core.fileout;

import com.cicadascms.builder.core.BaseFileOutConfig;
import com.cicadascms.builder.core.constant.MyConstVal;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;

/**
 * 自定义 UI VIEW 生成配置
 *
 * @author Jin
 */
public class UIViewFileOutConfig extends BaseFileOutConfig {

    @Override
    public String getTemplatePath() {
        return MyConstVal.TEMPLATE_UI_VIEW + ".vm";
    }

    @Override
    public String outputFile(TableInfo tableInfo) {
        String path = System.getProperty("user.dir") + "/vue/";
        String fileName = "index" + MyConstVal.VUE_SUFFIX;
        String moduleName = getLowerFirstNonPrefixName(tableInfo.getName(), tableInfo.getEntityName());
        return joinPath(path, "ui.views." + moduleName + ".") + fileName;
    }
}
