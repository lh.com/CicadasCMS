package com.cicadascms.builder.core.config;

import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * 自定义数据源配置
 *
 * @author Jin
 */
@AllArgsConstructor
public class MyDataSourceConfig extends DataSourceConfig {

    private DataSource dataSource;

    @SneakyThrows
    @Override
    public Connection getConn() {
        return dataSource.getConnection();
    }
}
