package com.cicadascms.builder.core.config;

import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 配置扩展
 *
 * @author Jin
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MyGlobalConfig extends GlobalConfig {

    //DO文件生成目录
    private String doFileOutputDir;
    //DAO文件生成目录
    private String daoFileOutputDir;
    //业务逻辑文件生成目录
    private String logicFileOutputDir;

}
