package com.cicadascms.builder.core;

import de.felixroske.jfxsupport.SplashScreen;
import javafx.scene.Parent;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import lombok.SneakyThrows;


/**
 * DefaultSplashScreen
 *
 * @author Jin
 */
public class DefaultSplashScreen extends SplashScreen {

        @SneakyThrows
        @Override
        public Parent getParent() {
            ImageView imageView = new ImageView("/icons/bg.png");
            imageView.setFitHeight(1200);
            imageView.setFitWidth(1000);
            imageView.setPreserveRatio(true);
            ProgressBar splashProgressBar = new ProgressBar();
            splashProgressBar.setPrefWidth(imageView.getFitWidth());
            splashProgressBar.setStyle("-fx-accent: #5576BD");
            VBox vbox = new VBox();
            vbox.getChildren().addAll(imageView, splashProgressBar);
            return vbox;
        }

        @Override
        public boolean visible() {
            return true;
        }
    }