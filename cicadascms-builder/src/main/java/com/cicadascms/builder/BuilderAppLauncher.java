package com.cicadascms.builder;

import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.cicadascms.builder.core.DefaultSplashScreen;
import com.cicadascms.builder.view.IndexView;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;


/**
 * 代码生成器启动类
 *
 * @author Jin
 */
@MapperScan(value = "com.cicadascms.builder.dao")
@SpringBootApplication
public class BuilderAppLauncher extends AbstractJavaFxApplicationSupport {


    public static void main(String[] args) {
        runApp(args);
    }

    @Override
    public void beforeInitialView(Stage stage, ConfigurableApplicationContext ctx) {
        super.beforeInitialView(stage, ctx);
        stage.setTitle("CicadasCMS");
        stage.setResizable(false);
        stage.initStyle(StageStyle.TRANSPARENT);
    }

    @Bean
    public PaginationInnerInterceptor paginationInterceptor() {
        return new PaginationInnerInterceptor();
    }

    public static void runApp(String[] args) {
        try {
            ServerSocketChannel
                    .open()
                    .socket()
                    .bind(new InetSocketAddress(65478));
            launch(BuilderAppLauncher.class, IndexView.class, new DefaultSplashScreen(), args);
        } catch (Exception e) {
            showErrorAlert("请不要重复运行！");
        }
    }


    public static void showErrorAlert(String msg) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.WARNING, msg);
            alert.showAndWait();
        });
    }

}
