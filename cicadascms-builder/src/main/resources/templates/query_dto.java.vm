package ${package.QueryDTO};
#foreach($pkg in ${table.importPackages})
#if("$!pkg"!= "com.baomidou.mybatisplus.annotation.IdType" && "$!pkg"!= "com.baomidou.mybatisplus.annotation.TableId")
import ${pkg};
#end
#end
import ${package.Entity}.${entity};
import cn.hutool.core.util.StrUtil;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.common.constant.Constant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.io.Serializable;

/**
 * <p>
 * ${nonPrefixEntityNameUpper}QueryDTO对象
 * $!{table.comment}
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="${nonPrefixEntityNameUpper}QueryDTO对象")
public class ${nonPrefixEntityNameUpper}QueryDTO extends BaseDTO<${nonPrefixEntityNameUpper}QueryDTO, ${entity}>  implements Serializable {

    #if(${entitySerialVersionUID})
    private static final long serialVersionUID = 1L;
    #end

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;

    #foreach($field in ${table.fields})
    #set($sort=$foreach.count + 2)
    #if(!${field.keyFlag})
    #set($sort=$sort - 1)
    #if("$!field.comment" != "")
    /**
    * ${field.comment}
    */
    @ApiModelProperty(value = "${sort}-${field.comment}" )
    #end
    private ${field.propertyType} ${field.propertyName};
    #end
    #end

    public Page<${entity}> page() {
        Page<${entity}> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<${nonPrefixEntityNameUpper}QueryDTO, ${entity}> converter = new Converter<${nonPrefixEntityNameUpper}QueryDTO, ${entity}>() {
        @Override
        public ${entity} doForward(${nonPrefixEntityNameUpper}QueryDTO ${nonPrefixEntityNameLower}QueryDTO) {
            return WarpsUtils.copyTo(${nonPrefixEntityNameLower}QueryDTO, ${entity}.class);
        }

        @Override
        public ${nonPrefixEntityNameUpper}QueryDTO doBackward(${entity} ${nonPrefixEntityNameLower}) {
            return WarpsUtils.copyTo(${nonPrefixEntityNameLower}, ${nonPrefixEntityNameUpper}QueryDTO.class);
        }
    };

    @Override
    public ${entity} convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ${nonPrefixEntityNameUpper}QueryDTO convertFor(${entity} ${nonPrefixEntityNameLower}) {
        return converter.doBackward(${nonPrefixEntityNameLower});
    }
}
