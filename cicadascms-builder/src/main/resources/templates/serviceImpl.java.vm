package ${package.ServiceImpl};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import java.io.Serializable;
import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${package.InputDTO}.${table.inputDTOName};
import ${package.UpdateDTO}.${table.updateDTOName};
import ${package.QueryDTO}.${table.queryDTOName};
#if(${genWrapperFile})
import ${package.Wrapper}.${table.wrapperName};
#end
import org.springframework.stereotype.Service;

/**
 * <p>
 * $!{table.comment} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service("${nonPrefixEntityNameLower}Service")
#if(${kotlin})
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
#else
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

    @Override
    public R page(${nonPrefixEntityNameUpper}QueryDTO ${nonPrefixEntityNameLower}QueryDTO) {
        ${entity} ${nonPrefixEntityNameLower} = ${nonPrefixEntityNameLower}QueryDTO.convertToEntity();
        Page page = baseMapper.selectPage(${nonPrefixEntityNameLower}QueryDTO.page(), getLambdaQueryWrapper().setEntity(${nonPrefixEntityNameLower}));
        #if(${genWrapperFile})
        return R.ok(${nonPrefixEntityNameUpper}Wrapper.newBuilder().pageVO(page));
        #else
        return R.ok(page);
        #end
    }

    @Override
    public R save(${nonPrefixEntityNameUpper}InputDTO ${nonPrefixEntityNameLower}InputDTO) {
        ${entity} ${nonPrefixEntityNameLower} = ${nonPrefixEntityNameLower}InputDTO.convertToEntity();
        baseMapper.insert(${nonPrefixEntityNameLower});
        return R.ok(true);
    }

    @Override
    public R update(${nonPrefixEntityNameUpper}UpdateDTO ${nonPrefixEntityNameLower}UpdateDTO) {
        ${entity} ${nonPrefixEntityNameLower} = ${nonPrefixEntityNameLower}UpdateDTO.convertToEntity();
        baseMapper.updateById(${nonPrefixEntityNameLower});
        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        ${entity} ${nonPrefixEntityNameLower} = baseMapper.selectById(id);
        #if(${genWrapperFile})
        return R.ok(${nonPrefixEntityNameUpper}Wrapper.newBuilder().entityVO(${nonPrefixEntityNameLower}));
        #else
        return R.ok(${nonPrefixEntityNameLower});
        #end

    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

}
#end
