package com.cicadascms.logger.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * LogTypeEnum
 *
 * @author Jin
 */
@Getter
@AllArgsConstructor
public enum LogTypeEnum {

    操作日志("OPERA", "操作日志"),
    错误日志("ERROR", "错误日志");

    private String type;
    private String description;
}