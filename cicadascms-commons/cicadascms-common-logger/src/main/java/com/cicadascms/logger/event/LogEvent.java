package com.cicadascms.logger.event;

import org.springframework.context.ApplicationEvent;

/**
 * LogEvent
 *
 * @author Jin
 */
public class LogEvent extends ApplicationEvent {

    public LogEvent(Object source) {
        super(source);
    }
}
