package com.cicadascms.logger.annotation;

import java.lang.annotation.*;


/**
 * 接口日志注解
 *
 * @author Jin
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ApiLog {

    String value() default "";

}
