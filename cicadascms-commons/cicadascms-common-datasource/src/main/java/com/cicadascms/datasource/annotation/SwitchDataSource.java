package com.cicadascms.datasource.annotation;

import java.lang.annotation.*;

/**
 * 动态数据源切换注解
 *
 * @author Jin
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SwitchDataSource {
	//数据库名称
    String name();
}