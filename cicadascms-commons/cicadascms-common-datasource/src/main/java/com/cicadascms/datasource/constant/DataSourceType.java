package com.cicadascms.datasource.constant;

/**
 * DataSourceType
 *
 * @author Jin
 */
public enum DataSourceType {
    FIRST,

    SECOND,

    THREE;
}