package com.cicadascms.datasource.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.cicadascms.datasource.aspect.DataSourceAspect;
import com.cicadascms.datasource.constant.DataSourceType;
import com.cicadascms.datasource.datasorce.DynamicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 多数据源配置
 *
 * @author Jin
 */
@Configuration
@Import(DataSourceAspect.class)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class DataSourceConfig {

    @Value("${spring.datasource.druid.second.enabled:false}")
    private Boolean secondDataSourceEnable;
    @Value("${spring.datasource.druid.three.enabled:false}")
    private Boolean threeDataSourceEnable;

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.druid.first")
    public DataSource firstDataSource() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.druid.second")
    @ConditionalOnProperty(prefix = "spring.datasource.druid.second", name = "enabled", havingValue = "true")
    public DataSource secondDataSource() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.druid.three")
    @ConditionalOnProperty(prefix = "spring.datasource.druid.three", name = "enabled", havingValue = "true")
    public DataSource thereDataSource() {
        return DruidDataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "dataSource")
    public DynamicDataSource dataSource() {
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DataSourceType.FIRST.name(), firstDataSource());
        if (secondDataSourceEnable) {
            targetDataSources.put(DataSourceType.SECOND.name(), secondDataSource());
        }
        if (threeDataSourceEnable) {
            targetDataSources.put(DataSourceType.THREE.name(), thereDataSource());
        }
        return new DynamicDataSource(firstDataSource(), targetDataSources);
    }

    @Bean(name = "transactionManager")
    public DataSourceTransactionManager transactionManager(@Qualifier("dataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
