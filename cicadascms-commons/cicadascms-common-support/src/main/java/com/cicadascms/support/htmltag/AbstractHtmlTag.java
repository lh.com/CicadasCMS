package com.cicadascms.support.htmltag;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.SiteDO;
import org.beetl.core.tag.GeneralVarTagBinding;

import javax.servlet.http.HttpServletRequest;

/**
 * AbstractHtmlTag
 *
 * @author Jin
 */
public abstract class AbstractHtmlTag extends GeneralVarTagBinding {

    protected Integer getAttrToInt(String name) {
        if (Fn.isNotNull(this.getAttributeValue(name)))
            return Integer.parseInt((String) this.getAttributeValue(name));
        return null;
    }

    protected Long getAttrToLong(String name) {
        if (Fn.isNotNull(this.getAttributeValue(name)))
            return Long.parseLong((String) this.getAttributeValue(name));
        return null;
    }

    protected String getAttrToString(String name) {
        if (Fn.isNotNull(this.getAttributeValue(name)))
            return (String) this.getAttributeValue(name);
        return null;
    }


    protected IPage getContentPage() {
        if (Fn.isNotNull(this.getAttributeValue("page")))
            return (IPage) this.getAttributeValue("page");
        return null;
    }

    protected HttpServletRequest getHttpServletRequest() {
        return (HttpServletRequest) ctx.getGlobal("request");
    }

    protected String getShortTitle(String title, Integer titleLen) {
        if (title.length() > titleLen) {
            return title.substring(0, titleLen) + "...";
        }
        return title;
    }

    protected SiteDO getCurrentWebSite() {
        return (SiteDO) getHttpServletRequest().getAttribute("site");
    }
}
