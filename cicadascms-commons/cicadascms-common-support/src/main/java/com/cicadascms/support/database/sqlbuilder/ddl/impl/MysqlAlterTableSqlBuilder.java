package com.cicadascms.support.database.sqlbuilder.ddl.impl;

import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.support.database.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.ddl.AlterTableSqlBuilder;
import org.apache.commons.lang3.StringUtils;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;

public class MysqlAlterTableSqlBuilder extends AbstractSqlBuilder<MysqlAlterTableSqlBuilder> implements AlterTableSqlBuilder<MysqlAlterTableSqlBuilder> {

    //编辑表
    private final static String ALTER_TABLE_BEGIN = "ALTER TABLE `{table}`";

    private final static String CHANGE_COLUMN = " CHANGE COLUMN ";

    private final static String ADD_COLUMN = " ADD COLUMN ";

    private final static String DROP_COLUMN = " DROP COLUMN ";

    @Override
    public MysqlAlterTableSqlBuilder changeColumn(String columnName, String newColumnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull) {
        MysqlColumnTypeEnum columnTypeEnum = MysqlColumnTypeEnum.checkAndGet(columnType);
        StringBuilder sqlBody = new StringBuilder();
        sqlBody.append(CHANGE_COLUMN);
        if (!columnTypeEnum.getIsNotLength()) {
            sqlBody.append(" `" + columnName + "` " + " `" + newColumnName + "` " + columnType + "(" + length + ")");
        } else {
            sqlBody.append(" `" + columnName + "` " + " `" + newColumnName + "` " + columnType);
        }
        if (isNotNull) {
            sqlBody.append(" NOT null");
        }
        if (autoIncrement) {
            if (columnTypeEnum.getAutoIncrement()) {
                if (!isNotNull) {
                    sqlBody.append(" NOT null");
                }
                sqlBody.append(" AUTO_INCREMENT ;");
            }
        } else {
            if (!isNotNull) {
                sqlBody.append(" NULL ");
            }
            if (!columnTypeEnum.getIsNotDefaultValue()) {
                sqlBody.append(" DEFAULT " + (Fn.isEmpty(defaultValue) ? null : "'" + defaultValue + "'") + ";");
            }

        }
        setSqlBody(sqlBody.toString());
        return this;
    }

    @Override
    public MysqlAlterTableSqlBuilder addColumn(String columnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull, boolean isPrimaryKey) {
        MysqlColumnTypeEnum columnTypeEnum = MysqlColumnTypeEnum.checkAndGet(columnType);
        StringBuilder sqlBody = new StringBuilder();
        sqlBody.append(ADD_COLUMN);
        if (!columnTypeEnum.getIsNotLength()) {
            //判断字段是否允许设置长度
            sqlBody.append(" `" + columnName + "` " + columnType + "(" + length + ")");
        } else {
            sqlBody.append(" `" + columnName + "` " + columnType);
        }
        if (isNotNull) {
            //判断字段是否不为空
            sqlBody.append(" NOT NULL");
        }
        if (autoIncrement) {
            //判断字段类型是否支持自动增长
            if (columnTypeEnum.getAutoIncrement()) {
                if (!isNotNull) {
                    sqlBody.append(" NOT NULL");
                }
                sqlBody.append(" AUTO_INCREMENT");
            }
        } else {
            if (!isNotNull) {
                //判断字段是否允许为空
                sqlBody.append(" NULL ");
            }
            if (!columnTypeEnum.getIsNotDefaultValue()) {
                //判断字段类型是否为数值型
                if (columnTypeEnum.getAutoIncrement() && !StringUtils.isNumeric(defaultValue)) {
                    throw new ServiceException("数值型字段不能设置默认值为字符");
                }
                if (columnTypeEnum.equals(MysqlColumnTypeEnum.DATE) || columnTypeEnum.equals(MysqlColumnTypeEnum.DATE_TIME)) {

                }
                sqlBody.append(" DEFAULT " + (Fn.isEmpty(defaultValue) ? null : "'" + defaultValue + "'"));
            }
        }
        sqlBody.append(";");
        sqlBody.append("\n");
        if (isPrimaryKey) {
            sqlBody.append("alter table `" + getTableName() + "`drop primary key;");
            sqlBody.append("\n");
            //是否为主键
            sqlBody.append("alter table `" + getTableName() + "` add constraint " + getTableName() + "_pk " +
                    "primary key (" + columnName + ");");
        }
        setSqlBody(sqlBody.toString());
        return this;
    }

    @Override
    public MysqlAlterTableSqlBuilder dropColumn(String columnName, boolean isPrimaryKey) {
        StringBuilder sqlBody = new StringBuilder();
        if (isPrimaryKey) {
            sqlBody.append("alter table `" + getTableName() + "`drop primary key;");
            sqlBody.append("\n");
        }
        sqlBody.append(DROP_COLUMN + "`" + columnName + "`");
        setSqlBody(sqlBody.toString());
        return this;
    }


    @Override
    public MysqlAlterTableSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        setSqlHead(ALTER_TABLE_BEGIN);
        setSqlBody("");
        setSqlFoot("");
        return this;
    }

    @Override
    public String buildSql() {
        return build();
    }
}
