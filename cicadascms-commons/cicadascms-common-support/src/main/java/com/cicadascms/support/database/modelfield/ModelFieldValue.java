package com.cicadascms.support.database.modelfield;


import com.cicadascms.support.database.modelfield.value.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * BaseModelFieldType
 *
 * @author Jin
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CheckboxValue.class, name = "checkbox"),
        @JsonSubTypes.Type(value = DateTimeValue.class, name = "dateTime"),
        @JsonSubTypes.Type(value = EditorValue.class, name = "editor"),
        @JsonSubTypes.Type(value = FileUploadValue.class, name = "fileUpload"),
        @JsonSubTypes.Type(value = ImgUploadValue.class, name = "imgUpload"),
        @JsonSubTypes.Type(value = InputValue.class, name = "input"),
        @JsonSubTypes.Type(value = MultiImgUploadValue.class, name = "multiImgUpload"),
        @JsonSubTypes.Type(value = SelectValue.class, name = "select"),
        @JsonSubTypes.Type(value = TextAreaValue.class, name = "textarea"),
        @JsonSubTypes.Type(value = RadioValue.class, name = "radio"),
        @JsonSubTypes.Type(value = JsonValue.class, name = "JSON")
})
public interface ModelFieldValue<T>{

    @JsonIgnore
    T getValue();

    String getType();

}
