package com.cicadascms.support.database.modelfield.rule;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class InputRule implements ModelFieldRule {

    private String name = ModelFieldTypeEnum.INPUT_NAME;

    private Boolean require = false;

    private String message = "请输入文本！";

    private Integer wordLimit = 20;

    private String defaultValue = "";


}
