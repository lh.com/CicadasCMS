package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.CheckboxRule;
import com.cicadascms.support.database.modelfield.value.CheckboxValue;
import lombok.Getter;
import lombok.Setter;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class CheckboxProp implements ModelFieldProp<CheckboxRule, CheckboxValue> {
    private String name = ModelFieldTypeEnum.CHECKBOX_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.toString();
    private Integer length = 64;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;
    private CheckboxRule rule = new CheckboxRule();
    private CheckboxValue value = new CheckboxValue();
}
