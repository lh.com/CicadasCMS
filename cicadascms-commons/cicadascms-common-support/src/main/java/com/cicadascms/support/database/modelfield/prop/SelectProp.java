package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.RadioRule;
import com.cicadascms.support.database.modelfield.rule.SelectRule;
import com.cicadascms.support.database.modelfield.value.RadioValue;
import com.cicadascms.support.database.modelfield.value.SelectValue;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * DateTimeType
 *
 * @author Jin
 */
@Getter
@Setter
public class SelectProp implements ModelFieldProp<SelectRule, SelectValue> {
    private String name = ModelFieldTypeEnum.SELECT_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 64;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private SelectRule rule = new SelectRule();
    private SelectValue value = new SelectValue();

}
