package com.cicadascms.support.database.enums;

import com.cicadascms.common.base.BaseEnum;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.prop.*;

import java.util.Arrays;

public enum ModelFieldTypeEnum implements BaseEnum<Integer> {

    文本框(1, ModelFieldTypeEnum.INPUT_NAME, new InputProp()),
    单选框(2, ModelFieldTypeEnum.RADIO_NAME, new RadioProp()),
    多选框(3, ModelFieldTypeEnum.CHECKBOX_NAME, new CheckboxProp()),
    菜单下拉框(4, ModelFieldTypeEnum.SELECT_NAME, new SelectProp()),
    日期输入框(5, ModelFieldTypeEnum.DATE_TIME_NAME, new DateTimeProp()),
    文件上传(6, ModelFieldTypeEnum.FILE_UPLOAD_NAME, new FileUploadProp()),
    单图上传(7, ModelFieldTypeEnum.IMG_UPLOAD_NAME, new ImgUploadProp()),
    多图上传(8, ModelFieldTypeEnum.MULTI_IMG_UPLOAD_NAME, new MultiImgUploadProp()),
    富文本编辑器(9, ModelFieldTypeEnum.EDITOR_NAME, new EditorProp()),
    多行文本(10, ModelFieldTypeEnum.TEXTAREA_NAME, new TextAreaProp()),
    JSON(11, ModelFieldTypeEnum.JSON_NAME, new JsonProp());

    public static final String INPUT_NAME = "input";
    public static final String RADIO_NAME = "radio";
    public static final String CHECKBOX_NAME = "checkbox";
    public static final String SELECT_NAME = "select";
    public static final String DATE_TIME_NAME = "dateTime";
    public static final String FILE_UPLOAD_NAME = "fileUpload";
    public static final String IMG_UPLOAD_NAME = "imgUpload";
    public static final String MULTI_IMG_UPLOAD_NAME = "multiImgUpload";
    public static final String EDITOR_NAME = "editor";
    public static final String TEXTAREA_NAME = "textarea";
    public static final String JSON_NAME = "JSON";

    private Integer code;
    private String text;
    private ModelFieldProp modelFieldProp;

    ModelFieldTypeEnum(Integer code, String text, ModelFieldProp modelFieldProp) {
        this.code = code;
        this.text = text;
        this.modelFieldProp = modelFieldProp;
    }

    public String getText() {
        return text;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    public ModelFieldProp getModelFieldProp() {
        return modelFieldProp;
    }

    public static ModelFieldTypeEnum checkAndGet(Integer code) {
        return Arrays.stream(ModelFieldTypeEnum.values())
                .parallel()
                .filter(m -> Fn.equal(m.getCode(), code))
                .findFirst()
                .orElseThrow(new ServiceException("未找到支持的模型！"));
    }
}