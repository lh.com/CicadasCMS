package com.cicadascms.support.database.sqlbuilder.dml.impl;

import com.cicadascms.support.database.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.InsertDataSqlBuilder;

import java.util.ArrayList;
import java.util.List;


public class MysqlInsertDataSqlBuilder extends AbstractSqlBuilder<MysqlInsertDataSqlBuilder> implements InsertDataSqlBuilder<MysqlInsertDataSqlBuilder> {

    private final static String INSERT_DATA_BEGIN = "insert into `{table}` set ";

    @Override
    public MysqlInsertDataSqlBuilder insert(List<String> fields, List<Object> paramValues) {
        StringBuilder sqlBody = new StringBuilder();
        for (int i = 0; i < fields.size(); i++) {
            sqlBody.append(" `").append(fields.get(i)).append("`= ");
            Object paramValue = paramValues.get(i);
            if (paramValue instanceof String) {
                sqlBody.append("'");
                sqlBody.append(paramValues.get(i));
                sqlBody.append("'");
            } else {
                sqlBody.append(paramValue);
            }
            if (i >= fields.size() - 1) {
                sqlBody.append("; ");
            } else {
                sqlBody.append(", ");
            }
        }
        setSqlBody(sqlBody.toString());
        return this;
    }

    @Override
    public MysqlInsertDataSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        setSqlHead(INSERT_DATA_BEGIN);
        return this;
    }


    public static void main(String arg[]) {
        MysqlInsertDataSqlBuilder insertDataSqlBuilder = new MysqlInsertDataSqlBuilder();
        List<String> fields = new ArrayList<>();
        fields.add("attr_class_id");
        fields.add("attr_name");
        fields.add("attr_store_type");
        fields.add("attr_location");
        List<Object> paramValues = new ArrayList<>();
        paramValues.add(1);
        paramValues.add("111");
        paramValues.add(3);
        paramValues.add("44444444444444");
        paramValues.add(5);

        String sql = insertDataSqlBuilder.tableName("sys_attr").insert(fields, paramValues).build();

        sql = sql.substring(0, sql.length() - 1);
        System.out.println(sql);
    }

    @Override
    public String buildSql() {
        return build();
    }
}
