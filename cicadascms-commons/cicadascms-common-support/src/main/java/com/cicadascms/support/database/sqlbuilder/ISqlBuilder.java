package com.cicadascms.support.database.sqlbuilder;


/**
 * ISqlBuilder
 *
 * @author Jin
 */
public interface ISqlBuilder {

    String buildSql();

}
