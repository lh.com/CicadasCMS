package com.cicadascms.support.database.sqlbuilder.ddl;


import com.cicadascms.support.database.sqlbuilder.ISqlBuilder;

/**
 * @author Jin
 * @date 2021-01-31 19:11:43
 * @description: 删除数据表
 */
public interface DropTableSqlBuilder<T extends DropTableSqlBuilder> extends ISqlBuilder {

    T drop();

    T tableName(String tableName);

}
