package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class CheckboxValue implements ModelFieldValue<String[]> {
    private String type = ModelFieldTypeEnum.CHECKBOX_NAME;

    private String[] checkList = new String[]{};

    @Override
    public String[] getValue() {
        return checkList;
    }
}
