package com.cicadascms.support.storage;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.auth.CredentialsProvider;
/**
 * 阿里云 oss
 *
 * @author Jin
 */
public class AliYunStorage extends OSSClient {

//    @Getter
//    @Setter(value = AccessLevel.PRIVATE)
//    private AliYunProperties.OssProperties properties;
//
//    public static AliYunOSSClient getInstance(String accessKeyId, String secretAccessKey, AliYunProperties.OssProperties properties) {
//        AliYunOSSClient aliYunOSSClient = new AliYunOSSClient(properties.getEndpoint(), accessKeyId, secretAccessKey);
//        aliYunOSSClient.setProperties(properties);
//        return aliYunOSSClient;
//    }

    public AliYunStorage(String endpoint, String accessKeyId, String secretAccessKey) {
        super(endpoint, accessKeyId, secretAccessKey);
    }

    public AliYunStorage(String endpoint, String accessKeyId, String secretAccessKey, String securityToken) {
        super(endpoint, accessKeyId, secretAccessKey, securityToken);
    }

    public AliYunStorage(String endpoint, String accessKeyId, String secretAccessKey, ClientConfiguration config) {
        super(endpoint, accessKeyId, secretAccessKey, config);
    }

    public AliYunStorage(String endpoint, String accessKeyId, String secretAccessKey, String securityToken, ClientConfiguration config) {
        super(endpoint, accessKeyId, secretAccessKey, securityToken, config);
    }

    public AliYunStorage(String endpoint, CredentialsProvider credsProvider) {
        super(endpoint, credsProvider);
    }

    public AliYunStorage(String endpoint, CredentialsProvider credsProvider, ClientConfiguration config) {
        super(endpoint, credsProvider, config);
    }
}
