package com.cicadascms.support.database.modelfield.rule;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

/**
 * TestAreaRule
 *
 * @author Jin
 */
@Getter
@Setter
public class TextAreaRule implements ModelFieldRule {

    private String name = ModelFieldTypeEnum.TEXTAREA_NAME;

    private Boolean require = false;

    private String message = "请输入文本！";

    private Integer wordLimit = 20;

}
