package com.cicadascms.support.database.modelfield;

import com.cicadascms.support.database.modelfield.prop.*;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * BaseModelFieldType
 *
 * @author Jin
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "name", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CheckboxProp.class, name = "checkbox"),
        @JsonSubTypes.Type(value = DateTimeProp.class, name = "dateTime"),
        @JsonSubTypes.Type(value = EditorProp.class, name = "editor"),
        @JsonSubTypes.Type(value = FileUploadProp.class, name = "fileUpload"),
        @JsonSubTypes.Type(value = ImgUploadProp.class, name = "imgUpload"),
        @JsonSubTypes.Type(value = InputProp.class, name = "input"),
        @JsonSubTypes.Type(value = MultiImgUploadProp.class, name = "multiImgUpload"),
        @JsonSubTypes.Type(value = SelectProp.class, name = "select"),
        @JsonSubTypes.Type(value = TextAreaProp.class, name = "textarea"),
        @JsonSubTypes.Type(value = RadioProp.class, name = "radio"),
        @JsonSubTypes.Type(value = JsonProp.class, name = "JSON")
})
public interface ModelFieldProp<R extends ModelFieldRule, V extends ModelFieldValue> {

    R getRule();

    V getValue();

    String getName();

    String getColumnType();

    Integer getLength();

    Boolean getAutoIncrement();

    String getDefaultValue();

    Boolean getNotNull();

    Boolean getPrimaryKey();

}
