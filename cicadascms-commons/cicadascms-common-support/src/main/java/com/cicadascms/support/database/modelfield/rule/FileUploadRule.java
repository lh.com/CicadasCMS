package com.cicadascms.support.database.modelfield.rule;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class FileUploadRule implements ModelFieldRule {
    private String name = ModelFieldTypeEnum.FILE_UPLOAD_NAME;

    private String accept = "*.*";

    private Boolean require = false;

    private String message = "请选择文件！";

    private Integer wordLimit = 20;


}
