package com.cicadascms.support.database.modelfield.rule;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

/**
 * JsonRule
 *
 * @author Jin
 */
@Getter
@Setter
public class JsonRule implements ModelFieldRule {

    private String name = ModelFieldTypeEnum.JSON_NAME;

    private Boolean require = false;

    private String message = "请输入文本！";

    private Integer wordLimit = 20;

}
