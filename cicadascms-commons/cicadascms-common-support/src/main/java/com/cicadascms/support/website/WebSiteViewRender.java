package com.cicadascms.support.website;

import com.cicadascms.common.utils.ControllerUtil;

/**
 * @author Jin
 * @date 2021-02-03 20:11:26
 * @description: TODO
 */
public class WebSiteViewRender {

    private final static String DEFAULT_TEMPLATE_PATH = "www";

    public static String indexViewRender(String templateDir, String mobileTemplateDir, String indexViewName) {
        if (ControllerUtil.isMobile()) {
            return DEFAULT_TEMPLATE_PATH + mobileTemplateDir + indexViewName;
        }
        return DEFAULT_TEMPLATE_PATH + templateDir + indexViewName;
    }

    public static String channelViewRender(String templateDir, String mobileTemplateDir, String channelViewName) {
        if (ControllerUtil.isMobile()) {
            return DEFAULT_TEMPLATE_PATH + mobileTemplateDir + channelViewName;
        }
        return DEFAULT_TEMPLATE_PATH + templateDir + channelViewName;
    }

    public static String contentViewRender(String templateDir, String mobileTemplateDir, String contentViewName) {

        if (ControllerUtil.isMobile()) {
            return DEFAULT_TEMPLATE_PATH + mobileTemplateDir + contentViewName;
        }
        return DEFAULT_TEMPLATE_PATH + templateDir + contentViewName;
    }


}
