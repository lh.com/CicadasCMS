package com.cicadascms.support.database.sqlbuilder.dml;

import com.cicadascms.support.database.sqlbuilder.ISqlBuilder;


/**
 * @author Jin
 * @date 2021-02-01 21:28:13
 * @description: 删除表数据
 */
public interface DeleteDataSqlBuilder<T extends DeleteDataSqlBuilder> extends ISqlBuilder {

    T delete(String conditionField, Object conditionValue);

    T tableName(String tableName);

}
