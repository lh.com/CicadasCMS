package com.cicadascms.support.database.modelfield.rule;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class DateTimeRule implements ModelFieldRule {

    private String name = ModelFieldTypeEnum.DATE_TIME_NAME;

    private String dateFormat = "yyyy-MM-dd HH:mm:ss";

    private Boolean require = false;

    private String message = "请输入日期！";

    private Integer wordLimit = 20;


}
