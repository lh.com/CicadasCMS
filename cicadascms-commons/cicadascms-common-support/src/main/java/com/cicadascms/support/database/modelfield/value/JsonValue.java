package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.common.func.Fn;
import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JsonValue
 *
 * @author Jin
 */
@Getter
@Setter
public class JsonValue implements ModelFieldValue<List<Map<String, Object>>> {
    private String type = ModelFieldTypeEnum.JSON_NAME;
    private List<Map<String, Object>> jsonList = new ArrayList<>();

    public List<Map<String, Object>> getJsonList() {
        if (Fn.isEmpty(jsonList)) {
            Map<String, Object> demo = new HashMap<>();
            demo.put("key", "value");
            jsonList.add(demo);
        }
        return jsonList;
    }

    @Override
    public List<Map<String, Object>> getValue() {
        return jsonList;
    }
}
