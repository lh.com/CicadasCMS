package com.cicadascms.support.database.modelfield.value;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import lombok.Getter;
import lombok.Setter;

/**
 * DateTimeType
 *
 * @author Jin
 */
@Getter
@Setter
public class SelectValue implements ModelFieldValue<String[]> {

    private String type = ModelFieldTypeEnum.SELECT_NAME;

    private String[] selectedValue = new String[]{};

    @Override
    public String[] getValue() {
        return selectedValue;
    }
}
