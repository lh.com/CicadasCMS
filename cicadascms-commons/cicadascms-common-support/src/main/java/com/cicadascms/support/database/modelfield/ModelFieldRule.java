package com.cicadascms.support.database.modelfield;

import com.cicadascms.support.database.modelfield.rule.*;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * ModelFieldRule
 *
 * @author Jin
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "name", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CheckboxRule.class, name = "checkbox"),
        @JsonSubTypes.Type(value = DateTimeRule.class, name = "dateTime"),
        @JsonSubTypes.Type(value = EditorRule.class, name = "editor"),
        @JsonSubTypes.Type(value = FileUploadRule.class, name = "fileUpload"),
        @JsonSubTypes.Type(value = ImgUploadRule.class, name = "imgUpload"),
        @JsonSubTypes.Type(value = InputRule.class, name = "input"),
        @JsonSubTypes.Type(value = MultiImgUploadRule.class, name = "multiImgUpload"),
        @JsonSubTypes.Type(value = SelectRule.class, name = "select"),
        @JsonSubTypes.Type(value = TextAreaRule.class, name = "text"),
        @JsonSubTypes.Type(value = RadioRule.class, name = "radio"),
        @JsonSubTypes.Type(value = JsonRule.class, name = "JSON")
})
public interface ModelFieldRule {

    String getName();

    Boolean getRequire();

    String getMessage();

    Integer getWordLimit();

}
