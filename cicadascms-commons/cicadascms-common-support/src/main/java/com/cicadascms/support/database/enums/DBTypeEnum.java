package com.cicadascms.support.database.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * DBTypeEnum
 *
 * @author Jin
 */
@Getter
@AllArgsConstructor
public enum DBTypeEnum {

    DATABASE_TYPE_H2("h2"),
    DATABASE_TYPE_HSQL("hsql"),
    DATABASE_TYPE_MYSQL("mysql"),
    DATABASE_TYPE_ORACLE("oracle"),
    DATABASE_TYPE_POSTGRES("postgres"),
    DATABASE_TYPE_MSSQL("mssql"),
    DATABASE_TYPE_DB2("db2");

    private String value;

    @Override
    public String toString() {
        return value;
    }
}
