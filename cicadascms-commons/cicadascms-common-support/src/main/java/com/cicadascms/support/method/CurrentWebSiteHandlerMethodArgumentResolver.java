package com.cicadascms.support.method;

import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.support.annotation.CurrentWebSite;
import com.cicadascms.support.website.WebSiteContextHolder;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class CurrentWebSiteHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(SiteDO.class)
                && parameter.hasParameterAnnotation(CurrentWebSite.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer container, NativeWebRequest request,
                                  WebDataBinderFactory factory) {
        return WebSiteContextHolder.get();
    }
}
