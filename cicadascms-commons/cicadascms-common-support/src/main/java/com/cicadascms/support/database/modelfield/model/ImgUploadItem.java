package com.cicadascms.support.database.modelfield.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * ImgUploadItem
 *
 * @author Jin
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.ALWAYS)
public class ImgUploadItem implements Serializable {

    private String fileUrl = "";
    private String fileName = "";
    private Integer sortId = 0;

}
