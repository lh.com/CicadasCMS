package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * Textarea
 *
 * @author Jin
 */
@Getter
@Setter
public class TextAreaValue implements ModelFieldValue<String> {
    private String type = ModelFieldTypeEnum.TEXTAREA_NAME;
    private String content = "";

    @Override
    public String getValue() {
        return content;
    }
}
