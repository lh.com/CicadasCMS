package com.cicadascms.support.database.modelfield.value;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import lombok.Getter;
import lombok.Setter;

/**
 * RadioType
 *
 * @author Jin
 */
@Getter
@Setter
public class RadioValue implements ModelFieldValue<String> {

    private String type = ModelFieldTypeEnum.RADIO_NAME;

    private String checkValue = "";

    @Override
    public String getValue() {
        return checkValue;
    }
}
