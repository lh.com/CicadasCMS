package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.MultiImgUploadRule;
import com.cicadascms.support.database.modelfield.rule.RadioRule;
import com.cicadascms.support.database.modelfield.value.MultiImgUploadValue;
import com.cicadascms.support.database.modelfield.value.RadioValue;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * RadioType
 *
 * @author Jin
 */
@Getter
@Setter
public class RadioProp implements ModelFieldProp<RadioRule, RadioValue> {
    private String name = ModelFieldTypeEnum.RADIO_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 64;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private RadioRule rule = new RadioRule();
    private RadioValue value = new RadioValue();

}
