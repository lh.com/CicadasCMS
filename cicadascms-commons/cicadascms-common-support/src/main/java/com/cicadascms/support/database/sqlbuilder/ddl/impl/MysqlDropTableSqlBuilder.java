package com.cicadascms.support.database.sqlbuilder.ddl.impl;

import com.cicadascms.support.database.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.ddl.DropTableSqlBuilder;

public class MysqlDropTableSqlBuilder extends AbstractSqlBuilder<MysqlDropTableSqlBuilder> implements DropTableSqlBuilder<MysqlDropTableSqlBuilder> {

    //删除表
    private final static String DROP_TABLE = "DROP TABLE `{table}` ";

    @Override
    public MysqlDropTableSqlBuilder drop() {
        setSqlHead(DROP_TABLE);
        setSqlBody("");
        setSqlFoot("");
        return this;
    }

    @Override
    public MysqlDropTableSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        return this;
    }

    @Override
    public String buildSql() {
        return build();
    }
}
