package com.cicadascms.support.database.modelfield.rule;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class EditorRule implements ModelFieldRule {

    private String name = ModelFieldTypeEnum.EDITOR_NAME;
    private Boolean require = false;

    private String message = "请输入内容！";

    private Integer wordLimit = 5000;

}
