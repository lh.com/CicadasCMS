package com.cicadascms.support.captch;


import com.cicadascms.common.resp.R;

/**
 * ValidateCodeService
 *
 * @author Jin
 */
public interface ValidateCodeService {


    /**
     * 创建验证码
     */
    R create();


    /**
     * 图形验证码校验
     *
     * @param deviceId
     * @param inputCodeValue
     */
    void verifyCaptcha(String deviceId, String inputCodeValue);

    /**
     * 短信验证码校验
     *
     * @param deviceId
     * @param mobile
     * @param inputCodeValue
     */
    void verifySmsCode(String deviceId, String mobile, String inputCodeValue);

}
