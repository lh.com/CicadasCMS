package com.cicadascms.support.database.sqlbuilder.dml;

import com.cicadascms.support.database.sqlbuilder.ISqlBuilder;

import java.util.List;

/**
 * @author Jin
 * @date 2021-02-01 21:28:13
 * @description: 插入表数据
 */
public interface InsertDataSqlBuilder<T extends InsertDataSqlBuilder> extends ISqlBuilder {

    T insert(List<String> fields, List<Object> paramValues);

    T tableName(String tableName);

}
