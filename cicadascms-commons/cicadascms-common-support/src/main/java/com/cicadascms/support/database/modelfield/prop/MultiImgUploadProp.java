package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.JsonRule;
import com.cicadascms.support.database.modelfield.rule.MultiImgUploadRule;
import com.cicadascms.support.database.modelfield.value.JsonValue;
import com.cicadascms.support.database.modelfield.value.MultiImgUploadValue;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MultiImgUploadProp implements ModelFieldProp<MultiImgUploadRule, MultiImgUploadValue> {
    private String name = ModelFieldTypeEnum.MULTI_IMG_UPLOAD_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 1024;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private MultiImgUploadRule rule = new MultiImgUploadRule();
    private MultiImgUploadValue value = new MultiImgUploadValue();

}
