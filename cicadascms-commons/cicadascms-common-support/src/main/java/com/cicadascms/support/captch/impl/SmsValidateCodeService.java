package com.cicadascms.support.captch.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.json.JSONObject;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.resp.R;
import com.cicadascms.support.captch.AbstractValidateCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author westboy
 * @date 2019/4/27 14:10
 * @description: 验证码服务
 */
@Slf4j
@Service("smsValidateCodeService")
public class SmsValidateCodeService extends AbstractValidateCodeService {

    private final static Integer MAX_SEND_NUM = 10;

    @Autowired
    public SmsValidateCodeService(StringRedisTemplate redisTemplate) {
        super(redisTemplate);
    }

    @Override
    public R create() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String deviceId = request.getParameter(Constant.VALIDATE_PARAMETER_NAME_DEVICE_ID);
        String mobile = request.getParameter(Constant.VALIDATE_PARAMETER_NAME_MOBILE);

        //如果deviceId为空，随机生成一个deviceId
        if (Fn.isEmpty(deviceId)) {
            return R.ok(IdUtil.randomUUID());
        }

        String key = buildCacheKey(deviceId + ":" + mobile);

        preCheck(key, deviceId, mobile);
        //获取随机数值
        String value = getRandomCode(Constant.VALIDATE_CODE_SIZE);

        JSONObject templateParam = new JSONObject();
        templateParam.put("code", mobile);
        log.debug("向手机号{}发送验证码{}成功", mobile, value);
        //发送验证码
        //aliyunSmsTools.send(mobile, Enums.SmsTypeEnum.验证码, templateParam);
//        JdSmsUtils.sendVerifyCode(mobile, value);
        //设置缓存
        put(key, value);
        return R.ok("发送短信成功证码！[验证码:" + value + "]");
    }

    private String buildSendCountCacheKey(String mobile) {
        if (Fn.isEmpty(mobile)) {
            throw new ServiceException("手机号不能为空！");
        }
        return Constant.PREFIX + Constant.VALIDATE_REDIS_CODE_KEY + "_send_count" + ":" + mobile;
    }

    private void preCheck(String key, String deviceId, String mobile) {
        //检查验证码是否过期
        if (redisTemplate.hasKey(key)) {
            throw new ServiceException("请不要重复获取验证码！");
        }
        //检查当日验证发送是否上线
        String sendCountCacheKey = buildSendCountCacheKey(mobile);
        if (!redisTemplate.hasKey(sendCountCacheKey)) {
            redisTemplate.opsForValue().set(sendCountCacheKey, String.valueOf(1));
        } else {
            Integer count = Integer.parseInt(redisTemplate.opsForValue().get(sendCountCacheKey));
            //短信发送是否上限
            if (count > MAX_SEND_NUM) {
                Date tomorrow = DateUtil.tomorrow();
                Date today = new Date();
                Long exp = DateUtil.between(tomorrow, today, DateUnit.HOUR);
                //设置过期时间
                redisTemplate.expire(sendCountCacheKey, exp, TimeUnit.HOURS);
                throw new ServiceException("此号码" + mobile + "当日短信发送次数已上限！");
            } else {
                redisTemplate.opsForValue().set(sendCountCacheKey, String.valueOf(count + 1));
            }
        }
    }

    private String getRandomCode(int size) {
        String randomCode = "";
        for (int i : NumberUtil.generateRandomNumber(0, 9, size)) {
            randomCode += i;
        }
        return randomCode;
    }


    @Override
    protected String buildCacheKey(String key) {
        return Constant.PREFIX + Constant.VALIDATE_REDIS_CODE_KEY + ":" + key;
    }
}
