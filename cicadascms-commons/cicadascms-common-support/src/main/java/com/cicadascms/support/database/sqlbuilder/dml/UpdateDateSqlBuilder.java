package com.cicadascms.support.database.sqlbuilder.dml;

import com.cicadascms.support.database.sqlbuilder.ISqlBuilder;

import java.util.List;

/**
 * @author Jin
 * @date 2021-02-01 21:28:13
 * @description: 更新表数据
 */
public interface UpdateDateSqlBuilder<T extends UpdateDateSqlBuilder> extends ISqlBuilder {

    T update(String conditionField, Object conditionValue, List<String> fields, List<Object> paramValues);

    T tableName(String tableName);

}
