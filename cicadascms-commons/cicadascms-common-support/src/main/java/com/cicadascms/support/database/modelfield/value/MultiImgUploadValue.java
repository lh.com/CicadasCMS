package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.common.func.Fn;
import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import com.cicadascms.support.database.modelfield.model.ImgUploadItem;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class MultiImgUploadValue implements ModelFieldValue<List<ImgUploadItem>> {

    private String type = ModelFieldTypeEnum.MULTI_IMG_UPLOAD_NAME;

    private List<ImgUploadItem> imgList = new ArrayList<>();

    public List<ImgUploadItem> getImgList() {
        if (Fn.isEmpty(imgList)) {
            imgList.add(new ImgUploadItem());
        }
        return imgList;
    }

    @Override
    public List<ImgUploadItem> getValue() {
        return imgList;
    }
}