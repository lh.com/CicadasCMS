package com.cicadascms.support.database.modelfield.rule;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class RadioRule implements ModelFieldRule {

    private String name = ModelFieldTypeEnum.RADIO_NAME;

    private Boolean require = false;

    private String message = "请选择！";

    private Integer wordLimit = 20;

    private List<Map<String, Object>> group = new ArrayList<>();

    public RadioRule() {
        Map<String, Object> demo = new HashMap<>();
        demo.put("label", "label");
        demo.put("value", "value");
        group.add(demo);
    }
}
