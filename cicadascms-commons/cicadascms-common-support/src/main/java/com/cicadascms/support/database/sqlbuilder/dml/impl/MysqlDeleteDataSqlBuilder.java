package com.cicadascms.support.database.sqlbuilder.dml.impl;

import com.cicadascms.support.database.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.database.sqlbuilder.dml.DeleteDataSqlBuilder;


public class MysqlDeleteDataSqlBuilder extends AbstractSqlBuilder<MysqlDeleteDataSqlBuilder> implements DeleteDataSqlBuilder<MysqlDeleteDataSqlBuilder> {

    private final static String DELETE_DATA_BEGIN = "DELETE FROM `{table}` ";

    @Override
    public MysqlDeleteDataSqlBuilder delete(String conditionField, Object conditionValue) {
        StringBuilder sqlBody = new StringBuilder();
        if (conditionValue instanceof String) {
            conditionValue = "'" + conditionValue + "'";
        }
        sqlBody.append(" WHERE ")
                .append("`")
                .append(conditionField)
                .append("`=")
                .append(conditionValue)
                .append(";");
        setSqlBody(sqlBody.toString());
        return this;
    }

    @Override
    public MysqlDeleteDataSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        setSqlHead(DELETE_DATA_BEGIN);
        return this;
    }


    @Override
    public String buildSql() {
        return build();
    }
}
