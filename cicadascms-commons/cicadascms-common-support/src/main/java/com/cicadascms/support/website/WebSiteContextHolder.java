package com.cicadascms.support.website;

import com.cicadascms.data.domain.SiteDO;

import java.util.function.Supplier;


public class WebSiteContextHolder {

    private static final ThreadLocal<SiteDO> siteThreadLocal = new InheritableThreadLocal<>();

   public static <S> ThreadLocal<S> withInitial(Supplier<? extends S> supplier) {
      return ThreadLocal.withInitial(supplier);
   }

   public static SiteDO get() {
      return siteThreadLocal.get();
   }

   public static void set(SiteDO value) {
      siteThreadLocal.set(value);
   }

   public static void remove() {
      siteThreadLocal.remove();
   }
}
