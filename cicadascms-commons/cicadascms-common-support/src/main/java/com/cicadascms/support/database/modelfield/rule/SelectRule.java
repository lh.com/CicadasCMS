package com.cicadascms.support.database.modelfield.rule;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldRule;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CheckboxType
 *
 * @author Jin
 */
@Getter
@Setter
public class SelectRule implements ModelFieldRule {

    private String name = ModelFieldTypeEnum.SELECT_NAME;

    private Boolean require = false;

    private String message = "请选择菜单！";

    private Integer wordLimit = 20;

    private List<Map<String, Object>> options = new ArrayList<>();

    public SelectRule() {
        Map<String, Object> demo = new HashMap<>();
        demo.put("label", "text");
        demo.put("value", "object");
        demo.put("disabled", false);
        options.add(demo);
    }
}
