package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.EditorRule;
import com.cicadascms.support.database.modelfield.rule.FileUploadRule;
import com.cicadascms.support.database.modelfield.value.EditorValue;
import com.cicadascms.support.database.modelfield.value.FileUploadValue;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * InputDTO
 *
 * @author Jin
 */
@Getter
@Setter
public class FileUploadProp implements ModelFieldProp<FileUploadRule, FileUploadValue> {
    private String name = ModelFieldTypeEnum.FILE_UPLOAD_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 64;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private FileUploadRule rule = new FileUploadRule();
    private FileUploadValue value = new FileUploadValue();

}
