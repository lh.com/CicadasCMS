package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.SelectRule;
import com.cicadascms.support.database.modelfield.rule.TextAreaRule;
import com.cicadascms.support.database.modelfield.value.SelectValue;
import com.cicadascms.support.database.modelfield.value.TextAreaValue;
import lombok.Getter;
import lombok.Setter;

/**
 * Textarea
 *
 * @author Jin
 */
@Getter
@Setter
public class TextAreaProp implements ModelFieldProp<TextAreaRule, TextAreaValue> {
    private String name = ModelFieldTypeEnum.TEXTAREA_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 64;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private TextAreaRule rule = new TextAreaRule();
    private TextAreaValue value = new TextAreaValue();

}
