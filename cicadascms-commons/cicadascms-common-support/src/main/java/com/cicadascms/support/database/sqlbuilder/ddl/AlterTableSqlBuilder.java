package com.cicadascms.support.database.sqlbuilder.ddl;

import com.cicadascms.common.base.BaseEnum;
import com.cicadascms.support.database.sqlbuilder.ISqlBuilder;

/**
 * @author Jin
 * @date 2021-01-31 19:11:43
 * @description: 修改数据包
 */
public interface AlterTableSqlBuilder<T extends AlterTableSqlBuilder> extends ISqlBuilder {

    T changeColumn(String columnName, String newColumnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull);

    T addColumn(String columnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull, boolean isPrimaryKey);

    T dropColumn(String columnName, boolean isPrimaryKey);

    T tableName(String tableName);

}
