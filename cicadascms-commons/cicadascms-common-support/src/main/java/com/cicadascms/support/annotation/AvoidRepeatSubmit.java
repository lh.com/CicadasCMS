package com.cicadascms.support.annotation;


import java.lang.annotation.*;

/**
 * 防重复提交注解
 *
 * @author Jin
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AvoidRepeatSubmit {

    //毫秒
    long lockTime() default 1000;

}