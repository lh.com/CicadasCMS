package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import lombok.Getter;
import lombok.Setter;

/**
 * InputDTO
 *
 * @author Jin
 */
@Getter
@Setter
public class InputValue  implements ModelFieldValue<String> {
    private String type = ModelFieldTypeEnum.INPUT_NAME;
    private String content = "";

    @Override
    public String getValue() {
        return content;
    }
}
