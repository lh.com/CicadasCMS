package com.cicadascms.support.annotation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.lang.annotation.*;

/**
 * CicadasCmsApplication
 *
 * @author Jin
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@MapperScan({"com.cicadascms.data.mapper"})
@SpringBootApplication(scanBasePackages = {"com.cicadascms.**"}, exclude = {DataSourceAutoConfiguration.class})
public @interface CicadasCmsApplication {
}
