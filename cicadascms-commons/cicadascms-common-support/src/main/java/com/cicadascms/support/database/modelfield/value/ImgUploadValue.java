package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImgUploadValue implements ModelFieldValue<String> {

    private String type = ModelFieldTypeEnum.IMG_UPLOAD_NAME;

    private String imgUrl = "";

    @Override
    public String getValue() {
        return imgUrl;
    }
}
