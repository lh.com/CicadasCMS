package com.cicadascms.support.database.sqlbuilder.ddl.impl;

import com.cicadascms.common.func.Fn;
import com.cicadascms.support.database.sqlbuilder.AbstractSqlBuilder;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import com.cicadascms.support.database.sqlbuilder.ddl.CreateTableSqlBuilder;

public class MysqlCreateTableSqlBuilder extends AbstractSqlBuilder<MysqlCreateTableSqlBuilder> implements CreateTableSqlBuilder<MysqlCreateTableSqlBuilder> {

    // 创建表 开始
    private final static String CREATE_TABLE_BEGIN = "CREATE TABLE `{table}` (";

    //修改表名称
    private final static String RENAME_TABLE_BEGIN = "RENAME TABLE `{table}` to ";

    // 创建表 结束
    private final static String CREATE_TABLE_END = ") ";

    @Override
    public MysqlCreateTableSqlBuilder initColumn(String columnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull, boolean isPrimaryKey) {
        MysqlColumnTypeEnum columnTypeEnum = MysqlColumnTypeEnum.checkAndGet(columnType);
        StringBuilder sqlBody = new StringBuilder();
        if (columnTypeEnum.getIsNotLength()) {
            //判断字段是否允许设置长度
            sqlBody.append(" `" + columnName + "` " + columnType + "(" + length + ")");
        } else {
            sqlBody.append(" `" + columnName + "` " + columnType);
        }
        if (isNotNull) {
            //判断字段是否不为空
            sqlBody.append(" NOT null");
        }
        if (!autoIncrement) {
            if (!isNotNull) {
                //判断字段是否允许为空
                sqlBody.append(" NULL ");
            }
            if (Fn.isNotNull(defaultValue)) {
                sqlBody.append(" DEFAULT " + (Fn.isEmpty(defaultValue) ? null : "'" + defaultValue + "'"));
            }
        } else {
            // 判断字段类型是否支持自动增长
            if (columnTypeEnum.getAutoIncrement()) {
                if (!isNotNull) {
                    sqlBody.append(" NOT null");
                }
                sqlBody.append(" AUTO_INCREMENT");
            }
        }
        if (isPrimaryKey) {
            //是否为主键
            sqlBody.append(" , PRIMARY KEY (`" + columnName + "`) ");
        }
        if (Fn.isNotEmpty(getSqlBody())) {
            sqlBody.append(",")
                    .append("\n")
                    .append(sqlBody.toString());
        }

        setSqlBody(sqlBody.toString());
        return this;
    }

    @Override
    public MysqlCreateTableSqlBuilder rename(String newTableName) {
        setSqlHead(RENAME_TABLE_BEGIN);
        setSqlBody("`" + newTableName + "`");
        setSqlFoot("");
        return this;
    }


    @Override
    public String build() {
        if (getSqlBody().endsWith(",")) {
            setSqlBody(getSqlBody().substring(0, getSqlBody().length() - 1));
        }
        return super.build();
    }

    @Override
    public MysqlCreateTableSqlBuilder tableName(String tableName) {
        clearSql();
        setTableName(tableName);
        setSqlHead(CREATE_TABLE_BEGIN);
        setSqlBody("");
        setSqlFoot(CREATE_TABLE_END);
        return this;
    }

    @Override
    public String buildSql() {
        return build();
    }
}
