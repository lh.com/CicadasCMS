package com.cicadascms.support.database.sqlbuilder.ddl;

import com.cicadascms.common.base.BaseEnum;
import com.cicadascms.support.database.sqlbuilder.ISqlBuilder;

/**
 * @author Jin
 * @date 2021-01-31 19:10:53
 * @description: 创建数据表
 */
public interface CreateTableSqlBuilder<T extends CreateTableSqlBuilder> extends ISqlBuilder {

    T initColumn(String columnName, String columnType, Integer length, boolean autoIncrement, String defaultValue, boolean isNotNull, boolean isPrimaryKey);

    T rename(String newTableName);

    T tableName(String tableName);

}
