package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.InputRule;
import com.cicadascms.support.database.modelfield.value.InputValue;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * InputDTO
 *
 * @author Jin
 */
@Getter
@Setter
public class InputProp implements ModelFieldProp<InputRule, InputValue> {
    private String name = ModelFieldTypeEnum.INPUT_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 64;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private InputRule rule = new InputRule();
    private InputValue value = new InputValue();

}
