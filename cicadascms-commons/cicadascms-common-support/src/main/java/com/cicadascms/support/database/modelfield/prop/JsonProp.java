package com.cicadascms.support.database.modelfield.prop;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.enums.MysqlColumnTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import com.cicadascms.support.database.modelfield.rule.InputRule;
import com.cicadascms.support.database.modelfield.rule.JsonRule;
import com.cicadascms.support.database.modelfield.rule.TextAreaRule;
import com.cicadascms.support.database.modelfield.value.InputValue;
import com.cicadascms.support.database.modelfield.value.JsonValue;
import com.cicadascms.support.database.modelfield.value.TextAreaValue;
import lombok.Getter;
import lombok.Setter;

/**
 * JsonProp
 *
 * @author Jin
 */
@Getter
@Setter
public class JsonProp implements ModelFieldProp<JsonRule, JsonValue> {
    private String name = ModelFieldTypeEnum.JSON_NAME;
    private String columnType = MysqlColumnTypeEnum.VARCHAR.getCode();
    private Integer length = 1024;
    private Boolean autoIncrement = false;
    private String defaultValue = "";
    private Boolean notNull = false;
    private Boolean primaryKey = false;

    private JsonRule rule = new JsonRule();
    private JsonValue value = new JsonValue();

}
