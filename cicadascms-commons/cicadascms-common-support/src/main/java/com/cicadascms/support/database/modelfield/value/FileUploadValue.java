package com.cicadascms.support.database.modelfield.value;

import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import lombok.Getter;
import lombok.Setter;

/**
 * InputDTO
 *
 * @author Jin
 */
@Getter
@Setter
public class FileUploadValue implements ModelFieldValue<String> {
    private String type = ModelFieldTypeEnum.FILE_UPLOAD_NAME;
    private String fileUrl = "";

    @Override
    public String getValue() {
        return fileUrl;
    }
}
