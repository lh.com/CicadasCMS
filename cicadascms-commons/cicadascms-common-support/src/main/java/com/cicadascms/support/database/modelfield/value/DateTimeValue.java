package com.cicadascms.support.database.modelfield.value;


import com.cicadascms.support.database.enums.ModelFieldTypeEnum;
import com.cicadascms.support.database.modelfield.ModelFieldValue;
import lombok.Getter;
import lombok.Setter;

/**
 * DateTimeType
 *
 * @author Jin
 */
@Getter
@Setter
public class DateTimeValue implements ModelFieldValue<String> {
    private String type = ModelFieldTypeEnum.DATE_TIME_NAME;
    private String content = "";

    @Override
    public String getValue() {
        return content;
    }
}
