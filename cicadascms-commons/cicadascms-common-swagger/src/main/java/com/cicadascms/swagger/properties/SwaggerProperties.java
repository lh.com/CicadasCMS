package com.cicadascms.swagger.properties;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * SwaggerConfig
 *
 * @author Jin
 */
@Data
@ConfigurationProperties("swagger")
public class SwaggerProperties {

	private String basePackage = "";

	private List<String> basePath = new ArrayList<>();

	private List<String> excludePath = new ArrayList<>();

	private String title = "";

	private String description = "";

	private String version = "";

	private String license = "";

	private String licenseUrl = "";

	private String termsOfServiceUrl = "";

	private String host = "";

	private Contact contact = new Contact();

	private Authorization authorization = new Authorization();

	@Data
	@NoArgsConstructor
	public static class Contact {

		private String name = "";
		private String url = "";
		private String email = "";

	}

	@Data
	@NoArgsConstructor
	public static class Authorization {

		private String name = "";
		private String authRegex = "^.*$";
		private List<AuthorizationScope> authorizationScopeList = new ArrayList<>();
		private List<String> tokenUrlList = new ArrayList<>();

	}

	@Data
	@NoArgsConstructor
	public static class AuthorizationScope {
		private String scope = "";
		private String description = "";

	}
}
