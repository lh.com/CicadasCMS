package com.cicadascms.common.dict;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * DictManager
 *
 * @author Jin
 */
public interface DictManager {

    Optional<List<Dict>> getOptionalDictData(String code);

    List<Dict> getDictData(String code);

    Dict getDict(String code, String value);

    Dict getDict(Serializable id);

}
