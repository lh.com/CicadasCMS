package com.cicadascms.common.utils;


import lombok.experimental.UtilityClass;

import java.text.DecimalFormat;

/**
 * AmountUtils
 *
 * @author Jin
 */
@UtilityClass
public class AmountUtils {

    /**
     * 分转元
     * @param amount
     * @return
     */
    public static String centToYuan(String amount){
        DecimalFormat df = new DecimalFormat("######0.00");
        Double d = Double.parseDouble(amount)/100;
        return df.format(d);
    }

    /**
     * 元转分
     * @param amount
     * @return
     */
    public static String yuanToCent(String amount){
        DecimalFormat df = new DecimalFormat("######0");
        Double d = Double.parseDouble(amount)*100;
        return df.format(d);
    }
}
