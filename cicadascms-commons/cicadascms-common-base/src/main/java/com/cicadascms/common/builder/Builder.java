package com.cicadascms.common.builder;

/**
 * Builder
 *
 * @author Jin
 */
public interface Builder<T> {

    T build();
}