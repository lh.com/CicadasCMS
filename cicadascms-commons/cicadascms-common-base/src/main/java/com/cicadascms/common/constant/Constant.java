package com.cicadascms.common.constant;

import java.io.File;

/**
 * Constant
 *
 * @author Jin
 */
public class Constant {

    public static final String UTF_8 = "UTF-8";

    public static final String APPLICATION_JSON = "application/json";

    public static final String PREFIX = "cicadascms_";

    public static final String OAUTH_LICENSE = "made by CicadasCMS v2.0";

    public static final String OAUTH_PREFIX = "oauth:";

    public static final String RESOURCES_FOLDER_NAME = System.getProperty("user.dir");

    //Api 返回值常量
    public static final Integer SUCCESS_CODE = 20000;

    public static final Integer ERROR_CODE = 30000;

    public static final Integer PARENT_ID = 0;
    public static final Integer PAGE_NUM = 1;
    public static final Integer PAGE_SIZE = 20;

    //跨域相关常量

    public static final String CORS_ALLOW_HEADERS = "Origin, X-Requested-With, Content-Type, Accept, x-token, authorization";

    public static final String CORS_ALLOW_METHODS = "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH";

    public static final Long CORS_MAX_AGE = 3600l;

    //验证相关常量

    public static final String VALIDATE_PARAMETER_NAME_CODE = "code";

    public static final String VALIDATE_PARAMETER_NAME_DEVICE_ID = "deviceId";

    public static final String VALIDATE_PARAMETER_NAME_MOBILE = "mobile";

    public static final String VALIDATE_REDIS_CODE_KEY = "validate_code";

    public static final int VALIDATE_CODE_SIZE = 6;

    public static final int VALIDATE_CODE_TIME = 300;

    public static final String VALIDATE_TEST_CLIENT = "test";
}
