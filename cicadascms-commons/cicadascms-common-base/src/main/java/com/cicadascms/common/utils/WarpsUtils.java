package com.cicadascms.common.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ReflectUtil;
import lombok.experimental.UtilityClass;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * SpringContextUtils
 *
 * @author Jin
 */
@UtilityClass
public class WarpsUtils {

    public static <T> T copyTo(Object object, Class<T> clazz) {
        try {
            T vo = ReflectUtil.newInstance(clazz);
            BeanUtil.copyProperties(object, vo);
            return vo;
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T convertTo(Object bean, Class<T> clazz) {
        try {
            ObjectMapper mapper = SpringContextUtils.getBean(ObjectMapper.class);
            return mapper.convertValue(bean, clazz);
        } catch (Exception e) {
            return null;
        }
    }
}
