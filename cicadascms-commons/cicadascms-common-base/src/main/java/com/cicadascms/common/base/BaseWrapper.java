package com.cicadascms.common.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.tree.TreeNode;
import com.cicadascms.common.utils.TreeUtils;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * EntityWrapper
 *
 * @author Jin
 */
public interface BaseWrapper<E, V> {

    V entityVO(E entity);

    default <V extends TreeNode<V>> List<V> treeVO(List<V> list) {
        return TreeUtils.toTree(list);
    }

    default List<V> listVO(List<E> list) {
        return list.stream().map(this::entityVO).collect(Collectors.toList());
    }

    default IPage<V> pageVO(IPage<E> pages) {
        List<V> records = this.listVO(pages.getRecords());
        IPage<V> pageVo;
        pageVo = new Page<>(pages.getCurrent(), pages.getSize(), pages.getTotal());
        pageVo.setRecords(records);
        return pageVo;
    }

}