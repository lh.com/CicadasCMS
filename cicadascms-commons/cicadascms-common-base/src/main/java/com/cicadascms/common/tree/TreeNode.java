package com.cicadascms.common.tree;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;

/**
 * TreeNode
 *
 * @author Jin
 */
public interface TreeNode<T> {

    @JsonIgnore
    Serializable getCurrentNodeId();

    @JsonIgnore
    Serializable getParentNodeId();

    void setChildren(List<T> children);
}
