package com.cicadascms.common.utils;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.func.Fn;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * EvnUtils
 *
 * @author Jin
 */
public class EvnUtils {
    private static String DEFAULT_FOLDER_NAME = "cicadascms";

    public static boolean isJarActive(Class<?> clazz) {
        URL url = clazz.getResource("");
        String protocol = url.getProtocol();
        return Fn.equal("jar", protocol);
    }

    public static String checkAndCreateResourceDir(String dir) {
        String path = Constant.RESOURCES_FOLDER_NAME + File.separator + DEFAULT_FOLDER_NAME+ File.separator + dir;
        File folder = new File(path);
        if (!folder.exists()) folder.mkdirs();
        return folder.getAbsolutePath();
    }

    public static String checkAndCreateNewFile(String dir) throws IOException {
        String path = Constant.RESOURCES_FOLDER_NAME + File.separator + DEFAULT_FOLDER_NAME + File.separator + dir;
        File file = new File(path);
        if (file.isDirectory()) file.delete();
        if (!file.exists()) file.createNewFile();
        return file.getAbsolutePath();
    }

}
