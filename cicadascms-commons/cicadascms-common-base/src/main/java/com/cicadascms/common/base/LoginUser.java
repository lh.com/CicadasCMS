package com.cicadascms.common.base;

import java.io.Serializable;
import java.util.Set;

public interface LoginUser {

    Set<Integer> getRoleIds();

    Set<String> getDataScopes();

    String getUsername();

    String getPassword();

    Serializable getUid();


}
