package com.cicadascms.common.base;

/**
 * BaseEnum
 *
 * @author Jin
 */
public interface BaseEnum<I> {
    I getCode();
}