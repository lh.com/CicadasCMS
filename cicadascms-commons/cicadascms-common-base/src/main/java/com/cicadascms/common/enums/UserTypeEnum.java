package com.cicadascms.common.enums;

import com.cicadascms.common.base.BaseEnum;

/**
 * UserTypeEnum
 *
 * @author Jin
 */
public enum UserTypeEnum implements BaseEnum<Integer> {

    系统(1, "system", "/system/**"),
    用户(2, "member", "/member/**");

    public Integer code;
    public String alias;
    public String allowAccessUrl;

    UserTypeEnum(Integer code, String alias, String allowAccessUrl) {
        this.code = code;
        this.alias = alias;
        this.allowAccessUrl = allowAccessUrl;
    }

    @Override
    public Integer getCode() {
        return code;
    }
}