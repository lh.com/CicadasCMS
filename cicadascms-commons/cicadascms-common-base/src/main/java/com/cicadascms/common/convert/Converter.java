package com.cicadascms.common.convert;


/**
 * Converter
 *
 * @author Jin
 */
public abstract class Converter<A, B> {

    public abstract B doForward(A a);

    public abstract A doBackward(B b);
}
