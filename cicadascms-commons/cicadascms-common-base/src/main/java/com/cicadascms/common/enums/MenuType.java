package com.cicadascms.common.enums;

import com.cicadascms.common.base.BaseEnum;

/**
 * MenuType
 *
 * @author Jin
 */
public enum MenuType implements BaseEnum<Integer> {
    菜单(1),
    按钮(2);
    public Integer code;

    MenuType(Integer code) {
        this.code = code;
    }


    @Override
    public Integer getCode() {
        return code;
    }
}