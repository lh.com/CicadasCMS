package com.cicadascms.common.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * BaseDTO
 *
 * @author Jin
 */
public abstract class BaseDTO<D, E>{

    public abstract E convertToEntity();

    public abstract D convertFor(E e);

    protected Page<E> getPage(long current,long size){
        return new Page<>(current, size);
    }

}
