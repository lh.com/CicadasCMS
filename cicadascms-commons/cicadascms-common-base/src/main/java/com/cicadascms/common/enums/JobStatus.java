package com.cicadascms.common.enums;

/**
 * JobStatus
 *
 * @author Jin
 */
public enum JobStatus {

    RUNNING("RUNNING"),
    ACQUIRED("ACQUIRED"),
    COMPLETE("COMPLETE"),
    PAUSED("PAUSED");
    private String status;

    JobStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}