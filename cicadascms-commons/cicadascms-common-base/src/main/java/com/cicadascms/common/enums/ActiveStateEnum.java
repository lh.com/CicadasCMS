package com.cicadascms.common.enums;

import com.cicadascms.common.base.BaseEnum;

/**
 * ActiveStateEnum
 *
 * @author Jin
 */
public enum ActiveStateEnum implements BaseEnum<Integer> {

    启用(1),
    禁用(0),
    删除(-1);

    ActiveStateEnum(Integer code) {
        this.code = code;
    }

    public Integer code;

    @Override
    public Integer getCode() {
        return code;
    }
}