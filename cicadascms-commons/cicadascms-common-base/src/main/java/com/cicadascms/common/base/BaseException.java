package com.cicadascms.common.base;

import java.util.function.Supplier;

/**
 * BaseException
 *
 * @author Jin
 */
public abstract class BaseException extends RuntimeException implements Supplier<BaseException> {

    private int code;
    private String tenantId;
    private String moduleName;

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, int code, String tenantId, String moduleName) {
        super(message);
        this.code = code;
        this.tenantId = tenantId;
        this.moduleName = moduleName;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


    @Override
    public BaseException get() {
        return this;
    }
}
