package com.cicadascms.common.base;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cicadascms.common.utils.WarpsUtils;

import java.util.List;

/**
 * BaseService
 *
 * @author Jin
 */
public class BaseService<M extends BaseMapper<E>, E> extends ServiceImpl<M, E> {

    public LambdaQueryWrapper<E> getLambdaQueryWrapper() {
        return new LambdaQueryWrapper<>();
    }

    public LambdaQueryWrapper<E> getLambdaQueryWrapper(E entity) {
        return new LambdaQueryWrapper<>(entity);
    }

    public List<E> findAll() {
        return baseMapper.selectList(Wrappers.emptyWrapper());
    }

    protected <T> T copyTo(Object object, Class<T> clazz) {
        return WarpsUtils.copyTo(object, clazz);
    }
}
