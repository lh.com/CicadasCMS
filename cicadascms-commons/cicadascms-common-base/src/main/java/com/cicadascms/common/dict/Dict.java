package com.cicadascms.common.dict;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Dict
 *
 * @author Jin
 */
@Getter
@Setter
public class Dict {

    private Integer id;

    /**
     * 字典标识
     */
    private String dictCode;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典值
     */
    private Integer dictValue;

    /**
     * 上级ID
     */
    private Integer parentId;

    /**
     * 状态[0：不可用，1：可用]
     */
    private Boolean state;

    /**
     * 排序字段
     */
    private Integer sortId;

    /**
     * 排序字段
     */
    private String remarks;

    /**
     * parentName
     */
    private String parentName;


    /**
     * children
     */
    private List<Dict> children;

}
