package com.cicadascms.common.base;


import com.cicadascms.common.convert.Converter;

/**
 * BaseVo
 *
 * @author Jin
 */
public class BaseVO<V,E>  extends Converter<V, E> {

    @Override
    public E doForward(V v) {
        return null;
    }

    @Override
    public V doBackward(E e) {
        return null;
    }
}
