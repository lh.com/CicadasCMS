package com.cicadascms.common.exception;

import com.cicadascms.common.base.BaseException;

/**
 * ServiceException
 *
 * @author Jin
 */
public class FrontNotFoundException extends BaseException {

    public FrontNotFoundException(String message) {
        super(message);
    }

    public FrontNotFoundException(String message, int code, String tenantId, String moduleName) {
        super(message, code, tenantId, moduleName);
    }


    public FrontNotFoundException(String message, Throwable throwable) {
        super(message);
    }


}
