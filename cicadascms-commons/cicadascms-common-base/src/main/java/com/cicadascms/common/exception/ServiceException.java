package com.cicadascms.common.exception;

import com.cicadascms.common.base.BaseException;

/**
 * ServiceException
 *
 * @author Jin
 */
public class ServiceException extends BaseException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, int code, String tenantId, String moduleName) {
        super(message, code, tenantId, moduleName);
    }


    public ServiceException(String message, Throwable throwable) {
        super(message);
    }


}
