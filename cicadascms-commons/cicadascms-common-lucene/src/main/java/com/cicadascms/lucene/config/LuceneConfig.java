package com.cicadascms.lucene.config;

import com.cicadascms.common.utils.EvnUtils;
import com.cicadascms.lucene.manager.LuceneManager;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Paths;

/**
 * LuceneConfig
 *
 * @author Jin
 */
@Configuration
public class LuceneConfig {

    @Bean
    public LuceneManager luceneManager() {
        String luceneFolder = EvnUtils.checkAndCreateResourceDir("lucene");
        LuceneManager luceneManager = new LuceneManager();
        luceneManager.setDirectoryPath(Paths.get(luceneFolder));
        luceneManager.setAnalyzer(new SmartChineseAnalyzer());
        return luceneManager;
    }
}
