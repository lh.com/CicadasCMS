package com.cicadascms.plugin;

import org.springframework.beans.factory.InitializingBean;

/**
 * AbstractPlugin
 *
 * @author Jin
 */
public abstract class AbstractPlugin<T> implements IPlugin<T>, Comparable<AbstractPlugin>, InitializingBean {

    protected int title = 0;
    protected int author = 0;
    protected int description = 0;
    protected int version = 0;
    protected int weight = 0;

    public abstract void installPlugin() throws Exception;

    @Override
    public int compareTo(AbstractPlugin o) {
        return o.weight - this.weight;
    }


}
