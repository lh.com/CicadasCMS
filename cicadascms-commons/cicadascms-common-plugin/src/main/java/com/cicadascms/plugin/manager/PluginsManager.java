package com.cicadascms.plugin.manager;


import com.cicadascms.common.func.Fn;
import com.cicadascms.plugin.IPlugin;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * PluginsManager
 *
 * @author Jin
 */
@Component
public class PluginsManager {

    private final Map<Class<?>, List<IPlugin>> pluginsMap = new HashMap<>();

    public void registerPlugin(IPlugin plugin) {
        Class<?> clazz = plugin.getClass().getSuperclass();
        List<IPlugin> pluginList = pluginsMap.get(clazz);
        if (Fn.isEmpty(pluginList)) {
            pluginList = new LinkedList<>();
        }
        pluginsMap.put(clazz, pluginList);
        pluginList.add(plugin);
    }


    public <T> List<IPlugin> getPlugins(Class<T> clazz) {
        return pluginsMap.get(clazz);
    }

}
