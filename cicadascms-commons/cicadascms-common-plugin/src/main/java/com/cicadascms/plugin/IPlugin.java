package com.cicadascms.plugin;


import com.cicadascms.common.exception.PluginException;

/**
 * IPlugin
 *
 * @author Jin
 */
public interface IPlugin<T> {

    Object invoke(T obj) throws PluginException;

}
