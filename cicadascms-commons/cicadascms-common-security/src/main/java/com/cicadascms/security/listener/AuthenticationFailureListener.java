package com.cicadascms.security.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.*;
import org.springframework.security.oauth2.client.filter.OAuth2AuthenticationFailureEvent;
import org.springframework.stereotype.Component;

/**
 * AuthenticationFailureListener
 *
 * @author Jin
 */
@Slf4j
@Component
public class AuthenticationFailureListener implements ApplicationListener<AbstractAuthenticationFailureEvent> {
    @Override
    public void onApplicationEvent(AbstractAuthenticationFailureEvent event) {
        if (event instanceof AuthenticationFailureBadCredentialsEvent) {
            log.info("提供的凭据是错误的，用户名或者密码错误");
        } else if (event instanceof AuthenticationFailureCredentialsExpiredEvent) {
            log.info("验证通过，但是密码过期");
        } else if (event instanceof AuthenticationFailureDisabledEvent) {
            log.info("验证过了但是账户被禁用");
        } else if (event instanceof AuthenticationFailureExpiredEvent) {
            log.info("验证通过了，但是账号已经过期");
        } else if (event instanceof AuthenticationFailureLockedEvent) {
            log.info("账户被锁定");
        } else if (event instanceof AuthenticationFailureProviderNotFoundEvent) {
            log.info("配置错误，没有合适的AuthenticationProvider来处理登录验证");
        } else if (event instanceof AuthenticationFailureProxyUntrustedEvent) {
            log.info("代理不受信任，用于Oauth、CAS这类三方验证的情形，多属于配置错误");
        } else if (event instanceof AuthenticationFailureServiceExceptionEvent) {
            log.info("其他任何在AuthenticationManager中内部发生的异常都会被封装成此类");
        } else if (event instanceof OAuth2AuthenticationFailureEvent) {
            log.info("Oauth2认证失败");
        }
    }

}