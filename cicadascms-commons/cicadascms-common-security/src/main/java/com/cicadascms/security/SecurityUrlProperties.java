package com.cicadascms.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * SecurityUrlProperties
 *
 * @author Jin
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "security.url")
public class SecurityUrlProperties {

    private static final String[] ENDPOINTS = {
            "/**/actuator/**", "/**/actuator/**/**",  //断点监控
            "/**/v2/api-docs/**", "/**/swagger-ui.html", "/**/swagger-resources/**", "/**/webjars/**", //swagger
            "/**/druid/**", "/**/favicon.ico", "/**/prometheus", "/configuration/ui", "/configuration/security",
            "/swagger-ui.html", "/webjars/**", "/doc.html", "/js/**", "/static/css/**", "/health", "/error/**",
            "/oauth/token", "/oauth/token/**",
    };

    private String[] excludePath;

    private String[] authPath;

    public String[] getExcludePath() {
        if (excludePath == null || excludePath.length == 0) {
            return ENDPOINTS;
        }
        List<String> list = new ArrayList<>();
        Collections.addAll(list, ENDPOINTS);
        Collections.addAll(list, excludePath);

        return list.toArray(new String[0]);
    }

    public void setIgnored(String[] ignored) {
        this.excludePath = ignored;
    }

}
