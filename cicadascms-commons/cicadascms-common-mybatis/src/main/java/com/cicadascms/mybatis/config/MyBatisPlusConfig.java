package com.cicadascms.mybatis.config;


import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.cicadascms.mybatis.interceptor.DataScoreInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MyBatisPlusConfig
 *
 * @author Jin
 */
@Configuration
public class MyBatisPlusConfig {

    private String[] excludeTableNames = {"", ""};

    /**
     * 数据权限拦截器
     *
     * @return
     */
    @Bean
    public DataScoreInterceptor dataScoreInterceptor() {
        return new DataScoreInterceptor();
    }

    /***
     * 分页拦截器
     * @return
     */
    @Bean
    public PaginationInnerInterceptor paginationInterceptor() {
        return new PaginationInnerInterceptor();
    }

}