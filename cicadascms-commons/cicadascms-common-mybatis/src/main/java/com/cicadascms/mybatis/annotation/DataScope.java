package com.cicadascms.mybatis.annotation;

import java.lang.annotation.*;


/**
 * 数据权限注解
 *
 * @author Jin
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataScope {

    String fieldName() default "";

    String alias() default "";

    String value() default "";

}
