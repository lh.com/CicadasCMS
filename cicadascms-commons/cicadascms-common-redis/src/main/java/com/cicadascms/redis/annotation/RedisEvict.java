package com.cicadascms.redis.annotation;

import java.lang.annotation.*;

/**
 * Redis cache Evict
 *
 * @author Jin
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisEvict {
    String key();

    String fieldKey();
}