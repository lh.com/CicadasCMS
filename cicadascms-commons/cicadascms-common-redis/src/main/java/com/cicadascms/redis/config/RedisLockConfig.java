package com.cicadascms.redis.config;

import com.cicadascms.redis.lock.RedisLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RedisLockConfig
 *
 * @author Jin
 */
@Configuration
@ConditionalOnClass({RedissonClient.class})
public class RedisLockConfig {

    @Bean
    public RedisLock getRedisLock(RedissonClient redissonClient) {
        return new RedisLock(redissonClient);
    }

}
