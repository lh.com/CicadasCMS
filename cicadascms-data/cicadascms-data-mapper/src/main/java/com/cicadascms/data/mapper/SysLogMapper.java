package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.LogDO;

/**
 * <p>
 * 日志表 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2020-03-26
 */
public interface SysLogMapper extends BaseMapper<LogDO> {

}
