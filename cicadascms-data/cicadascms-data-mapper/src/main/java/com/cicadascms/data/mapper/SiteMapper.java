package com.cicadascms.data.mapper;

import com.cicadascms.data.domain.SiteDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 站点表 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2020-10-12
 */
public interface SiteMapper extends BaseMapper<SiteDO> {

}
