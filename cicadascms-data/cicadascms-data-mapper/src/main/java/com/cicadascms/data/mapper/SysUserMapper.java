package com.cicadascms.data.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.data.domain.UserDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 基础账户表 Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-05-23
 */
public interface SysUserMapper extends BaseMapper<UserDO> {

    <E extends IPage<UserDO>> E selectPageByDeptId(E page, @Param("deptIds") List<Integer> deptIds);

}
