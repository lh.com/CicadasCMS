package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.OauthClientDetailsDO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-06-19
 */
public interface SysOauthClientDetailsMapper extends BaseMapper<OauthClientDetailsDO> {

}
