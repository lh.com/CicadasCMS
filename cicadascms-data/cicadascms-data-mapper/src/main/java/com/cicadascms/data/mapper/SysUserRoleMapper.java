package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.UserRoleDO;

/**
 * <p>
 * 用户角色管理表 Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
public interface SysUserRoleMapper extends BaseMapper<UserRoleDO> {

}
