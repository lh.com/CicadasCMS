package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.DictDO;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2020-04-07
 */
public interface SysDictMapper extends BaseMapper<DictDO> {

}
