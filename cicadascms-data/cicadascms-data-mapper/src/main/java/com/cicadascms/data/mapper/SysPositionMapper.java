package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.PositionDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 职位表 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
public interface SysPositionMapper extends BaseMapper<PositionDO> {

    @Select("select sp.* from sys_position sp " +
            "    join sys_user_position sup on  sp.id = sup.post_id " +
            "    join sys_user su on su.uid = sup.uid " +
            "where su.uid = #{userId}")
    @ResultMap("BaseResultMap")
    List<PositionDO> selectByUserId(@Param("userId") Serializable userId);

}
