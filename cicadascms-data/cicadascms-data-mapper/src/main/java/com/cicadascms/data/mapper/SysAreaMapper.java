package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.AreaDO;

/**
 * <p>
 * 区域 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
public interface SysAreaMapper extends BaseMapper<AreaDO> {

}
