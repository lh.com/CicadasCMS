package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.UserPositionDO;

/**
 * <p>
 * 用户职位 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
public interface SysUserPositionMapper extends BaseMapper<UserPositionDO> {

}
