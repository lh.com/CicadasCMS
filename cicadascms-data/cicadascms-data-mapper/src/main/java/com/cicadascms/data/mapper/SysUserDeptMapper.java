package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.UserDeptDO;

/**
 * <p>
 * 用户部门关联表 Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-08-21
 */
public interface SysUserDeptMapper extends BaseMapper<UserDeptDO> {

}
