package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.RoleMenuDO;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
public interface SysRoleMenuMapper extends BaseMapper<RoleMenuDO> {

}
