package com.cicadascms.data.mapper;

import com.cicadascms.data.domain.NavDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 导航菜单 Mapper 接口
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface NavMapper extends BaseMapper<NavDO> {

}
