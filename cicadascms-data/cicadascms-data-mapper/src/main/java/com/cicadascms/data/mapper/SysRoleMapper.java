package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.RoleDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;
import java.util.List;


/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-07-10
 */
public interface SysRoleMapper extends BaseMapper<RoleDO> {

    @Select("SELECT sr.* \n" +
            "FROM sys_role sr \n" +
            "    join sys_user_role sur \n" +
            "        on sr.role_id = sur.role_id \n" +
            "    join sys_user su \n" +
            "        on sur.uid = su.uid\n" +
            "where su.uid = #{userId}")
    @ResultMap("BaseResultMap")
    List<RoleDO> selectByUserId(@Param("userId") Serializable userId);

}
