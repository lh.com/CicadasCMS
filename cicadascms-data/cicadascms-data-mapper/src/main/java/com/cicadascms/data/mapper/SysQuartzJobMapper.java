package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.QuartzJobDO;

/**
 * <p>
 * 定时任务 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2020-04-29
 */
public interface SysQuartzJobMapper extends BaseMapper<QuartzJobDO> {

}
