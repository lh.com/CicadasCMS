package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.ModelDO;

/**
 * <p>
 * 内容模型表 Mapper 接口
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface ModelMapper extends BaseMapper<ModelDO> {

}
