package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.AttrClassDO;

/**
 * <p>
 * 附件分类 Mapper 接口
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
public interface AttrClassMapper extends BaseMapper<AttrClassDO> {

}
