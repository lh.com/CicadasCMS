package com.cicadascms.data.mapper;

import com.cicadascms.data.domain.ModelFieldDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模型字段 Mapper 接口
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface ModelFieldMapper extends BaseMapper<ModelFieldDO> {

}
