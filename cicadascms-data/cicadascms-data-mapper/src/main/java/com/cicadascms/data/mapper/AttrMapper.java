package com.cicadascms.data.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.AttrDO;

/**
 * <p>
 * 附件 Mapper 接口
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
public interface AttrMapper extends BaseMapper<AttrDO> {

}
