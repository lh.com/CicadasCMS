package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.DeptDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 机构部门 Mapper 接口
 * </p>

 * @author westboy
 * @date 2019-07-21
 */
public interface SysDeptMapper extends BaseMapper<DeptDO> {

    @Select("SELECT sd.* \n" +
            "FROM sys_dept sd \n" +
            "    join sys_user_dept sud \n" +
            "        on sd.dept_id = sud.dept_id \n" +
            "    join sys_user su \n" +
            "        on sud.uid = su.uid\n" +
            "where su.uid = #{userId}")
    @ResultMap("BaseResultMap")
    List<DeptDO> selectByUserId(@Param("userId") Serializable userId);

}
