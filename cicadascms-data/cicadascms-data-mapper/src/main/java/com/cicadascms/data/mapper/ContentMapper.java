package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.ContentDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * 内容 Mapper 接口
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface ContentMapper extends BaseMapper<ContentDO> {

    @Select("SELECT ${fields} FROM ${tableName} WHERE content_id = #{contentId}")
    @ResultType(Map.class)
    Map<String, Object> selectByTableNameAndContentId(@Param("fields") String fields,@Param("tableName") String tableName, @Param("contentId") Serializable contentId);

}
