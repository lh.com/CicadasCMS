package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.UserConnectionDO;

/**
 * <p>
 * 用户社交账号绑定 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2020-03-27
 */
public interface SysUserConnectionMapper extends BaseMapper<UserConnectionDO> {

}
