package com.cicadascms.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cicadascms.data.domain.MenuDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author westboy
 * @date 2019-07-10
 */
public interface SysMenuMapper extends BaseMapper<MenuDO> {

    List<MenuDO> selectByRoleIds(Set<Integer> roleIds);

    @Select("SELECT sm.* from sys_menu sm " +
            "    join sys_role_menu srm " +
            "        on sm.menu_id = srm.menu_id "+
            "    where  srm.role_id = #{roleId}" +
            " order by sm.sort_id")
    @ResultMap("BaseResultMap")
    List<MenuDO> selectByRoleId(@Param("roleId") Integer roleId);
}
