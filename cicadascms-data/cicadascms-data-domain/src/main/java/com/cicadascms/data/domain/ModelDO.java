package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 内容模型表
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName("cms_model")
public class ModelDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(value = "model_id", type = IdType.AUTO)
    private Integer modelId;

    /**
     * 站点id
     */
    private Long siteId;

    /**
     * 模型名称
     */
    private String modelName;

    /**
     * 模型表名称
     */
    private String tableName;

    /**
     * 内容模型，栏目模型
     */
    private Integer modelType;

    /**
     * 字段描述
     */
    private String des;

    /**
     * 模型视图模板
     */
    @TableField(value = "model_view")
    private String modelView;

    /**
     * 状态
     */
    private Boolean status;
}
