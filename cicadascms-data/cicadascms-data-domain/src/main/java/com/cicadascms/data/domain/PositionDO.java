package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 职位表
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_position")
public class PositionDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    /**
     * 职位id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 职位名称
     */
    private String postName;

    /**
     * 职位编号
     */
    private String postCode;

    /**
     * 职位类型字典表(post_type)
     */
    private Integer postType;

    /**
     * 排序字段
     */
    private Integer sortId;

}
