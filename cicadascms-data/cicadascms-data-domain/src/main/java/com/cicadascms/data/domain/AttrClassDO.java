package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 附件分类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@TableName("sys_attr_class")
public class AttrClassDO implements Serializable {

private static final long serialVersionUID=1L;

                @TableId(value = "class_id", type = IdType.AUTO)
                private Integer classId;


    public Integer getClassId() {
            return classId;
            }

        public void setClassId(Integer classId) {
            this.classId = classId;
            }
    
@Override
public String toString() {
        return "AttrClass{" +
                "classId=" + classId +
        "}";
        }
}
