package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.io.Serializable;

/**
 * <p>
 * 导航菜单
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName("cms_nav")
public class NavDO extends BaseDO{

        private static final long serialVersionUID=1L;

        @TableId(value = "nav_id", type = IdType.AUTO)
        private Integer navId;

        /**
         * 导航名称
         */
        private String navName;

        /**
         * target
         */
        private String target;

        /**
         * 链接地址
         */
        private String href;

        /**
         * 上级编号
         */
        private Integer parentId;

        /**
         * 是否拥有子类
         */
        private Integer hasChild;

        /**
         * 导航图标
         */
        private String icon;

        /**
         * 导航状态
         */
        private Boolean status;
}
