package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * 用户职位
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
@Data
@TableName("sys_user_position")
public class UserPositionDO implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer uid;

    private Integer postId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPositionDO that = (UserPositionDO) o;
        return uid.equals(that.uid) &&
                postId.equals(that.postId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid, postId);
    }
}
