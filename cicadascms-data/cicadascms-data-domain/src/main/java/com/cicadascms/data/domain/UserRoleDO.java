package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.google.common.base.Objects;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 用户角色管理表
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
@Data
@ToString
@TableName("sys_user_role")
public class UserRoleDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "角色id")
    private Integer roleId;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserRoleDO that = (UserRoleDO) o;
        return Objects.equal(uid, that.uid) &&
                Objects.equal(roleId, that.roleId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uid, roleId);
    }
}
