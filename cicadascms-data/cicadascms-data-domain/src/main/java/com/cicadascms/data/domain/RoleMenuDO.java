package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.google.common.base.Objects;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 角色菜单表
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
@ApiModel(value="SysRoleMenu对象", description="角色菜单表")
@Getter
@Setter
@ToString
@TableName("sys_role_menu")
public class RoleMenuDO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer roleId;

    private Integer menuId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleMenuDO that = (RoleMenuDO) o;
        return Objects.equal(roleId, that.roleId) &&
                Objects.equal(menuId, that.menuId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(roleId, menuId);
    }
}
