package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import com.google.common.base.Objects;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 机构部门
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
@ApiModel(value="SysDept对象", description="机构部门")
@Getter
@Setter
@ToString(callSuper = true)
@TableName("sys_dept")
public class DeptDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(value = "dept_id", type = IdType.AUTO)
    private Integer deptId;

    private String deptName;

    private String deptCode;

    private Integer parentId;

    @ApiModelProperty(value = "排序字段")
    private Integer sortId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    private boolean hasChildren;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DeptDO dept = (DeptDO) o;
        return Objects.equal(deptId, dept.deptId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(deptId);
    }
}
