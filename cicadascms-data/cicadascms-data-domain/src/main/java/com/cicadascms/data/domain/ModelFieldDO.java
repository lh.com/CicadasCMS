package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 模型字段
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName("cms_model_field")
public class ModelFieldDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer fieldId;

    /**
     * 模型字段
     */
    private Integer modelId;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段类型
     */
    private Integer fieldType;

    /**
     * 数据库字段类型
     */
    private Integer columnType;

    /**
     * 字段属性
     */
    private String fieldConfig;

    /**
     * 是否检索字段
     */
    private Integer isSearchField;

    /**
     * 字段描述
     */
    private String des;
}
