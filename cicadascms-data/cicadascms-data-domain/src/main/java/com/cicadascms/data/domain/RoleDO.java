package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cicadascms.common.base.BaseDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@TableName("sys_role")
public class RoleDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(value = "role_id", type = IdType.AUTO)
    private Integer roleId;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;


    @ApiModelProperty(value = "父id")
    private Integer parentId;

    private String roleName;

    private String roleKey;

    @ApiModelProperty(value = "菜单类型(1，管理角色，2，业务角色)")
    private Integer roleType;

    private String remark;

    private boolean hasChildren;

    @ApiModelProperty(value = "数据权限范围")
    private String dataScope;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RoleDO roleDO = (RoleDO) o;
        return roleId.equals(roleDO.roleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId);
    }
}
