package com.cicadascms.data.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.google.common.base.Objects;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 用户部门关联表
 * </p>
 *
 * @author westboy
 * @date 2019-08-21
 */
@Data
@ToString
@TableName("sys_user_dept")
public class UserDeptDO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer uid;

    private Integer deptId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserDeptDO userDept = (UserDeptDO) o;
        return Objects.equal(uid, userDept.uid) &&
                Objects.equal(deptId, userDept.deptId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uid, deptId);
    }
}
