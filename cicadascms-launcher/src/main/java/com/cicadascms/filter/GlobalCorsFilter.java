package com.cicadascms.filter;

import com.cicadascms.common.constant.Constant;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.GenericArrayType;

/**
 * CorsFilter
 *
 * @author Jin
 */
@Order(0)
@Component
@WebFilter(filterName = "CorsFilter", urlPatterns = {"/*"})
public class GlobalCorsFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;
        // response.setHeader("Access-Control-Allow-Origin",request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "false");
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Max-Age", Constant.CORS_MAX_AGE.toString());
        response.setHeader("Access-Control-Allow-Headers", "*");
        chain.doFilter(req, res);
    }

}