package com.cicadascms.filter;

import com.cicadascms.common.func.Fn;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * GlobalSwaggerFilter 资源转发过滤器
 *
 * @author Jin
 */
@Component
@WebFilter(filterName = "GlobalSwaggerFilter", urlPatterns = {"/api/swagger/*"})
public class GlobalSwaggerFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String requestUri = request.getRequestURI();
        if (Fn.equal(requestUri, "/api/swagger/swagger-resources/configuration/security")) {
            request.getRequestDispatcher("/swagger-resources/configuration/security").forward(request, response);
            return;
        }
        if (Fn.equal(requestUri, "/api/swagger/v2/api-docs")) {
            request.getRequestDispatcher("/v2/api-docs").forward(request, response);
            return;
        }
        if (Fn.equal(requestUri, "/api/swagger/swagger-resources/configuration/ui")) {
            request.getRequestDispatcher("/swagger-resources/configuration/ui").forward(request, response);
            return;
        }
        if (Fn.equal(requestUri, "/api/swagger/swagger-resources")) {
            request.getRequestDispatcher("/swagger-resources").forward(request, response);
            return;
        }
        chain.doFilter(req, res);
    }

}
