package com.cicadascms.filter;


import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.ControllerUtil;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.front.service.IChannelService;
import com.cicadascms.front.service.ISiteService;
import com.cicadascms.security.SecurityUrlProperties;
import com.cicadascms.support.website.WebSiteContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * WebSiteFilter
 *
 * @author Jin
 */
@Order(0)
@Component
@WebFilter(filterName = "WebSiteFilter", urlPatterns = {"/*"})
public class GlobalWebSiteFilter implements Filter {
    private ISiteService siteService;
    private IChannelService channelService;
    private SecurityUrlProperties securityUrlProperties;

    private AntPathMatcher pathMatcher = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        String requestPath = request.getServletPath();
        if (Fn.equal("/favicon.ico", requestPath)) {
            request.getRequestDispatcher("/static/favicon.ico").forward(request, response);
            return;
        }

        //Jin 跳过接口请求
        if (Fn.startWith(requestPath, "/api")) {
            chain.doFilter(req, res);
            return;
        }

        //Jin 跳过静态资源请求
        if (Fn.startWith(requestPath, "/cms/admin")) {
            chain.doFilter(req, res);
            return;
        }

        String domain = ControllerUtil.getDomain(request);
        SiteDO siteDO = siteService.findByDomain(domain);

        if (Fn.isNull(siteDO)) {
            ChannelDO channelDO = channelService.findByDomain(domain);
            if (Fn.isNotNull(channelDO)) {
                siteDO = siteService.getById(channelDO.getSiteId());
                //Jin 在栏目绑定域名的情况下直接转发到栏目首页
                if (Fn.equal("/", requestPath)) {
                    WebSiteContextHolder.set(siteDO);
                    request.getRequestDispatcher(channelDO.getChannelUrlPath()).forward(request, response);
                }
            } else {
                siteDO = siteService.getDefaultSite();
            }
        }
        WebSiteContextHolder.set(siteDO);
        chain.doFilter(req, res);
        WebSiteContextHolder.remove();

    }


    @Autowired
    public void setSiteService(ISiteService siteService) {
        this.siteService = siteService;
    }

    @Autowired
    public void setChannelService(IChannelService channelService) {
        this.channelService = channelService;
    }

    @Autowired
    public void setSecurityUrlProperties(SecurityUrlProperties securityUrlProperties) {
        this.securityUrlProperties = securityUrlProperties;
    }
}