package com.cicadascms.config.executor;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;


/**
 * GlobalExecutorConfig
 *
 * @author Jin
 */
@EnableAsync
@Configuration
public class GlobalExecutorConfig extends AsyncConfigurerSupport {

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor threadTask = new ThreadPoolTaskExecutor();
        threadTask.setCorePoolSize(10);
        threadTask.setCorePoolSize(20);
        threadTask.setQueueCapacity(500);
        threadTask.setThreadNamePrefix("CicadasCMS-Global-Executor-");
        threadTask.initialize();
        return threadTask;
    }

}
