package com.cicadascms.initializer.resource;

import cn.hutool.core.io.FileUtil;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.EvnUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * JarResourcesCopyToExternalInitializer
 * springboot打jar运行将静态资源复制外部目录
 *
 * @author Jin
 */
@Slf4j
@Component
@Configuration
public class JarResourcesCopyToExternalInitializer implements CommandLineRunner {

    public void processJarFile() throws IOException {
        URL url = JarResourcesCopyToExternalInitializer.class.getProtectionDomain().getCodeSource().getLocation();
        String jarfilePath = URLDecoder.decode(url.getPath(), "UTF-8");

        if (jarfilePath.contains("\\")) {
            jarfilePath = jarfilePath.replace("\\", "/");
        }
        if (Fn.startWith(jarfilePath, "file:/")) {
            if (Fn.startWith(System.getProperty("os.name"), "win")) {
                jarfilePath = jarfilePath.replace("file:/", "");
            } else {
                jarfilePath = jarfilePath.replace("file:", "");
            }
        }
        if (Fn.endsWith(jarfilePath, ".jar!/BOOT-INF/classes!/")) {
            jarfilePath = jarfilePath.replace("!/BOOT-INF/classes!", "");
        }

        final JarFile jarFile = new JarFile(jarfilePath.replace("/", File.separator));
        Enumeration<JarEntry> entries = jarFile.entries();

        while (entries.hasMoreElements()) {
            JarEntry jarEntry = entries.nextElement();
            //复制逻辑代码中可能存在的资源的文件
            if (Fn.startWith(jarEntry.getName(), "BOOT-INF/lib/cicadascms-logic") && Fn.endsWith(jarEntry.getName(), ".jar")) {
                //创建临时目录
                EvnUtils.checkAndCreateResourceDir("tmp");
                String libJarFileName = jarEntry.getName().replace("BOOT-INF/lib", "tmp");
                String libJarFilePath = createFile(libJarFileName.replace("/", File.separator), jarEntry, jarFile);
                final JarFile libJarFile = new JarFile(libJarFilePath);
                Enumeration<JarEntry> libJarEntries = libJarFile.entries();
                //遍历复制出静态资源文件
                while (libJarEntries.hasMoreElements()) {
                    JarEntry libJarEntry = libJarEntries.nextElement();
                    if (Fn.startWith(libJarEntry.getName(), "static/") || Fn.startWith(libJarEntry.getName(), "templates/")) {
                        this.createFile(libJarEntry.getName(), libJarEntry, libJarFile);
                    }
                }
                libJarFile.close();
                FileUtil.del(libJarFilePath);
            } else if (Fn.startWith(jarEntry.getName(), "BOOT-INF/classes/static/") || Fn.startWith(jarEntry.getName(), "BOOT-INF/classes/templates/")) {
                String fileName = jarEntry.getName().replace("BOOT-INF/classes", "");
                this.createFile(fileName, jarEntry, jarFile);
            }
        }

        jarFile.close();
    }

    private String createFile(String fileName, JarEntry jarEntry, JarFile jarFile) throws IOException {
        if (jarEntry.isDirectory()) {
            String folderPath = EvnUtils.checkAndCreateResourceDir(fileName);
            log.info("创建目录成功 - {}", folderPath);
            return folderPath;
        }
        String filePath = EvnUtils.checkAndCreateNewFile(fileName);
        File file = new File(filePath);
        InputStream inputStream = jarFile.getInputStream(jarEntry);
        OutputStream outputStream = new FileOutputStream(file);
        IOUtils.copy(inputStream, outputStream);
        inputStream.close();
        outputStream.flush();
        outputStream.close();
        log.info("创建文件成功 - {}", filePath);
        return filePath;
    }

    @Override
    public void run(String... args) throws Exception {
        if (EvnUtils.isJarActive(JarResourcesCopyToExternalInitializer.class)) {
            log.info("========[ 开始复制JAR内资源到外部目录 ]========");
            this.processJarFile();
            log.info("========[ 复制JAR内资源到外部目录完成 ]========\n");
        }
    }
}
