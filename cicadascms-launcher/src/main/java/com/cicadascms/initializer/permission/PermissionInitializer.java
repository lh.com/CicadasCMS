package com.cicadascms.initializer.permission;

import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.plugin.IPlugin;
import com.cicadascms.plugin.annotation.Plugin;
import com.cicadascms.plugin.manager.PluginsManager;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 插件初始化
 *
 * @author Jin
 */
@Slf4j
@Component
@Configuration
public class PermissionInitializer implements CommandLineRunner {

    public void registerPermission() {
        String[] pluginMap = SpringContextUtils.getBeanNamesForAnnotation(Api.class);

    }

    @Override
    public void run(String... args) throws Exception {
        log.info("========[ 开始注册插件]========");
        this.registerPermission();
        log.info("========[ 插件注册完成 ]========\n");
    }
}
