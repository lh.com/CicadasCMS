package com.cicadascms.exception;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.exception.ServiceException;
import com.cicadascms.common.resp.R;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.logger.constant.LogTypeEnum;
import com.cicadascms.logger.event.LogEvent;
import com.cicadascms.logger.utils.LogUtils;
import com.cicadascms.data.domain.LogDO;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.RequestDispatcher;
import java.util.Objects;

import static javax.servlet.RequestDispatcher.ERROR_STATUS_CODE;

/**
 * 全局异常处理器
 *
 * @author Jin
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({Throwable.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R exception(Throwable throwable) {
        LogDO logDO = LogUtils.buildSysLog(LogTypeEnum.错误日志);
        logDO.setTitle(throwable.toString());
        logDO.setException(throwable.getMessage());
        SpringContextUtils.publishEvent(new LogEvent(logDO));
        throwable.printStackTrace();
        return R.error().setMessage(throwable.getMessage());
    }

    @ExceptionHandler({FrontNotFoundException.class})
    public ModelAndView frontNotFoundException(FrontNotFoundException frontNotFoundException) {
        ModelAndView view = new ModelAndView("forward:/error");
        view.addObject(RequestDispatcher.ERROR_MESSAGE, frontNotFoundException.getMessage());
        view.addObject(RequestDispatcher.ERROR_STATUS_CODE, HttpStatus.NOT_FOUND.value());
        return view;
    }

    @ExceptionHandler({BadCredentialsException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public R badCredentialsException() {
        return R.newBuilder().code(Constant.ERROR_CODE).message("账号或密码错误！").build();
    }

    @ExceptionHandler({UnapprovedClientAuthenticationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public R unapprovedClientAuthenticationException(UnapprovedClientAuthenticationException exception) {
        return R.newBuilder().code(Constant.ERROR_CODE).message(exception.getMessage()).build();
    }

    @ExceptionHandler({IllegalArgumentException.class, ServiceException.class, BindException.class})
    @ResponseStatus(HttpStatus.OK)
    public R illegalArgumentException(Exception exception) {
        if (exception instanceof BindException) {
            BindException bindException = (BindException) exception;
            return R.newBuilder().
                    code(Constant.ERROR_CODE).
                    message(Objects.requireNonNull(bindException.getFieldError()).getDefaultMessage().concat(" [ " + bindException.getFieldError().getField() + " ]")).
                    build();
        }
        return R.newBuilder().code(Constant.ERROR_CODE).message(exception.getMessage()).build();
    }

    @ExceptionHandler({AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public R accessDeniedException(AccessDeniedException accessDeniedException) {
        return R.newBuilder().code(Constant.ERROR_CODE).message(accessDeniedException.getMessage()).build();
    }

}
