package com.cicadascms;

import com.cicadascms.support.annotation.CicadasCmsApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@CicadasCmsApplication
public class CicadasCmsLauncher extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(CicadasCmsLauncher.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CicadasCmsLauncher.class);
    }


}
