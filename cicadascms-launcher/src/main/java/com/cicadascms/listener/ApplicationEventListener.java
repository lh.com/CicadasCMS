package com.cicadascms.listener;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ApplicationEventListener implements ApplicationListener {

    @Value("${application.name}")
    private String applicationName;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ApplicationReadyEvent) {
            System.out.println();
            System.out.println();
            System.out.println(
                    "   ██████  ██                        ██                     ██████  ████     ████  ████████\n" +
                            "  ██░░░░██░░                        ░██                    ██░░░░██░██░██   ██░██ ██░░░░░░ \n" +
                            " ██    ░░  ██  █████   ██████       ░██  ██████    ██████ ██    ░░ ░██░░██ ██ ░██░██       \n" +
                            "░██       ░██ ██░░░██ ░░░░░░██   ██████ ░░░░░░██  ██░░░░ ░██       ░██ ░░███  ░██░█████████\n" +
                            "░██       ░██░██  ░░   ███████  ██░░░██  ███████ ░░█████ ░██       ░██  ░░█   ░██░░░░░░░░██\n" +
                            "░░██    ██░██░██   ██ ██░░░░██ ░██  ░██ ██░░░░██  ░░░░░██░░██    ██░██   ░    ░██       ░██\n" +
                            " ░░██████ ░██░░█████ ░░████████░░██████░░████████ ██████  ░░██████ ░██        ░██ ████████ \n" +
                            "  ░░░░░░  ░░  ░░░░░   ░░░░░░░░  ░░░░░░  ░░░░░░░░ ░░░░░░    ░░░░░░  ░░         ░░ ░░░░░░░░   Version 2.0\n");
            System.out.println();
            System.out.println();
            log.info("{} - 启动成功 [{}]", applicationName, DateUtil.now());
        }
    }


}

