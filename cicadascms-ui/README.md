#### 项目说明

`安装依赖：mpm install`

`项目启动：mpm run dev`

`项目打包：mpm run build:prod`

#### 界面预览

![后台登录界面](../docs/picture/login.jpg "后台登录界面")

![后台用户管理界面](../docs/picture/user.jpg "后台用户管理界面")

![后台部门管理界面](../docs/picture/dept.jpg "后台部门管理界面")

![后台菜单管理界面](../docs/picture/dept.jpg "后台菜单管理界面")

![后台字典管理界面](../docs/picture/dict.jpg "后台字典管理界面")





