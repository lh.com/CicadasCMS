import request from '@/utils/request'
import QueryString from 'qs'

const urlPrefix = '/system/quartzJob'

export function page(query) {
  return request({
    url: urlPrefix + '/list',
    method: 'get',
    params: query,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      return QueryString.stringify(data)
    }],
  })
}

export function findById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'get'
  })
}

export function resumeById(id) {
  return request({
    url: urlPrefix + '/' + id + '/resume',
    method: 'get'
  })
}

export function pauseById(id) {
  return request({
    url: urlPrefix + '/' + id + '/pause',
    method: 'get'
  })
}

export function triggerById(id) {
  return request({
    url: urlPrefix + '/' + id + '/trigger',
    method: 'get'
  })
}

export function deleteById(id) {
  return request({
    url: urlPrefix + '/' + id,
    method: 'delete'
  })
}

export function save(data) {
  return request({
    url: urlPrefix,
    method: 'post',
    data
  })
}

export function updateById(data) {
  return request({
    url: urlPrefix,
    method: 'put',
    data
  })
}
