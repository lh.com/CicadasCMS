CicadasCMS 2.0 开发中... 老版本请看master分支


#### 项目结构

```
CicadasCMS v2.0
    ├── cicadascms-builder                  : 代码生成器
    ├── cicadascms-commons                  : 通用组件
    ├── cicadascms-launcher                 : 项目启动器   
    ├── cicadascms-logics                   : 业务逻辑 
            ├── cicadascms-logics-admin     : 后台功能逻辑 
            ├── cicadascms-logics-front     : 前台功能逻辑 
            └── cicadascms-logics-rbac      : 权限管理逻辑  
    ├── cicadascms-plugins                  : 扩展插件  
    ├── cicadascms-ui                       : 前端    
    └── doc                       
``` 

#### 功能说明

| 功能   | 进度 | 
| :---- | :---- | 
| 通用权限管理功能 | 已完成|
| 前台业务逻辑功能 | 进行中|
| 后台业务逻辑功能 | 进行中|


#### 界面预览
![后台登录界面](docs/picture/login.jpg "后台登录界面")

![后台菜单管理界面](docs/picture/menu.jpg "后台菜单管理界面")


<img src="https://img.shields.io/badge/JDK-1.8+-green.svg" alt="jdk version">
<img src="https://img.shields.io/badge/Spring%20Boot-2.4.2-blue.svg" alt="Coverage Status">
<img src="https://img.shields.io/badge/Mybatis%20Plus-3.4.2-red.svg" alt="Coverage Status">




