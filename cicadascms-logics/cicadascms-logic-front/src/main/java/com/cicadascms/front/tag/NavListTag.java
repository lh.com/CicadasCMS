package com.cicadascms.front.tag;

import org.beetl.core.tag.GeneralVarTagBinding;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 导航列表标签
 *
 * @author Jin
 */
@Component
@Scope("prototype")
public class  NavListTag extends GeneralVarTagBinding {


    @Override
    public void render() {
        HttpServletRequest request = (HttpServletRequest) ctx.getGlobal("request");
        String siteId = (String) this.getAttributeValue("siteId");

    }

}
