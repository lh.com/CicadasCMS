package com.cicadascms.front.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.ModelDO;

/**
 * <p>
 * 内容模型表 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IModelService extends IService<ModelDO> {


}
