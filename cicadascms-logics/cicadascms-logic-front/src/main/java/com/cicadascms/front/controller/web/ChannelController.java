package com.cicadascms.front.controller.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.support.annotation.CurrentWebSite;
import com.cicadascms.support.website.WebSiteViewRender;
import com.cicadascms.front.service.IChannelService;
import com.cicadascms.front.service.IContentService;
import com.cicadascms.front.vo.ChannelVO;
import com.cicadascms.front.vo.ContentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ChannelController {
    private IChannelService channelService;
    private IContentService contentService;

    @RequestMapping("/{channelUrlPath}")
    public String index(@PathVariable String channelUrlPath, @CurrentWebSite SiteDO siteDO, Model model) {
        return page(channelUrlPath, 1, siteDO, model);
    }


    @RequestMapping("/{channelUrlPath}_{pageNumber:\\d+}")
    public String page(@PathVariable String channelUrlPath, @PathVariable Integer pageNumber, @CurrentWebSite SiteDO siteDO, Model model) {
        if (Fn.isNull(siteDO)) {
            throw new FrontNotFoundException("站点已被删除或不存在！");
        }
        model.addAttribute("site", siteDO);
        ChannelVO channelVO = channelService.findBySiteIdAndChannelUrlPath(siteDO.getSiteId(), channelUrlPath);
        if (Fn.isNull(channelVO)) {
            throw new FrontNotFoundException("栏目已被删除或不存在！");
        }
        model.addAttribute("channel", channelVO);
        IPage<ContentVO> page = contentService.findByPageNumberAndChannelId(pageNumber, channelVO.getChannelId());
        model.addAttribute("page", page);
        return WebSiteViewRender.channelViewRender(siteDO.getPcTemplateDir(), siteDO.getMobileTemplateDir(), channelVO.getChannelView());
    }

    @Autowired
    public void setContentService(IContentService contentService) {
        this.contentService = contentService;
    }

    @Autowired
    public void setChannelService(IChannelService channelService) {
        this.channelService = channelService;
    }


}
