package com.cicadascms.front.tag;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.func.Fn;
import com.cicadascms.front.vo.ContentVO;
import com.cicadascms.support.htmltag.AbstractHtmlTag;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 栏目列表标签
 *
 * @author Jin
 */
@Component
@Scope("prototype")
public class ChannelListTag extends AbstractHtmlTag {


    @Override
    public void render() {
        Integer channelId = getAttrToInt("channelId");
        if (Fn.isNotNull(channelId)) {

        } else {
            IPage<ContentVO> page = getContentPage();
            for (int i = 0; i < page.getRecords().size(); i++) {
                this.binds(i);
                this.doBodyRender();
            }
        }
    }

}
