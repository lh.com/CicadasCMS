package com.cicadascms.front.controller.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.front.vo.ContentIndexVO;
import com.cicadascms.lucene.manager.LuceneManager;
import com.cicadascms.lucene.utils.IndexObjectUtil;
import com.cicadascms.support.annotation.CurrentWebSite;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
@AllArgsConstructor
public class SearchController {
    private final LuceneManager luceneManager;

    @GetMapping("/{searchType}/search")
    @ResponseBody
    public String index(@CurrentWebSite SiteDO siteDO,
                        @PathVariable String searchType,
                        @RequestParam("wd") String wd,
                        @RequestParam(value = "current", defaultValue = "1") Integer current,
                        @RequestParam(value = "size", defaultValue = "20") Integer size,
                        Model model) throws IOException {
        if (Fn.isNull(siteDO)) throw new FrontNotFoundException("站点已被删除或不存在！");
        model.addAttribute("site", siteDO);
        if (Fn.isEmpty(wd)) throw new FrontNotFoundException("关键字不能为空！");
        IPage<ContentIndexVO> page = luceneManager.page(wd, current, size, ContentIndexVO.class, IndexObjectUtil.getQueryFieldNames(ContentIndexVO.class));
        model.addAttribute(page);
        return Fn.toJson(page.getRecords());
    }

}
