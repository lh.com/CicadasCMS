package com.cicadascms.front.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.data.mapper.ModelFieldMapper;
import com.cicadascms.front.service.IModelFieldService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 模型字段 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service
public class ModelFieldServiceImpl extends BaseService<ModelFieldMapper, ModelFieldDO> implements IModelFieldService {


    @Override
    public List<ModelFieldDO> findByModelId(Serializable modelId) {
        return this.list(getLambdaQueryWrapper().eq(ModelFieldDO::getModelId, modelId));
    }

}
