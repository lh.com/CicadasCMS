package com.cicadascms.front.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.front.vo.ChannelVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容分类表 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IChannelService extends IService<ChannelDO> {

    /**
     * 根据域名查询
     *
     * @param domain
     * @return
     */
    ChannelDO findByDomain(String domain);

    /**
     * CHA XUN
     *
     * @param siteId
     * @param channelUrlPath
     * @return
     */
    ChannelVO findBySiteIdAndChannelUrlPath(Integer siteId, String channelUrlPath);

    /**
     * 查询扩展表字段信息
     *
     * @param tableName
     * @param channelId
     * @return
     */
    Map<String, Object> findExtendFieldByTableNameAndChannelId(String fields,String tableName, Integer channelId);

    /**
     * 获取父栏目
     *
     * @param parentId
     * @return
     */
    List<ChannelDO> findByParentId(Integer parentId);

}
