package com.cicadascms.front.controller.web;

import com.cicadascms.common.exception.FrontNotFoundException;
import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.support.annotation.CurrentWebSite;
import com.cicadascms.support.website.WebSiteContextHolder;
import com.cicadascms.support.website.WebSiteViewRender;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class IndexController {

    private final static String DEFAULT_INDEX_VIEW_NAME = "/index";

    @GetMapping("/")
    public String index(@CurrentWebSite SiteDO siteDO, Model model) {
        if (Fn.isNull(siteDO)) {
            throw new FrontNotFoundException("站点已被删除或不存在！");
        }
        model.addAttribute("site", siteDO);
        return  WebSiteViewRender.indexViewRender(siteDO.getPcTemplateDir(), siteDO.getMobileTemplateDir(), DEFAULT_INDEX_VIEW_NAME);
    }

}