package com.cicadascms.front.tag;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.cicadascms.common.func.Fn;
import com.cicadascms.front.service.ISiteService;
import org.beetl.core.tag.GeneralVarTagBinding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 内容列表标签
 *
 * @author Jin
 */
@Component
@Scope("prototype")
public class ContentListTag extends GeneralVarTagBinding {
    private ISiteService siteService;

    @Override
    public void render() {
        HttpServletRequest request = (HttpServletRequest) ctx.getGlobal("request");
        String titleLen = (String) this.getAttributeValue("titleLen");
        String channelId = (String) this.getAttributeValue("channelId");
        if (Fn.isEmpty(channelId)) {
            IPage page = (IPage) request.getAttribute("page");
            if (Fn.isNull(page)) return;


        } else {
            String siteId = (String) this.getAttributeValue("siteId");
            String hasChild = (String) this.getAttributeValue("hasChild");
            String isPic = (String) this.getAttributeValue("isPic");
            String isRecommend = (String) this.getAttributeValue("isRecommend");
            String orderBy = (String) this.getAttributeValue("orderBy");
            String size = (String) this.getAttributeValue("size");
        }
    }

    @Autowired
    public void setSiteService(ISiteService siteService) {
        this.siteService = siteService;
    }
}