package com.cicadascms.front.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * ChannelVO对象
 * </p>
 *
 * @author Jin
 */
@Data
@ApiModel(value = "ChannelVO对象", description = "内容分类表")
public class ChannelVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer channelId;
    /**
     * 站点编号
     */
    @ApiModelProperty(value = "2-站点编号")
    private Integer siteId;

    /**
     * 域名
     */
    @ApiModelProperty(value = "2-栏目域名")
    private String domain;

    /**
     * 分类明细
     */
    @ApiModelProperty(value = "3-分类明细")
    private String channelName;
    /**
     * 栏目模型编号
     */
    @ApiModelProperty(value = "4-栏目模型编号")
    private Integer channelModelId;
    /**
     * 内容模型编号
     */
    @ApiModelProperty(value = "5-内容模型编号")
    private String contentModelIds;
    /**
     * 栏目路径
     */
    @ApiModelProperty(value = "6-栏目路径")
    private String channelUrlPath;
    /**
     * 父类编号
     */
    @ApiModelProperty(value = "7-父类编号")
    private Long parentId;
    /**
     * 单页栏目（0：不是，1：是）
     */
    @ApiModelProperty(value = "8-单页栏目（0：不是，1：是）")
    private Boolean isAlone;
    /**
     * 单页内容
     */
    @ApiModelProperty(value = "9-单页内容")
    private String aloneContent;
    /**
     * 首页视图模板
     */
    @ApiModelProperty(value = "10-首页视图模板")
    private String channelView;
    /**
     * 导航
     */
    @ApiModelProperty(value = "13-导航")
    private Boolean isNav;
    /**
     * 外链地址
     */
    @ApiModelProperty(value = "14-外链地址")
    private String url;
    /**
     * 是否有子类
     */
    @ApiModelProperty(value = "15-是否有子类")
    private Boolean hasChildren;
    /**
     * 栏目分页数量
     */
    @ApiModelProperty(value = "16-栏目分页数量")
    private Integer pageSize;
    /**
     * 当前栏目下的是否支持全文搜索
     */
    @ApiModelProperty(value = "17-当前栏目下的是否支持全文搜索")
    private Boolean allowSearch;
    /**
     * 栏目分类
     */
    @ApiModelProperty(value = "18-栏目分类")
    private Integer channelType;
    /**
     * 栏目图标
     */
    @ApiModelProperty(value = "19-栏目图标")
    private String channelIcon;

    /**
     * 栏目模型扩展字段
     */
    @ApiModelProperty(value = "19-栏目模型扩展字段")
    private Map<String, Object> ext;

}
