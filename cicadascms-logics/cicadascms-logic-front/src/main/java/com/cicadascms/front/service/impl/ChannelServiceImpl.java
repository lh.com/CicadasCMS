package com.cicadascms.front.service.impl;


import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.data.mapper.ChannelMapper;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.front.service.IChannelService;
import com.cicadascms.front.vo.ChannelVO;
import com.cicadascms.front.wrapper.ChannelWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容分类表 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service
@AllArgsConstructor
public class ChannelServiceImpl extends BaseService<ChannelMapper, ChannelDO> implements IChannelService {

    @Override
    public ChannelDO findByDomain(String domain) {
        return super.getOne(getLambdaQueryWrapper().eq(ChannelDO::getDomain, domain));
    }

    @Override
    public ChannelVO findBySiteIdAndChannelUrlPath(Integer siteId, String channelUrlPath) {
        ChannelDO channelDO = baseMapper.selectOne(getLambdaQueryWrapper()
                .eq(ChannelDO::getSiteId, siteId)
                .eq(ChannelDO::getChannelUrlPath, channelUrlPath));
        return ChannelWrapper.newBuilder().entityVO(channelDO);
    }

    @Override
    public Map<String, Object> findExtendFieldByTableNameAndChannelId(String fields, String tableName, Integer channelId) {
        return baseMapper.selectByTableNameAndChannelId(fields, tableName, channelId);
    }

    @Override
    public List<ChannelDO> findByParentId(Integer parentId) {
        return baseMapper.selectList(getLambdaQueryWrapper().eq(ChannelDO::getParentId, parentId));
    }

}
