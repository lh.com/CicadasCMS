package com.cicadascms.system.vo;

import com.cicadascms.common.tree.TreeNode;
import com.cicadascms.data.domain.MenuDO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "MenuVo", description = "SysMenu对象")
@Data
@EqualsAndHashCode(callSuper = true)
public class MenuVO extends MenuDO implements TreeNode<MenuVO> {

    String parentName;
    List<MenuVO> children;

    @Override
    public Serializable getCurrentNodeId() {
        return this.getMenuId();
    }

    @Override
    public Serializable getParentNodeId() {
        return this.getParentId();
    }

}
