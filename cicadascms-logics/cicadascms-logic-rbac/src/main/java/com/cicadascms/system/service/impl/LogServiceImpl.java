package com.cicadascms.system.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import com.cicadascms.logger.event.LogEvent;
import com.cicadascms.system.dto.LogQueryDTO;
import com.cicadascms.data.domain.LogDO;
import com.cicadascms.data.mapper.SysLogMapper;
import com.cicadascms.system.service.ILogService;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>
 * 日志表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Service
public class LogServiceImpl extends BaseService<SysLogMapper, LogDO> implements ILogService {

    @Override
    public R page(LogQueryDTO logQueryDTO) {
        LogDO log = logQueryDTO.convertToEntity();
        return R.ok(baseMapper.selectPage(logQueryDTO.page(), getLambdaQueryWrapper().setEntity(log)));
    }

    @Override
    public R findById(Serializable id) {
        LogDO log = baseMapper.selectById(id);
        return R.ok(log);
    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

    @Async
    @Order
    @EventListener(LogEvent.class)
    @Override
    public void asyncSave(LogEvent event) {
        LogDO logDO = (LogDO) event.getSource();
        baseMapper.insert(logDO);
    }

}
