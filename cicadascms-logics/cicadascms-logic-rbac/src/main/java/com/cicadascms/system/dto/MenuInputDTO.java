package com.cicadascms.system.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.MenuDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * MenuInputDTO对象
 * 系统菜单表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="InputMenuDTO对象")
public class MenuInputDTO extends BaseDTO<MenuInputDTO, MenuDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 租户id
    */
    @ApiModelProperty(value = "1-租户id" )
    private Integer tenantId;
    /**
    * 父菜单编号
    */
    @ApiModelProperty(value = "2-父菜单编号" )
    private Integer parentId;
    /**
    * 菜单名称

    */
    @ApiModelProperty(value = "3-菜单名称" )
    private String menuName;
    /**
    * 前端路由地址
    */
    @ApiModelProperty(value = "4-前端路由地址" )
    private String menuPath;
    /**
    * 图标
    */
    @ApiModelProperty(value = "5-图标" )
    private String menuIcon;
    /**
    * 菜单类型（1,一级菜单，2，二级菜单，3,按钮）
    */
    @ApiModelProperty(value = "6-菜单类型（1,一级菜单，2，二级菜单，3,按钮）" )
    private Integer menuType;
    /**
    * 前端组件
    */
    @ApiModelProperty(value = "7-前端组件" )
    private String component;
    /**
    * 跳转链接
    */
    @ApiModelProperty(value = "8-跳转链接" )
    private String redirectUrl;
    /**
    * 权限标识
    */
    @ApiModelProperty(value = "9-权限标识" )
    private String permissionKey;
    /**
    * 菜单状态
    */
    @ApiModelProperty(value = "10-菜单状态" )
    private Boolean status;
    /**
    * 排序字段
    */
    @ApiModelProperty(value = "11-排序字段" )
    private Integer sortId;
    private String remake;

    public static Converter<MenuInputDTO, MenuDO> converter = new Converter<MenuInputDTO, MenuDO>() {
        @Override
        public MenuDO doForward(MenuInputDTO menuInputDTO) {
            return WarpsUtils.copyTo(menuInputDTO, MenuDO.class);
        }

        @Override
        public MenuInputDTO doBackward(MenuDO menu) {
            return WarpsUtils.copyTo(menu, MenuInputDTO.class);
        }
    };

    @Override
    public MenuDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public MenuInputDTO convertFor(MenuDO menu) {
        return converter.doBackward(menu);
    }
}
