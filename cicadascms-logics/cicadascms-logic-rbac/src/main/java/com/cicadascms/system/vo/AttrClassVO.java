package com.cicadascms.system.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * AttrClassVO对象
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value="AttrClassVO对象", description="附件分类")
public class AttrClassVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer classId;

}
