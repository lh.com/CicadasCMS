package com.cicadascms.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;

import com.cicadascms.support.annotation.AvoidRepeatSubmit;
import com.cicadascms.system.dto.PositionInputDTO;
import com.cicadascms.system.dto.PositionQueryDTO;
import com.cicadascms.system.dto.PositionUpdateDTO;
import com.cicadascms.data.domain.PositionDO;
import com.cicadascms.system.service.IPositionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 职位 控制器
 * </p>
 *
 * @author jin
 * @since 2020-08-25
 */
@Api(tags = "S-岗位管理接口")
@RestController
@RequestMapping("/system/position")
@AllArgsConstructor
public class PositionController {
    private final IPositionService positionService;

    @ApiOperation(value = "职位列表接口")
    @GetMapping("/list")
    public R<List<PositionDO>> list() {
        return R.ok(positionService.list());
    }

    @ApiOperation(value = "职位列表接口")
    @GetMapping("/page")
    public R<Page<PositionDO>> page(PositionQueryDTO positionQueryDTO) {
        return positionService.page(positionQueryDTO);
    }

    @AvoidRepeatSubmit
    @ApiOperation(value = "职位保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid PositionInputDTO positionInputDTO) {
        return positionService.save(positionInputDTO);
    }

    @AvoidRepeatSubmit
    @ApiOperation(value = "职位更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid PositionUpdateDTO positionUpdateDTO) {
        return positionService.update(positionUpdateDTO);
    }

    @ApiOperation(value = "职位详情接口")
    @GetMapping("/{id}")
    public R<PositionDO> getById(@PathVariable Long id) {
        return positionService.findById(id);
    }

    @ApiOperation(value = "职位删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return positionService.deleteById(id);
    }


}