package com.cicadascms.system.controller;

import com.cicadascms.common.resp.R;
import com.cicadascms.system.dto.DeptInputDTO;
import com.cicadascms.system.dto.DeptQueryDTO;
import com.cicadascms.system.dto.DeptUpdateDTO;
import com.cicadascms.system.service.IDeptService;
import com.cicadascms.system.vo.DeptVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "S-部门管理接口")
@RestController
@RequestMapping("/system/dept")
public class DeptController {
    @Autowired
    private IDeptService deptService;

    @ApiOperation(value = "部门树列表接口")
    @GetMapping("/tree")
    public R<List<DeptVO>> getTree() {
        return R.ok(deptService.getTree());
    }

    @ApiOperation(value = "列表查询接口")
    @GetMapping("/list")
    public R<List<DeptVO>> getList(DeptQueryDTO deptQueryDTO) {
        return R.ok(deptService.findList(deptQueryDTO));
    }

    @ApiOperation(value = "部门保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid DeptInputDTO deptInputDTO) {
        return deptService.save(deptInputDTO);
    }

    @ApiOperation(value = "部门更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid DeptUpdateDTO deptUpdateDTO) {
        return deptService.update(deptUpdateDTO);
    }

    @ApiOperation(value = "部门详情接口")
    @GetMapping("/{id}")
    public R<DeptVO> getById(@PathVariable String id) {
        return deptService.findById(id);
    }

    @ApiOperation(value = "机构部门删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable String id) {
        return deptService.deleteById(id);
    }

}
