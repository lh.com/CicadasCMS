package com.cicadascms.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.common.base.LoginUser;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.dto.UserInputDTO;
import com.cicadascms.system.dto.UserQueryDTO;
import com.cicadascms.system.dto.UserUpdateDTO;
import com.cicadascms.data.domain.UserDO;

import java.io.Serializable;

/**
 * <p>
 * 基础账户表 服务类
 * </p>
 *
 * @author westboy
 * @date 2019-05-23
 */
public interface IUserService extends IService<UserDO> {

    /**
     * 登录接口
     * @param username
     * @return
     */
    LoginUser login(String username);

    /**
     * 修改密码
     * @param oldPassword
     * @param newPassword
     * @return
     */
    R changePassword(String oldPassword, String newPassword);


    /**
     * 分页方法
     * @param userQueryDTO
     * @return
     */
    R page(UserQueryDTO userQueryDTO);

    /**
     * 保存方法
     * @param userInputDTO
     * @return
     */
    R save(UserInputDTO userInputDTO);

    /**
     * 更新方法
     * @param userUpdateDTO
     * @return
     */
    R update(UserUpdateDTO userUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);

}
