package com.cicadascms.system.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AttrClassDO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * AttrClassInputDTO对象
 * 附件分类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="InputAttrClassDTO对象")
public class AttrClassInputDTO extends BaseDTO<AttrClassInputDTO, AttrClassDO>  implements Serializable {

    private static final long serialVersionUID = 1L;


    public static Converter<AttrClassInputDTO, AttrClassDO> converter = new Converter<AttrClassInputDTO, AttrClassDO>() {
        @Override
        public AttrClassDO doForward(AttrClassInputDTO attrClassInputDTO) {
            return WarpsUtils.copyTo(attrClassInputDTO, AttrClassDO.class);
        }

        @Override
        public AttrClassInputDTO doBackward(AttrClassDO attrClassDO) {
            return WarpsUtils.copyTo(attrClassDO, AttrClassInputDTO.class);
        }
    };

    @Override
    public AttrClassDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AttrClassInputDTO convertFor(AttrClassDO attrClassDO) {
        return converter.doBackward(attrClassDO);
    }
}
