package com.cicadascms.system.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.cicadascms.data.domain.AttrClassDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * AttrClassQueryDTO对象
 * 附件分类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="AttrClassQueryDTO对象")
public class AttrClassQueryDTO extends BaseDTO<AttrClassQueryDTO, AttrClassDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;


    public Page<AttrClassDO> page() {
        Page<AttrClassDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<AttrClassQueryDTO, AttrClassDO> converter = new Converter<AttrClassQueryDTO, AttrClassDO>() {
        @Override
        public AttrClassDO doForward(AttrClassQueryDTO attrClassQueryDTO) {
            return WarpsUtils.copyTo(attrClassQueryDTO, AttrClassDO.class);
        }

        @Override
        public AttrClassQueryDTO doBackward(AttrClassDO attrClassDO) {
            return WarpsUtils.copyTo(attrClassDO, AttrClassQueryDTO.class);
        }
    };

    @Override
    public AttrClassDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AttrClassQueryDTO convertFor(AttrClassDO attrClassDO) {
        return converter.doBackward(attrClassDO);
    }
}
