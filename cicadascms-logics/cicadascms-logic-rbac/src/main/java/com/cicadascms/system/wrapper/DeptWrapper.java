package com.cicadascms.system.wrapper;

import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.data.domain.DeptDO;
import com.cicadascms.system.service.IDeptService;
import com.cicadascms.system.vo.DeptVO;


public class DeptWrapper implements BaseWrapper<DeptDO, DeptVO> {

    private static final IDeptService deptService;

    static {
        deptService = SpringContextUtils.getBean(IDeptService.class);
    }

    public static DeptWrapper newBuilder() {
        return new DeptWrapper();
    }

    @Override
    public DeptVO entityVO(DeptDO entity) {
        DeptVO deptVo = WarpsUtils.copyTo(entity, DeptVO.class);
        assert deptVo != null;
        if (Fn.isNotNull(deptVo.getParentId()) && Fn.notEqual(Constant.PARENT_ID, deptVo.getParentId())) {
            DeptDO parentDept = deptService.getById(deptVo.getParentId());
            deptVo.setParentName(parentDept.getDeptName());
        } else {
            deptVo.setParentName("顶级部门");
        }
        return deptVo;
    }

}
