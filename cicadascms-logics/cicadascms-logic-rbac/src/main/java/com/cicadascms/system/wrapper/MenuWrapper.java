package com.cicadascms.system.wrapper;



import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.enums.MenuType;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.data.domain.MenuDO;
import com.cicadascms.system.vo.MenuVO;
import com.cicadascms.system.vo.RouteVo;
import com.cicadascms.data.mapper.SysMenuMapper;

import java.util.List;
import java.util.stream.Collectors;


public class MenuWrapper implements BaseWrapper<MenuDO, MenuVO> {

    private final static SysMenuMapper baseMapper;

    static {
        baseMapper = SpringContextUtils.getBean(SysMenuMapper.class);
    }

    public static MenuWrapper newBuilder() {
        return new MenuWrapper();
    }

    public RouteVo routeVO(MenuVO menuVo) {
        RouteVo routeVo = new RouteVo();
        routeVo.setName(menuVo.getMenuName());
        routeVo.setHidden(false);
        routeVo.setAlwaysShow(false);
        routeVo.setPath(menuVo.getMenuPath());
        routeVo.setComponent(menuVo.getComponent());
        RouteVo.Meta meta = new RouteVo.Meta();
        meta.setIcon(menuVo.getMenuIcon());
        meta.setTitle(menuVo.getMenuName());
        meta.setNoCache(true);
        if (menuVo.isHasChildren() && Fn.isNotEmpty(menuVo.getChildren())) {
            menuVo.getChildren().stream().forEach(sub -> {
                if (!Fn.equal(MenuType.按钮.getCode(), sub.getMenuType())) {
                    routeVo.getChildren().add(routeVO(sub));
                }
            });
        }
        routeVo.setMeta(meta);
        return routeVo;
    }

    public List<RouteVo> routeList(List<MenuVO> treeList) {
        return treeList.stream().map(this::routeVO).collect(Collectors.toList());
    }

    @Override
    public MenuVO entityVO(MenuDO entity) {
        MenuVO menuVo = WarpsUtils.copyTo(entity, MenuVO.class);
        if (Fn.isNotNull(menuVo.getParentId()) && Fn.notEqual(Constant.PARENT_ID, menuVo.getParentId())) {
            MenuDO parentMenu = baseMapper.selectById(menuVo.getParentId());
            menuVo.setParentName(parentMenu.getMenuName());
        } else {
            menuVo.setParentName("顶级类目");
        }
        return menuVo;
    }

}
