package com.cicadascms.system.service.impl;

import com.cicadascms.common.base.LoginUser;
import com.cicadascms.security.LoginUserDetails;
import com.cicadascms.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author westboy
 * @date 2019/4/21 19:54
 * @description: TODO
 */
@Primary
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginUser loginUser = userService.login(username);
        return (LoginUserDetails) loginUser;
    }


}
