package com.cicadascms.system.dto;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.UserDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * UserQueryDTO对象
 * 基础账户表
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserQueryDTO对象")
public class UserQueryDTO extends BaseDTO<UserQueryDTO, UserDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "降序排序字段 多个字段用英文逗号隔开")
    private String descs;

    @ApiModelProperty(value = "升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;
    /**
     * 登陆名
     */
    @ApiModelProperty(value = "登陆名")
    private String username;

    /**
     * 用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）
     */
    @ApiModelProperty(value = "用户类型（1.系统管理员；2,入驻药店；3.平台管理员；4.平台会员，5.入驻医生）")
    private Integer userType;

    /**
     * 状态 1:enable, 0:disable, -1:deleted
     */
    @ApiModelProperty(value = "状态 1:enable, 0:disable, -1:deleted")
    private Integer status;

    @ApiModelProperty(value = "部门编号")
    private Integer deptId;


    public Page<UserDO> page() {
        Page<UserDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<UserQueryDTO, UserDO> converter = new Converter<UserQueryDTO, UserDO>() {
        @Override
        public UserDO doForward(UserQueryDTO userQueryDTO) {
            return WarpsUtils.copyTo(userQueryDTO, UserDO.class);
        }

        @Override
        public UserQueryDTO doBackward(UserDO user) {
            return WarpsUtils.copyTo(user, UserQueryDTO.class);
        }
    };

    @Override
    public UserDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public UserQueryDTO convertFor(UserDO user) {
        return converter.doBackward(user);
    }
}
