package com.cicadascms.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.dto.RoleInputDTO;
import com.cicadascms.system.dto.RoleQueryDTO;
import com.cicadascms.system.dto.RoleUpdateDTO;
import com.cicadascms.system.service.IRoleService;
import com.cicadascms.system.vo.RoleVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Api(tags = "S-角色管理接口")
@RestController
@RequestMapping("/system/role")
public class RoleController {
    @Autowired
    private IRoleService roleService;

    @GetMapping("/tree")
    public R<RoleVO> getTree() {
        return R.ok(roleService.getTree());
    }

    @ApiOperation(value = "角色列表接口")
    @GetMapping("/list")
    public R<Page<RoleVO>> getList(RoleQueryDTO roleQueryDTO) {
        List<RoleVO> roleList = roleService.findList(roleQueryDTO);
        return R.ok(roleList);
    }

    @ApiOperation(value = "角色列表接口")
    @GetMapping("/page")
    public R<Page<RoleVO>> getPage(RoleQueryDTO roleQueryDTO) {
        return roleService.page(roleQueryDTO);
    }

    @ApiOperation(value = "角色保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid RoleInputDTO roleInputDTO) {
        return roleService.save(roleInputDTO);
    }

    @ApiOperation(value = "角色更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid RoleUpdateDTO roleUpdateDTO) {
        return roleService.update(roleUpdateDTO);
    }

    @ApiOperation(value = "角色详情接口")
    @GetMapping("/{id}")
    public R<RoleVO> getById(@PathVariable Long id) {
        return roleService.findById(id);
    }

    @ApiOperation(value = "角色删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return roleService.deleteById(id);
    }


}
