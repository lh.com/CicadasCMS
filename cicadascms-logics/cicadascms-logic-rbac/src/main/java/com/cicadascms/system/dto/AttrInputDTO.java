package com.cicadascms.system.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.data.domain.AttrDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="InputAttrDTO对象")
public class AttrInputDTO extends BaseDTO<AttrInputDTO, AttrDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 附件分类
    */
    @ApiModelProperty(value = "1-附件分类" )
    private Integer attrClassId;
    /**
    * 附件名称
    */
    @ApiModelProperty(value = "2-附件名称" )
    private String attrName;
    /**
    * 附件存储类型
    */
    @ApiModelProperty(value = "3-附件存储类型" )
    private Integer attrStoreType;
    /**
    * 附件地址
    */
    @ApiModelProperty(value = "4-附件地址" )
    private String attrLocation;
    /**
    * 创建人
    */
    @ApiModelProperty(value = "5-创建人" )
    private Integer createBy;
    /**
    * 创建时间
    */
    @ApiModelProperty(value = "6-创建时间" )
    private LocalDateTime createTime;

    public static Converter<AttrInputDTO, AttrDO> converter = new Converter<AttrInputDTO, AttrDO>() {
        @Override
        public AttrDO doForward(AttrInputDTO attrInputDTO) {
            return WarpsUtils.copyTo(attrInputDTO, AttrDO.class);
        }

        @Override
        public AttrInputDTO doBackward(AttrDO attrDO) {
            return WarpsUtils.copyTo(attrDO, AttrInputDTO.class);
        }
    };

    @Override
    public AttrDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public AttrInputDTO convertFor(AttrDO attrDO) {
        return converter.doBackward(attrDO);
    }
}
