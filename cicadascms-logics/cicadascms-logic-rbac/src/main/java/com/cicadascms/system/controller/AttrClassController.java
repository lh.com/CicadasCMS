package com.cicadascms.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.dto.AttrClassInputDTO;
import com.cicadascms.system.dto.AttrClassQueryDTO;
import com.cicadascms.system.dto.AttrClassUpdateDTO;
import com.cicadascms.system.service.IAttrClassService;
import com.cicadascms.system.vo.AttrClassVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 附件分类 控制器
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Api(tags = "附件分类接口")
@RestController
@RequestMapping("/system/attrClass")
@AllArgsConstructor
public class AttrClassController {
    private final IAttrClassService attrClassService;

    @ApiOperation(value = "附件分类分页接口")
    @GetMapping("/list")
    public R<Page<AttrClassVO>> page(AttrClassQueryDTO attrClassQueryDTO) {
        return attrClassService.page(attrClassQueryDTO);
    }

    @ApiOperation(value = "附件分类保存接口")
    @PostMapping
    public R<Boolean> save(@Valid AttrClassInputDTO attrClassInputDTO) {
        return attrClassService.save(attrClassInputDTO);
    }

    @ApiOperation(value = "附件分类更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid AttrClassUpdateDTO attrClassUpdateDTO) {
        return attrClassService.update(attrClassUpdateDTO);
    }

    @ApiOperation(value = "附件分类详情接口")
    @GetMapping("/{id}")
    public R<AttrClassVO> getById(@PathVariable Long id) {
        return attrClassService.findById(id);
    }

    @ApiOperation(value = "附件分类删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return attrClassService.deleteById(id);
    }


}