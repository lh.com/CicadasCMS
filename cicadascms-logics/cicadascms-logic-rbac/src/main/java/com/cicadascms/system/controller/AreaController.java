package com.cicadascms.system.controller;


import com.cicadascms.system.dto.AreaInputDTO;
import com.cicadascms.system.dto.AreaQueryDTO;
import com.cicadascms.system.dto.AreaUpdateDTO;
import com.cicadascms.system.service.IAreaService;
import com.cicadascms.system.vo.AreaVO;
import com.cicadascms.common.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * <p>
 * 区域 控制器
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Api(tags = "S-区域管理接口")
@RestController
@RequestMapping("/system/area")
@AllArgsConstructor
public class AreaController {
    private final IAreaService areaService;

    @ApiOperation(value = "区域列表接口")
    @GetMapping("/list")
    public R<List<AreaVO>> list(AreaQueryDTO areaQueryDTO) {
        return areaService.list(areaQueryDTO);
    }


    @ApiOperation(value = "区域列表树")
    @GetMapping("/tree")
    public R<List<AreaVO>> tree() {
        return R.ok(areaService.getTree());
    }

    @ApiOperation(value = "区域保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid AreaInputDTO areaInputDTO) {
        return areaService.save(areaInputDTO);
    }

    @ApiOperation(value = "区域更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid AreaUpdateDTO areaUpdateDTO) {
        return areaService.update(areaUpdateDTO);
    }

    @ApiOperation(value = "区域详情接口")
    @GetMapping("/{id}")
    public R<AreaVO> getById(@PathVariable Long id) {
        return areaService.findById(id);
    }

    @ApiOperation(value = "区域删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return areaService.deleteById(id);
    }


}