
package com.cicadascms.system.connect.wxapp;

import com.cicadascms.security.provider.ConnectAuthProvider;
import com.cicadascms.security.LoginUserDetails;
import com.cicadascms.data.domain.UserConnectionDO;
import com.cicadascms.system.service.IUserConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
public class WxMaAuthProvider extends ConnectAuthProvider<WxMaAuthToken> {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private IUserConnectionService userConnectionService;

    @Override
    protected WxMaAuthToken process(Authentication authentication) throws Exception {
        WxMaAuthToken authenticationToken = (WxMaAuthToken) authentication;
        UserConnectionDO userConnection = userConnectionService.findOrSaveByJsCode(authenticationToken.getPrincipal().toString(), authenticationToken.isLoginFailCreate());
        LoginUserDetails user = (LoginUserDetails) userDetailsService.loadUserByUsername(userConnection.getUserId());
        if (user == null) {
            throw new InternalAuthenticationServiceException("无法获取用户信息");
        }
        WxMaAuthToken wxMaAuthToken = new WxMaAuthToken(user, user.getAuthorities());
        wxMaAuthToken.setDetails(authenticationToken.getDetails());
        return wxMaAuthToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return WxMaAuthToken.class.isAssignableFrom(authentication);
    }


}
