package com.cicadascms.system.controller;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.dto.UserInputDTO;
import com.cicadascms.system.dto.UserQueryDTO;
import com.cicadascms.system.dto.UserUpdateDTO;
import com.cicadascms.system.service.IUserService;
import com.cicadascms.system.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 基础账户表 前端控制器
 * </p>
 *
 * @author westboy
 * @date 2019-05-30
 */
@Api(tags = "S-用户管理接口")
@RestController
@RequestMapping("/system/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @ApiOperation(value = "账号列表接口")
    @GetMapping("/page")
    public R<Page<UserVO>> getPage(UserQueryDTO userQueryDTO) {
        return userService.page(userQueryDTO);
    }

    @ApiOperation(value = "新增账号接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid UserInputDTO userInputDTO) {
        return userService.save(userInputDTO);
    }

    @ApiOperation(value = "更新账号接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid UserUpdateDTO userUpdateDTO) {
        return userService.update(userUpdateDTO);
    }

    @ApiOperation(value = "账号详情接口")
    @GetMapping("/{uid}")
    public R<UserVO> getById(@PathVariable("uid") String uid) {
        return userService.findById(uid);
    }

    @ApiOperation(value = "修改密码")
    @PostMapping("/changePassword")
    public R<Boolean> changePassword(@RequestParam("oldPassword") String oldPassword,
                                    @RequestParam("newPassword") String newPassword
                                    ) {
        return userService.changePassword(oldPassword,newPassword);
    }

    @ApiOperation(value = "删除账号接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable String id) {
        return userService.deleteById(id);
    }

}
