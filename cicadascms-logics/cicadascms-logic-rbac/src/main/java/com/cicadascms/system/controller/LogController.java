package com.cicadascms.system.controller;

import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.LogDO;
import com.cicadascms.system.service.ILogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 日志表 控制器
 * </p>
 *
 * @author jin
 * @since 2020-05-05
 */
@Api(tags = "S-日志管理接口")
@RestController
@RequestMapping("/system/log")
@AllArgsConstructor
public class LogController {
    private final ILogService logService;

//    @ApiOperation(value = "日志列表接口")
//    @GetMapping("/list")
//    public R<Page<SysLog>> getPage(LogQueryDTO logQueryDTO) {
//        return logService.page(logQueryDTO);
//    }

    @ApiOperation(value = "日志详情接口")
    @GetMapping("/{id}")
    public R<LogDO> getById(@PathVariable Long id) {
        return logService.findById(id);
    }

    @ApiOperation(value = "日志删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return logService.deleteById(id);
    }


}