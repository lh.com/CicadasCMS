package com.cicadascms.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * DeptQueryDTO对象
 * 机构部门
 * </p>
 *
 * @author jin
 * @since 2020-04-23
 */
@Data
@Accessors(chain = true)
@ToString
@ApiModel(value="DeptQueryDTO对象")
public class DeptQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
    * 部门名称
    */
    @ApiModelProperty(value = "3-部门名称" )
    private String deptName;
    /**
    * 部门编码
    */
    @ApiModelProperty(value = "4-部门编码" )
    private String deptCode;
    /**
    * 父id
    */
    @ApiModelProperty(value = "5-父id" )
    private Integer parentId;

}
