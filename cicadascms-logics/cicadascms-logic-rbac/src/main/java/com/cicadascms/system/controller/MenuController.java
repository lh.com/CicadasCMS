package com.cicadascms.system.controller;

import com.cicadascms.common.resp.R;
import com.cicadascms.system.dto.MenuInputDTO;
import com.cicadascms.system.dto.MenuQueryDTO;
import com.cicadascms.system.dto.MenuUpdateDTO;
import com.cicadascms.system.service.IMenuService;
import com.cicadascms.system.vo.MenuVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Api(tags = "S-菜单管理接口")
@RestController
@RequestMapping("/system/menu")
public class MenuController {
    @Autowired
    private IMenuService menuService;

    @ApiOperation(value = "菜单树列表接口")
    @GetMapping("/tree")
    public R<MenuVO> getTree() {
        return R.ok(menuService.getTree());
    }

    @ApiOperation(value = "菜单分页列表接口")
    @GetMapping("/list")
    public R<List<MenuVO>> getList(MenuQueryDTO menuQueryDTO) {
        return R.ok(menuService.findList(menuQueryDTO));
    }

    @GetMapping("/routes")
    public R getRoutes() {
        return R.ok(menuService.findRoutes());
    }

    @ApiOperation(value = "菜单保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid MenuInputDTO menuInputDTO) {
        return menuService.save(menuInputDTO);
    }

    @ApiOperation(value = "菜单更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid MenuUpdateDTO menuUpdateDTO) {
        return menuService.update(menuUpdateDTO);
    }

    @ApiOperation(value = "菜单详情接口")
    @GetMapping("/{id}")
    public R<MenuVO> getById(@PathVariable Long id) {
        return menuService.findById(id);
    }

    @ApiOperation(value = "菜单删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return menuService.deleteById(id);
    }

}
