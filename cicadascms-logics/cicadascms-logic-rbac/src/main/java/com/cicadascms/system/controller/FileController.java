//package com.cicadascms.system.controller;
//
//import com.cicadascms.framework.common.base.resp.R;
//import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
//import com.github.tobato.fastdfs.domain.fdfs.MetaData;
//import com.github.tobato.fastdfs.domain.fdfs.StorePath;
//import com.github.tobato.fastdfs.domain.fdfs.ThumbImageConfig;
//import com.github.tobato.fastdfs.domain.proto.storage.DownloadByteArray;
//import com.github.tobato.fastdfs.service.FastFileStorageClient;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.apache.commons.io.FilenameUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.ServletOutputStream;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.net.URLEncoder;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Map;
//import java.util.Set;
//
//@RestController
//@RequestMapping("/file")
//@Api(tags = "S-文件管理接口")
//public class FileController {
//
//    @Autowired
//    private FastFileStorageClient fastFileStorageClient;
//
//    @Autowired
//    private FdfsWebServer fdfsWebServer;
//
//    @Autowired
//    private ThumbImageConfig thumbImageConfig;
//    /**
//     * 文件上传
//     *
//     * @param file
//     * @return
//     * @throws IOException
//     */
//    @ApiOperation("文件上传")
//    @PostMapping("/upload")
//    public String upload(@RequestParam MultipartFile file) throws IOException {
//
//        // 设置文件信息
//        Set<MetaData> metaData = new HashSet<>();
//        metaData.add(new MetaData("author", "wander"));
//        metaData.add(new MetaData("description", "上传的啥子文件哟"));
//        // 上传   （文件上传可不填文件信息，填入null即可）
//        StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), metaData);
//        return getResAccessUrlFile(storePath);
//    }
//
//    @ApiOperation("图片上传")
//    @PostMapping("/img/upload")
//    public Map<String,Object> uploadimg(@RequestParam MultipartFile file) throws IOException {
//
//        // 设置文件信息
//        Set<MetaData> metaData = new HashSet<>();
//        metaData.add(new MetaData("author", "wander"));
//        metaData.add(new MetaData("description", "上传的啥子文件哟"));
//        StorePath storePath = fastFileStorageClient.uploadImageAndCrtThumbImage(
//        		file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), metaData);
//        return getResAccessUrl(storePath);
//    }
//
//    // 封装图片完整URL地址
//    private String getResAccessUrlFile(StorePath storePath) {
//        String fileUrl = fdfsWebServer.getWebServerUrl() + storePath.getFullPath();
//        return fileUrl;
//    }
//    // 封装图片完整URL地址返回json
//    private Map<String,Object> getResAccessUrl(StorePath storePath) {
//    	String imageUrl = fdfsWebServer.getWebServerUrl() + storePath.getFullPath();
//    	String thumbImageUrl = fdfsWebServer.getWebServerUrl() + thumbImageConfig.getThumbImagePath(storePath.getFullPath());
//    	Map<String, Object> map = new HashMap<String, Object>();
//    	map.put("imageUrl",imageUrl);
//    	map.put("thumbImageUrl",thumbImageUrl);
//    	return map;
//    }
//
//    /**
//     * 文件删除
//     *
//     * @param path
//     * @return
//     */
//    @ApiOperation("文件删除")
//    @DeleteMapping("/delete")
//    public R<String> delete(@RequestParam String path) {
//
//        // 第一种删除：参数：完整地址
//        fastFileStorageClient.deleteFile(path);
//
//        // 第二种删除：参数：组名加文件路径
//        // fastFileStorageClient.deleteFile(group,path);
//
//        return R.ok("恭喜恭喜，删除成功！");
//    }
//
//
//    /**
//     * 文件下载
//     *
//     * @param path
//     * @return
//     */
//    @ApiOperation("文件下载")
//    @GetMapping("/download")
//    public void downLoad(@RequestParam String group, @RequestParam String path, @RequestParam String fileName, HttpServletResponse response) throws IOException {
//
//        // 获取文件
//        byte[] bytes = fastFileStorageClient.downloadFile(group, path, new DownloadByteArray());
//
//        //设置相应类型application/octet-stream        （注：applicatoin/octet-stream 为通用，一些其它的类型苹果浏览器下载内容可能为空）
//        response.reset();
//        response.setContentType("applicatoin/octet-stream");
//        //设置头信息                 Content-Disposition为属性名  附件形式打开下载文件   指定名称  为 设定的fileName
//        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
//        // 写入到流
//        ServletOutputStream out = response.getOutputStream();
//        out.write(bytes);
//        out.close();
//    }
//}