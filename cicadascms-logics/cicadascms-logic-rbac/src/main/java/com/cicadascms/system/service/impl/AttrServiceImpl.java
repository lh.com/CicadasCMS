package com.cicadascms.system.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import com.cicadascms.system.dto.AttrInputDTO;
import com.cicadascms.system.dto.AttrQueryDTO;
import com.cicadascms.system.dto.AttrUpdateDTO;
import com.cicadascms.data.domain.AttrDO;
import com.cicadascms.data.mapper.AttrMapper;
import com.cicadascms.system.service.IAttrService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.io.Serializable;

/**
 * <p>
 * 附件 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2021-01-02
 */
@Service("attrService")
public class AttrServiceImpl extends BaseService<AttrMapper, AttrDO> implements IAttrService {

    @Override
    public R page(AttrQueryDTO attrQueryDTO) {
        AttrDO attrDO = attrQueryDTO.convertToEntity();
        Page page = baseMapper.selectPage(attrQueryDTO.page(), getLambdaQueryWrapper().setEntity(attrDO));
        return R.ok(page);
    }

    @Override
    public R save(AttrInputDTO attrInputDTO) {
        AttrDO attrDO = attrInputDTO.convertToEntity();
        baseMapper.insert(attrDO);
        return R.ok(true);
    }

    @Override
    public R update(AttrUpdateDTO attrUpdateDTO) {
        AttrDO attrDO = attrUpdateDTO.convertToEntity();
        baseMapper.updateById(attrDO);
        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        AttrDO attrDO = baseMapper.selectById(id);
        return R.ok(attrDO);

    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

}
