package com.cicadascms.system.wrapper;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.data.domain.RoleDO;
import com.cicadascms.system.service.IRoleService;
import com.cicadascms.system.vo.RoleVO;


public class RoleWrapper implements BaseWrapper<RoleDO, RoleVO> {

    private final static IRoleService roleService;

    static {
        roleService = SpringContextUtils.getBean(IRoleService.class);
    }

    public static RoleWrapper newBuilder() {
        return new RoleWrapper();
    }

    @Override
    public RoleVO entityVO(RoleDO entity) {
        RoleVO roleVo = WarpsUtils.copyTo(entity, RoleVO.class);
        assert roleVo != null;
        if (Fn.isNotNull(roleVo.getParentId()) && Fn.notEqual(Constant.PARENT_ID, roleVo.getParentId())) {
            RoleDO parentRole = roleService.getById(entity.getParentId());
            roleVo.setParentName(parentRole.getRoleName());
        } else {
            roleVo.setParentName("顶级类目");
        }

        return roleVo;
    }
}
