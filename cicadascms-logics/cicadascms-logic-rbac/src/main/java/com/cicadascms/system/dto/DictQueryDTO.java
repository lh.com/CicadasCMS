package com.cicadascms.system.dto;

import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.data.domain.DictDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * DictQueryDTO对象
 * 字典表
 * </p>
 *
 * @author jin
 * @since 2020-04-07
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="DictQueryDTO对象")
public class DictQueryDTO extends BaseDTO<DictQueryDTO, DictDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-降序排序字段 多个字段用英文逗号隔开")
    private String descs;

    @ApiModelProperty(value = "4-升序排序字段  多个字段用英文逗号隔开")
    private String ascs;

    /**
    * 字典标识
    */
    @ApiModelProperty(value = "3-字典标识" )
    private String dictCode;
    /**
    * 字典名称
    */
    @ApiModelProperty(value = "4-字典名称" )
    private String dictName;

    /**
    * 上级ID
    */
    @ApiModelProperty(value = "6-上级ID" )
    private Integer parentId;



    public Page<DictDO> page() {
        Page<DictDO> page = getPage(current,size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<DictQueryDTO, DictDO> converter = new Converter<DictQueryDTO, DictDO>() {
        @Override
        public DictDO doForward(DictQueryDTO dictQueryDTO) {
            return WarpsUtils.copyTo(dictQueryDTO, DictDO.class);
        }

        @Override
        public DictQueryDTO doBackward(DictDO dict) {
            return WarpsUtils.copyTo(dict, DictQueryDTO.class);
        }
    };

    @Override
    public DictDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public DictQueryDTO convertFor(DictDO dict) {
        return converter.doBackward(dict);
    }
}
