package com.cicadascms.system.vo;

import com.cicadascms.common.tree.TreeNode;
import com.cicadascms.data.domain.RoleDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "RoleVo", description = "角色")
@Data
@EqualsAndHashCode(callSuper = true)
public class RoleVO extends RoleDO implements TreeNode<RoleVO> {


    private String parentName;

    private List<RoleVO> children;

    @ApiModelProperty(value = "选中的")
    private Integer[] selectedPermissionIds;

    @Override
    public Serializable getCurrentNodeId() {
        return getRoleId();
    }

    @Override
    public Serializable getParentNodeId() {
        return getParentId();
    }

}
