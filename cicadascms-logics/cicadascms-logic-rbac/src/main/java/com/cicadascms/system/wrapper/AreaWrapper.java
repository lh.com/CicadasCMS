package com.cicadascms.system.wrapper;


import com.cicadascms.common.constant.Constant;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.data.domain.AreaDO;
import com.cicadascms.system.service.IAreaService;
import com.cicadascms.system.vo.AreaVO;

public class AreaWrapper implements BaseWrapper<AreaDO, AreaVO> {

    private final static IAreaService areaService;

    static {
        areaService = SpringContextUtils.getBean(IAreaService.class);
    }

    public static AreaWrapper newBuilder() {
        return new AreaWrapper();
    }

    @Override
    public AreaVO entityVO(AreaDO entity) {
        AreaVO entityVo = WarpsUtils.copyTo(entity, AreaVO.class);
        if (Fn.isNotNull(entityVo.getParentId()) && Fn.notEqual(Constant.PARENT_ID, entityVo.getParentId())) {
            AreaDO parentArea = areaService.getById(entityVo.getParentId());
            if (Fn.isNotNull(parentArea)) {
                entityVo.setParentName(parentArea.getName());
            }
        } else {
            entityVo.setParentName("中国");
        }
        entityVo.setLeaf(Fn.equal(entityVo.getLevelType(), 3));
        entityVo.setMergerName(entityVo.getMergerName().replace(",", "/"));
        return entityVo;
    }



}