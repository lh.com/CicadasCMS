package com.cicadascms.system.service.impl;

import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import com.cicadascms.mybatis.annotation.DataScope;
import com.cicadascms.redis.annotation.RedisCache;
import com.cicadascms.system.dto.AreaInputDTO;
import com.cicadascms.system.dto.AreaQueryDTO;
import com.cicadascms.system.dto.AreaUpdateDTO;
import com.cicadascms.data.domain.AreaDO;
import com.cicadascms.system.service.IAreaService;
import com.cicadascms.system.vo.AreaVO;
import com.cicadascms.data.mapper.SysAreaMapper;
import com.cicadascms.system.wrapper.AreaWrapper;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 区域 服务实现类
 * </p>
 *
 * @author jin
 * @since 2020-09-06
 */
@Service
public class AreaServiceImpl extends BaseService<SysAreaMapper, AreaDO> implements IAreaService {

    @RedisCache(key = "area_cache", fieldKey = "tree")
    @Override
    public List<AreaVO> getTree() {
        AreaWrapper areaWrapper = AreaWrapper.newBuilder();
        return areaWrapper.treeVO(areaWrapper.listVO(baseMapper
                .selectList(getLambdaQueryWrapper().orderByAsc(AreaDO::getId))));
    }

    @DataScope
    @Override
    public R list(AreaQueryDTO areaQueryDTO) {
        List<AreaDO> areaList = baseMapper.selectList(getLambdaQueryWrapper(areaQueryDTO.convertToEntity()));
        return R.ok(AreaWrapper.newBuilder().listVO(areaList));
    }

    @Override
    public R save(AreaInputDTO areaInputDTO) {
        AreaDO area = areaInputDTO.convertToEntity();
        baseMapper.insert(area);
        return R.ok(true);
    }

    @Override
    public R update(AreaUpdateDTO areaUpdateDTO) {
        AreaDO area = areaUpdateDTO.convertToEntity();
        baseMapper.updateById(area);
        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        AreaDO area = baseMapper.selectById(id);
        return R.ok(AreaWrapper.newBuilder().entityVO(area));
    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

}
