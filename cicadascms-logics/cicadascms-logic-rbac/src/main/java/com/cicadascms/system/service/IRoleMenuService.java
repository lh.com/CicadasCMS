package com.cicadascms.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.data.domain.RoleMenuDO;

import java.util.List;

/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 *
 * @author westboy
 * @date 2019-07-21
 */
public interface IRoleMenuService extends IService<RoleMenuDO> {

    void updateRoleMenu(Integer id, List<String> permissionIds);

}
