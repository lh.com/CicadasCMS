package com.cicadascms.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.admin.dto.ChannelInputDTO;
import com.cicadascms.admin.dto.ChannelQueryDTO;
import com.cicadascms.admin.dto.ChannelUpdateDTO;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.admin.vo.ChannelVO;
import com.cicadascms.common.resp.R;
import com.cicadascms.data.domain.MenuDO;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 内容分类表 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IAdminChannelService extends IService<ChannelDO> {

    /**
     * 分页方法
     *
     * @param channelQueryDTO
     * @return
     */
    R page(ChannelQueryDTO channelQueryDTO);

    /**
     * 保存方法
     *
     * @param channelInputDTO
     * @return
     */
    R save(ChannelInputDTO channelInputDTO);

    /**
     * 更新方法
     *
     * @param channelUpdateDTO
     * @return
     */
    R update(ChannelUpdateDTO channelUpdateDTO);

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R deleteById(Serializable id);

    /**
     * @param siteId
     * @param channelUrlPath
     * @return
     */
    ChannelVO findBySiteIdAndChannelUrlPath(Integer siteId, String channelUrlPath);

    /**
     * 根据域名查询
     *
     * @param domain
     * @return
     */
    ChannelDO findByDomain(String domain);

    /**
     * 获取分类树
     *
     * @return
     */
    List<ChannelVO> getTree();

    /**
     * 获取父栏目
     *
     * @param parentId
     * @return
     */
    List<ChannelDO> findByParentId(Integer parentId);

    /**
     * 查询扩展表字段内容
     *
     * @param tableName
     * @param contentId
     * @return
     */
    Map<String, Object> findByTableNameAndChannelId(String fields, String tableName, Integer contentId);


}
