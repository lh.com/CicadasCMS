package com.cicadascms.admin.wrapper;


import com.cicadascms.data.domain.NavDO;
import com.cicadascms.admin.service.IAdminNavService;
import com.cicadascms.admin.vo.NavVO;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.base.BaseWrapper;
import org.springframework.util.Assert;
import com.cicadascms.common.utils.WarpsUtils;

public class AdminNavWrapper implements BaseWrapper<NavDO, NavVO> {

    private final static IAdminNavService navService;

    static {
        navService = SpringContextUtils.getBean(IAdminNavService.class);
    }

    public static AdminNavWrapper newBuilder() {
        return new AdminNavWrapper();
    }

    @Override
    public NavVO entityVO(NavDO entity) {
        Assert.isTrue(Fn.isNotNull(entity), NavDO.class.getName() + " 对象不存在！");
        return WarpsUtils.copyTo(entity, NavVO.class);
    }
}