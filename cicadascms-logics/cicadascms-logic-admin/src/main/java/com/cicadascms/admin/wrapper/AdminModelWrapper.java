package com.cicadascms.admin.wrapper;

import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.admin.service.IAdminModelService;
import com.cicadascms.admin.vo.ModelVO;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.base.BaseWrapper;
import org.springframework.util.Assert;
import com.cicadascms.common.utils.WarpsUtils;

public class AdminModelWrapper implements BaseWrapper<ModelDO, ModelVO> {

    private final static IAdminModelService modelService;

    static {
        modelService = SpringContextUtils.getBean(IAdminModelService.class);
    }

    public static AdminModelWrapper newBuilder() {
        return new AdminModelWrapper();
    }

    @Override
    public ModelVO entityVO(ModelDO entity) {
        Assert.isTrue(Fn.isNotNull(entity), ModelDO.class.getName() + " 对象不存在！");
        return WarpsUtils.copyTo(entity, ModelVO.class);
    }
}