package com.cicadascms.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.admin.dto.NavInputDTO;
import com.cicadascms.admin.dto.NavQueryDTO;
import com.cicadascms.admin.dto.NavUpdateDTO;
import com.cicadascms.data.domain.NavDO;
import com.cicadascms.common.resp.R;

import java.io.Serializable;

/**
 * <p>
 * 导航菜单 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IAdminNavService extends IService<NavDO> {

    /**
     * 分页方法
     * @param navQueryDTO
     * @return
     */
    R page(NavQueryDTO navQueryDTO);

    /**
     * 保存方法
     * @param navInputDTO
     * @return
     */
    R save(NavInputDTO navInputDTO);

    /**
     * 更新方法
     * @param navUpdateDTO
     * @return
     */
    R update(NavUpdateDTO navUpdateDTO);

    /**
     * 查询方法
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 删除方法
     * @param id
     * @return
     */
    R deleteById(Serializable id);
}
