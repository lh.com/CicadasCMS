package com.cicadascms.admin.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.admin.dto.NavInputDTO;
import com.cicadascms.admin.dto.NavQueryDTO;
import com.cicadascms.admin.dto.NavUpdateDTO;
import com.cicadascms.data.domain.NavDO;
import com.cicadascms.data.mapper.NavMapper;
import com.cicadascms.admin.service.IAdminNavService;
import com.cicadascms.common.base.BaseService;
import com.cicadascms.common.resp.R;
import org.springframework.stereotype.Service;
import java.io.Serializable;

/**
 * <p>
 * 导航菜单 服务实现类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Service
public class AdminNavServiceImpl extends BaseService<NavMapper, NavDO> implements IAdminNavService {

    @Override
    public R page(NavQueryDTO navQueryDTO) {
        NavDO navDO = navQueryDTO.convertToEntity();
        Page page = baseMapper.selectPage(navQueryDTO.page(), getLambdaQueryWrapper().setEntity(navDO));
        return R.ok(page);
    }

    @Override
    public R save(NavInputDTO navInputDTO) {
        NavDO navDO = navInputDTO.convertToEntity();
        baseMapper.insert(navDO);
        return R.ok(true);
    }

    @Override
    public R update(NavUpdateDTO navUpdateDTO) {
        NavDO navDO = navUpdateDTO.convertToEntity();
        baseMapper.updateById(navDO);
        return R.ok(true);
    }

    @Override
    public R findById(Serializable id) {
        NavDO navDO = baseMapper.selectById(id);
        return R.ok(navDO);

    }

    @Override
    public R deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return R.ok(true);
    }

}
