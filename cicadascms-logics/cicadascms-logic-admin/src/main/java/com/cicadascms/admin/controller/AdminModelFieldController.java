package com.cicadascms.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.admin.dto.ModelFieldInputDTO;
import com.cicadascms.admin.dto.ModelFieldQueryDTO;
import com.cicadascms.admin.dto.ModelFieldUpdateDTO;
import com.cicadascms.admin.service.IAdminModelFieldService;
import com.cicadascms.admin.vo.ModelFieldVO;
import com.cicadascms.common.resp.R;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;


/**
 * <p>
 * 模型字段 控制器
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Api(tags = "A-内容模型字段管理接口")
@RestController
@RequestMapping("/admin/cms/modelField")
@AllArgsConstructor
public class AdminModelFieldController {
    private final IAdminModelFieldService modelFieldService;

    @ApiOperation(value = "模型字段分页接口")
    @GetMapping("/list")
    public R<Page<ModelFieldVO>> page(ModelFieldQueryDTO modelFieldQueryDTO) {
        return modelFieldService.page(modelFieldQueryDTO);
    }

    @ApiOperation(value = "模型字段保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid ModelFieldInputDTO modelFieldInputDTO) {
        return modelFieldService.save(modelFieldInputDTO);
    }

    @ApiOperation(value = "模型字段更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid ModelFieldUpdateDTO modelFieldUpdateDTO) {
        return modelFieldService.update(modelFieldUpdateDTO);
    }

    @ApiOperation(value = "模型字段详情接口")
    @GetMapping("/{id}")
    public R<ModelFieldVO> getById(@PathVariable Integer id) {
        return modelFieldService.findById(id);
    }

    @ApiOperation(value = "模型字段删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Integer id) {
        return modelFieldService.deleteById(id);
    }

    @ApiOperation(value = "获取模型字段类型")
    @GetMapping("/type")
    public R<Map<String, Object>> getType() {
        return modelFieldService.getModelFiledTypeList();
    }

    @ApiOperation(value = "获取模型字段规则")
    @GetMapping("/type/{modelType}/prop")
    public R<ModelFieldProp> getRule(@PathVariable Integer modelType) {
        return modelFieldService.getModelFieldProp(modelType);
    }

}