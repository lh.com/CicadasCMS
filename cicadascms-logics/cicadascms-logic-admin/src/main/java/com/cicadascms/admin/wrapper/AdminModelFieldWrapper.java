package com.cicadascms.admin.wrapper;

import com.cicadascms.common.func.Fn;
import com.cicadascms.data.domain.ModelFieldDO;
import com.cicadascms.admin.service.IAdminModelFieldService;
import com.cicadascms.admin.vo.ModelFieldVO;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.base.BaseWrapper;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.support.database.modelfield.ModelFieldProp;
import lombok.SneakyThrows;


public class AdminModelFieldWrapper implements BaseWrapper<ModelFieldDO, ModelFieldVO> {

    private final static IAdminModelFieldService modelFieldService;

    static {
        modelFieldService = SpringContextUtils.getBean(IAdminModelFieldService.class);
    }

    public static AdminModelFieldWrapper newBuilder() {
        return new AdminModelFieldWrapper();
    }

    @SneakyThrows
    @Override
    public ModelFieldVO entityVO(ModelFieldDO entity) {
        ModelFieldVO modelFieldVO = WarpsUtils.copyTo(entity, ModelFieldVO.class);
        ModelFieldProp modelFieldProp = Fn.readValue(entity.getFieldConfig(), ModelFieldProp.class);
        modelFieldVO.setFieldProp(modelFieldProp);
        return modelFieldVO;
    }

}