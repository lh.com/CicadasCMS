package com.cicadascms.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * ContentVO对象
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ContentVO对象", description = "内容")
public class ContentVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内容编号
     */
    @ApiModelProperty(value = "1-内容编号")
    private Integer contentId;
    /**
     * 站点编号
     */
    @ApiModelProperty(value = "2-站点编号")
    private Integer siteId;
    /**
     * 栏目编号
     */
    @ApiModelProperty(value = "3-栏目编号")
    private Integer channelId;
    /**
     * 模型编号
     */
    @ApiModelProperty(value = "4-模型编号")
    private Integer modelId;
    /**
     * 标题
     */
    @ApiModelProperty(value = "5-标题")
    private String title;
    /**
     * 副标题
     */
    @ApiModelProperty(value = "6-副标题")
    private String subTitle;
    /**
     * 作者
     */
    @ApiModelProperty(value = "7-作者")
    private String author;
    /**
     * 页面关键字
     */
    @ApiModelProperty(value = "8-页面关键字")
    private Integer keywords;
    /**
     * 页面描述
     */
    @ApiModelProperty(value = "9-页面描述")
    private Integer description;
    /**
     * 录入时间
     */
    @ApiModelProperty(value = "10-录入时间")
    private LocalDateTime inputTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "11-更新时间")
    private Integer updateTime;
    /**
     * 内容状态
     */
    @ApiModelProperty(value = "12-内容状态")
    private Integer state;
    /**
     * 来源
     */
    @ApiModelProperty(value = "13-来源")
    private String source;
    /**
     * 原文地址
     */
    @ApiModelProperty(value = "14-原文地址")
    private String sourceUrl;
    /**
     * 封面图片
     */
    @ApiModelProperty(value = "15-封面图片")
    private String thumb;
    /**
     * 浏览数量
     */
    @ApiModelProperty(value = "16-浏览数量")
    private Integer viewNum;
    /**
     * 价格
     */
    @ApiModelProperty(value = "17-价格")
    private Integer price;
    /**
     * 付费阅读
     */
    @ApiModelProperty(value = "18-付费阅读")
    private Boolean paidReading;
    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "19-扩展字段")
    private List<ModelFieldValueVO> ext;

}
