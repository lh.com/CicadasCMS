package com.cicadascms.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.admin.dto.NavInputDTO;
import com.cicadascms.admin.dto.NavQueryDTO;
import com.cicadascms.admin.dto.NavUpdateDTO;
import com.cicadascms.admin.service.IAdminNavService;
import com.cicadascms.admin.vo.NavVO;
import com.cicadascms.common.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


/**
 * <p>
 * 导航菜单 控制器
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Api(tags = "A-导航菜单接口")
@RestController
@RequestMapping("/admin/cms/nav")
@AllArgsConstructor
public class AdminNavController {
    private final IAdminNavService navService;

    @ApiOperation(value = "导航菜单分页接口")
    @GetMapping("/list")
    public R<Page<NavVO>> page(NavQueryDTO navQueryDTO) {
        return navService.page(navQueryDTO);
    }

    @ApiOperation(value = "导航菜单保存接口")
    @PostMapping
    public R<Boolean> save(@RequestBody @Valid NavInputDTO navInputDTO) {
        return navService.save(navInputDTO);
    }

    @ApiOperation(value = "导航菜单更新接口")
    @PutMapping
    public R<Boolean> updateById(@RequestBody @Valid NavUpdateDTO navUpdateDTO) {
        return navService.update(navUpdateDTO);
    }

    @ApiOperation(value = "导航菜单详情接口")
    @GetMapping("/{id}")
    public R<NavVO> getById(@PathVariable Long id) {
        return navService.findById(id);
    }

    @ApiOperation(value = "导航菜单删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return navService.deleteById(id);
    }


}