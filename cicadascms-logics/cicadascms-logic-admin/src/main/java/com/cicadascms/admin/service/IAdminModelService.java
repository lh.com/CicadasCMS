package com.cicadascms.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cicadascms.admin.dto.ModelInputDTO;
import com.cicadascms.admin.dto.ModelQueryDTO;
import com.cicadascms.admin.dto.ModelUpdateDTO;
import com.cicadascms.data.domain.ModelDO;
import com.cicadascms.common.resp.R;

import java.io.Serializable;

/**
 * <p>
 * 内容模型表 服务类
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
public interface IAdminModelService extends IService<ModelDO> {

    /**
     * 分页方法
     *
     * @param modelQueryDTO
     * @return
     */
    R page(ModelQueryDTO modelQueryDTO);

    /**
     * 保存方法
     *
     * @param modelInputDTO
     * @return
     */
    R save(ModelInputDTO modelInputDTO);

    /**
     * 更新方法
     *
     * @param modelUpdateDTO
     * @return
     */
    R update(ModelUpdateDTO modelUpdateDTO);

    /**
     * 查询方法
     *
     * @param id
     * @return
     */
    R findById(Serializable id);

    /**
     * 查询方法
     *
     * @param type
     * @return
     */
    R findByType(Serializable type);



    /**
     * 删除方法
     *
     * @param id
     * @return
     */
    R deleteById(Serializable id);

}
