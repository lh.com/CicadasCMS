package com.cicadascms.admin.dto;

import com.cicadascms.data.domain.NavDO;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * <p>
 * NavUpdateDTO对象
 * 导航菜单
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="NavUpdateDTO对象")
public class NavUpdateDTO extends BaseDTO<NavUpdateDTO, NavDO>  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer navId;
    /**
    * 导航名称
    */
    @ApiModelProperty(value = "2-导航名称" )
    private String navName;
    /**
    * target
    */
    @ApiModelProperty(value = "3-target" )
    private String target;
    /**
    * 链接地址
    */
    @ApiModelProperty(value = "4-链接地址" )
    private String href;
    /**
    * 上级编号
    */
    @ApiModelProperty(value = "5-上级编号" )
    private Integer parentId;
    /**
    * 是否拥有子类
    */
    @ApiModelProperty(value = "6-是否拥有子类" )
    private Integer hasChild;
    /**
    * 导航图标
    */
    @ApiModelProperty(value = "7-导航图标" )
    private String icon;
    /**
    * 导航状态
    */
    @ApiModelProperty(value = "8-导航状态" )
    private Boolean status;

    public static Converter<NavUpdateDTO, NavDO> converter = new Converter<NavUpdateDTO, NavDO>() {
        @Override
        public NavDO doForward(NavUpdateDTO navUpdateDTO) {
            return WarpsUtils.copyTo(navUpdateDTO, NavDO.class);
        }

        @Override
        public NavUpdateDTO doBackward(NavDO navDO) {
            return WarpsUtils.copyTo(navDO, NavUpdateDTO.class);
        }
    };

    @Override
    public NavDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public NavUpdateDTO convertFor(NavDO navDO) {
        return converter.doBackward(navDO);
    }
}
