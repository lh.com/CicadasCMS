package com.cicadascms.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cicadascms.admin.dto.ChannelInputDTO;
import com.cicadascms.admin.dto.ChannelQueryDTO;
import com.cicadascms.admin.dto.ChannelUpdateDTO;
import com.cicadascms.admin.service.IAdminChannelService;
import com.cicadascms.admin.vo.ChannelVO;
import com.cicadascms.common.base.LoginUser;
import com.cicadascms.common.resp.R;
import com.cicadascms.support.annotation.CurrentUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 内容分类表 控制器
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Api(tags = "A-内容分类表接口")
@RestController
@RequestMapping("/admin/cms/channel")
@AllArgsConstructor
public class AdminChannelController {
    private final IAdminChannelService channelService;

    @ApiOperation(value = "栏目树列表接口")
    @GetMapping("/tree")
    public R<ChannelVO> getTree() {
        return R.ok(channelService.getTree());
    }

    @ApiOperation(value = "内容分类表分页接口")
    @GetMapping("/list")
    public R<Page<ChannelVO>> page(@CurrentUser LoginUser loginUser, ChannelQueryDTO channelQueryDTO) {
        return channelService.page(channelQueryDTO);
    }

    @ApiOperation(value = "内容分类表保存接口")
    @PostMapping
    public R<Boolean> save(@Valid @RequestBody ChannelInputDTO channelInputDTO) {
        return channelService.save(channelInputDTO);
    }

    @ApiOperation(value = "内容分类表更新接口")
    @PutMapping
    public R<Boolean> updateById(@Valid @RequestBody ChannelUpdateDTO channelUpdateDTO) {
        return channelService.update(channelUpdateDTO);
    }

    @ApiOperation(value = "内容分类表详情接口")
    @GetMapping("/{id}")
    public R<ChannelVO> getById(@PathVariable Integer id) {
        return channelService.findById(id);
    }

    @ApiOperation(value = "内容分类表删除接口")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Integer id) {
        return channelService.deleteById(id);
    }


}