package com.cicadascms.admin.dto;
import cn.hutool.core.util.StrUtil;
import com.cicadascms.data.domain.ChannelDO;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.common.constant.Constant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.io.Serializable;

/**
 * <p>
 * ChannelQueryDTO对象
 * 内容分类表
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="ChannelQueryDTO对象")
public class ChannelQueryDTO extends BaseDTO<ChannelQueryDTO, ChannelDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;

    /**
    * 站点编号
    */
    @ApiModelProperty(value = "3-站点编号" )
    private Integer siteId;
    /**
    * 分类明细
    */
    @ApiModelProperty(value = "4-分类明细" )
    private String channelName;


    /**
     * 域名
     */
    @ApiModelProperty(value = "2-栏目域名")
    private String domain;

    /**
    * 栏目模型编号
    */
    @ApiModelProperty(value = "5-栏目模型编号" )
    private Integer channelModelId;
    /**
    * 内容模型编号
    */
    @ApiModelProperty(value = "6-内容模型编号" )
    private Integer contentModelId;
    /**
    * 栏目路径
    */
    @ApiModelProperty(value = "7-栏目路径" )
    private String channelUrlPath;
    /**
    * 父类编号
    */
    @ApiModelProperty(value = "8-父类编号" )
    private Long parentId;
    /**
    * 单页栏目（0：不是，1：是）
    */
    @ApiModelProperty(value = "9-单页栏目（0：不是，1：是）" )
    private Boolean isAlone;
    /**
    * 单页内容
    */
    @ApiModelProperty(value = "10-单页内容" )
    private String aloneContent;
    /**
    * 首页视图模板
    */
    @ApiModelProperty(value = "11-首页视图模板" )
    private String indexView;
    /**
    * 列表页视图模板
    */
    @ApiModelProperty(value = "12-列表页视图模板" )
    private String listView;
    /**
    * 内容页视图模板
    */
    @ApiModelProperty(value = "13-内容页视图模板" )
    private String contentView;
    /**
    * 导航
    */
    @ApiModelProperty(value = "14-导航" )
    private Boolean isNav;
    /**
    * 外链地址
    */
    @ApiModelProperty(value = "15-外链地址" )
    private String url;
    /**
    * 是否有子类
    */
    @ApiModelProperty(value = "16-是否有子类" )
    private Boolean hasChildren;
    /**
    * 栏目分页数量
    */
    @ApiModelProperty(value = "17-栏目分页数量" )
    private Integer pageSize;
    /**
    * 当前栏目下的是否支持全文搜索
    */
    @ApiModelProperty(value = "18-当前栏目下的是否支持全文搜索" )
    private Boolean allowSearch;
    /**
    * 栏目分类
    */
    @ApiModelProperty(value = "19-栏目分类" )
    private Integer channelType;
    /**
    * 栏目图标
    */
    @ApiModelProperty(value = "20-栏目图标" )
    private String channelIcon;
    private Integer sortId;

    public Page<ChannelDO> page() {
        Page<ChannelDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<ChannelQueryDTO, ChannelDO> converter = new Converter<ChannelQueryDTO, ChannelDO>() {
        @Override
        public ChannelDO doForward(ChannelQueryDTO channelQueryDTO) {
            return WarpsUtils.copyTo(channelQueryDTO, ChannelDO.class);
        }

        @Override
        public ChannelQueryDTO doBackward(ChannelDO channelDO) {
            return WarpsUtils.copyTo(channelDO, ChannelQueryDTO.class);
        }
    };

    @Override
    public ChannelDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public ChannelQueryDTO convertFor(ChannelDO channelDO) {
        return converter.doBackward(channelDO);
    }
}
