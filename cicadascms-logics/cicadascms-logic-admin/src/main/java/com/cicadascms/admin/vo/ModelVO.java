package com.cicadascms.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * ModelVO对象
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value="ModelVO对象", description="内容模型表")
public class ModelVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer modelId;
    /**
    * 站点id
    */
    @ApiModelProperty(value = "2-站点id" )
    private Long siteId;
    /**
    * 模型名称
    */
    @ApiModelProperty(value = "3-模型名称" )
    private String modelName;
    /**
    * 模型表名称
    */
    @ApiModelProperty(value = "4-模型表名称" )
    private String tableName;
    /**
    * 内容模型，栏目模型
    */
    @ApiModelProperty(value = "5-内容模型，栏目模型" )
    private Integer modelType;
    /**
    * 字段描述
    */
    @ApiModelProperty(value = "6-字段描述" )
    private String des;
    /**
    * 状态
    */
    @ApiModelProperty(value = "7-状态" )
    private Boolean status;
    /**
    * 创建人
    */
    @ApiModelProperty(value = "8-创建人" )
    private Integer createUser;
    /**
    * 创建时间
    */
    @ApiModelProperty(value = "9-创建时间" )
    private LocalDateTime createTime;
    /**
    * 更新用户
    */
    @ApiModelProperty(value = "10-更新用户" )
    private Integer updateUser;
    /**
    * 更新时间
    */
    @ApiModelProperty(value = "11-更新时间" )
    private LocalDateTime updateTime;
    /**
    * 删除状态
    */
    @ApiModelProperty(value = "12-删除状态" )
    private Boolean deletedFlag;

}
