package com.cicadascms.admin.dto;

import com.cicadascms.data.domain.NavDO;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.io.Serializable;

/**
 * <p>
 * NavInputDTO对象
 * 导航菜单
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="InputNavDTO对象")
public class NavInputDTO extends BaseDTO<NavInputDTO, NavDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * 导航名称
    */
    @ApiModelProperty(value = "1-导航名称" )
    private String navName;
    /**
    * target
    */
    @ApiModelProperty(value = "2-target" )
    private String target;
    /**
    * 链接地址
    */
    @ApiModelProperty(value = "3-链接地址" )
    private String href;
    /**
    * 上级编号
    */
    @ApiModelProperty(value = "4-上级编号" )
    private Integer parentId;
    /**
    * 是否拥有子类
    */
    @ApiModelProperty(value = "5-是否拥有子类" )
    private Integer hasChild;
    /**
    * 导航图标
    */
    @ApiModelProperty(value = "6-导航图标" )
    private String icon;
    /**
    * 导航状态
    */
    @ApiModelProperty(value = "7-导航状态" )
    private Boolean status;

    public static Converter<NavInputDTO, NavDO> converter = new Converter<NavInputDTO, NavDO>() {
        @Override
        public NavDO doForward(NavInputDTO navInputDTO) {
            return WarpsUtils.copyTo(navInputDTO, NavDO.class);
        }

        @Override
        public NavInputDTO doBackward(NavDO navDO) {
            return WarpsUtils.copyTo(navDO, NavInputDTO.class);
        }
    };

    @Override
    public NavDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public NavInputDTO convertFor(NavDO navDO) {
        return converter.doBackward(navDO);
    }
}
