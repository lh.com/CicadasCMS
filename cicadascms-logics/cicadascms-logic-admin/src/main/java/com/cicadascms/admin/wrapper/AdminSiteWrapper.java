package com.cicadascms.admin.wrapper;

import com.cicadascms.data.domain.SiteDO;
import com.cicadascms.admin.service.IAdminSiteService;
import com.cicadascms.admin.vo.SiteVO;
import com.cicadascms.common.func.Fn;
import com.cicadascms.common.utils.SpringContextUtils;
import com.cicadascms.common.base.BaseWrapper;
import org.springframework.util.Assert;
import com.cicadascms.common.utils.WarpsUtils;

public class AdminSiteWrapper implements BaseWrapper<SiteDO, SiteVO> {

    private final static IAdminSiteService siteService;

    static {
        siteService = SpringContextUtils.getBean(IAdminSiteService.class);
    }

    public static AdminSiteWrapper newBuilder() {
        return new AdminSiteWrapper();
    }

    @Override
    public SiteVO entityVO(SiteDO entity) {
        Assert.isTrue(Fn.isNotNull(entity), SiteDO.class.getName() + " 对象不存在！");
        return  WarpsUtils.copyTo(entity, SiteVO.class);
    }
}