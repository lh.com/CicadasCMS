package com.cicadascms.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * NavVO对象
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ApiModel(value="NavVO对象", description="导航菜单")
public class NavVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer navId;
    /**
    * 导航名称
    */
    @ApiModelProperty(value = "2-导航名称" )
    private String navName;
    /**
    * target
    */
    @ApiModelProperty(value = "3-target" )
    private String target;
    /**
    * 链接地址
    */
    @ApiModelProperty(value = "4-链接地址" )
    private String href;
    /**
    * 上级编号
    */
    @ApiModelProperty(value = "5-上级编号" )
    private Integer parentId;
    /**
    * 是否拥有子类
    */
    @ApiModelProperty(value = "6-是否拥有子类" )
    private Integer hasChild;
    /**
    * 导航图标
    */
    @ApiModelProperty(value = "7-导航图标" )
    private String icon;
    /**
    * 导航状态
    */
    @ApiModelProperty(value = "8-导航状态" )
    private Boolean status;
    /**
    * 创建用户
    */
    @ApiModelProperty(value = "9-创建用户" )
    private Integer createUser;
    /**
    * 创建时间
    */
    @ApiModelProperty(value = "10-创建时间" )
    private LocalDateTime createTime;
    /**
    * 更新用户
    */
    @ApiModelProperty(value = "11-更新用户" )
    private Integer updateUser;
    /**
    * 更新用户
    */
    @ApiModelProperty(value = "12-更新用户" )
    private LocalDateTime updateTime;
    /**
    * 删除标记
    */
    @ApiModelProperty(value = "13-删除标记" )
    private Boolean deletedFlag;

}
