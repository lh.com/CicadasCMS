package com.cicadascms.admin.dto;

import cn.hutool.core.util.StrUtil;
import com.cicadascms.data.domain.NavDO;
import com.cicadascms.common.base.BaseDTO;
import com.cicadascms.common.convert.Converter;
import com.cicadascms.common.utils.WarpsUtils;
import com.cicadascms.common.constant.Constant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.io.Serializable;

/**
 * <p>
 * NavQueryDTO对象
 * 导航菜单
 * </p>
 *
 * @author Jin
 * @since 2020-11-02
 */
@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="NavQueryDTO对象")
public class NavQueryDTO extends BaseDTO<NavQueryDTO, NavDO> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "1-当前页码")
    public long current = Constant.PAGE_NUM;

    @ApiModelProperty(value = "2-分页数量")
    public long size = Constant.PAGE_SIZE;

    @ApiModelProperty(value = "3-排序字段")
    private String descs;

    @ApiModelProperty(value = "4-排序字段")
    private String ascs;

    /**
    * 导航名称
    */
    @ApiModelProperty(value = "3-导航名称" )
    private String navName;
    /**
    * target
    */
    @ApiModelProperty(value = "4-target" )
    private String target;
    /**
    * 链接地址
    */
    @ApiModelProperty(value = "5-链接地址" )
    private String href;
    /**
    * 上级编号
    */
    @ApiModelProperty(value = "6-上级编号" )
    private Integer parentId;
    /**
    * 是否拥有子类
    */
    @ApiModelProperty(value = "7-是否拥有子类" )
    private Integer hasChild;
    /**
    * 导航图标
    */
    @ApiModelProperty(value = "8-导航图标" )
    private String icon;
    /**
    * 导航状态
    */
    @ApiModelProperty(value = "9-导航状态" )
    private Boolean status;

    public Page<NavDO> page() {
        Page<NavDO> page = getPage(current, size);
        OrderItem.ascs(StrUtil.split(ascs, ",")).forEach(page.getOrders()::add);
        OrderItem.descs(StrUtil.split(descs, ",")).forEach(page.getOrders()::add);
        return page;
    }

    public static Converter<NavQueryDTO, NavDO> converter = new Converter<NavQueryDTO, NavDO>() {
        @Override
        public NavDO doForward(NavQueryDTO navQueryDTO) {
            return WarpsUtils.copyTo(navQueryDTO, NavDO.class);
        }

        @Override
        public NavQueryDTO doBackward(NavDO navDO) {
            return WarpsUtils.copyTo(navDO, NavQueryDTO.class);
        }
    };

    @Override
    public NavDO convertToEntity() {
        return converter.doForward(this);
    }

    @Override
    public NavQueryDTO convertFor(NavDO navDO) {
        return converter.doBackward(navDO);
    }
}
